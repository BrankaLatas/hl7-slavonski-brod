USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Triggered_Exam_Assigned]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Triggered_Exam_Assigned]
	@examid int,
	@EditBy int,
	@error  varchar(max) output
AS
BEGIN
	SET NOCOUNT ON;
	SET @error='';
	IF(@examid is null)
		RETURN;
	/*
		INSERT INTO HL7_QUEUE to send message 'Assigned' to HIS
	*/
	DECLARE @HIS_ip   varchar(max),
			@HIS_port varchar(max);
	SELECT
			@HIS_ip = S_DATA
		FROM
			HL7_SETTINGS
		WHERE
			S_NAME = 'HIS_ADDR'

	SELECT
			@HIS_port = S_DATA
		FROM
			HL7_SETTINGS
		WHERE
			S_NAME = 'HIS_PORT';

	BEGIN TRY
		INSERT INTO 
			HL7_QUEUE(
				EXAMID,
				QSTATUS,
				SENDTIME,
				CMD,
				STAT,
				CUSTOM1,
				D_ADDR,
				D_PORT)
			VALUES (
				(select ExamID from EXAM where ExamID = @examid),
				0,
				GETDATE(),
				'UA',
				'IP',
				CAST(@EditBy AS VARCHAR(MAX)),
				@HIS_ip,
				@HIS_port)
	END TRY
	BEGIN CATCH
		SET @error='Failed to insert exam to output queue: ' + ERROR_MESSAGE();
	END CATCH; 
END



GO
