USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Users_Find]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[Users_Find]
@usersCODE varchar(256),@usersNAME varchar(256),
@usersID int output,
@error varchar(max) output
AS
BEGIN
SET NOCOUNT ON;
SET @error='';
SET @usersID=NULL;

DECLARE @issaNAME varchar(256);

BEGIN TRY
  SELECT @usersID=UsersID,@issaNAME=[Name] FROM USERS WHERE UserCode=@usersCODE;
END TRY
BEGIN CATCH
  SET @error=ERROR_MESSAGE();
  RETURN;
END CATCH;

IF @usersID is null
  BEGIN
    SET @error=''; /* user not found */
	RETURN;
  END;
ELSE
   BEGIN
     if @issaNAME<>@usersNAME
	   SET @error='Discrepancies';
   END;

END


GO
