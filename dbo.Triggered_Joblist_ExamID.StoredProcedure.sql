USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Triggered_Joblist_ExamID]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[Triggered_Joblist_ExamID]
	@joblistid int, /* joblistid = record that was updated */
	@examid int,    /* examid = new value for examid */
	@error varchar(max) output
AS
BEGIN
   SET NOCOUNT ON;
   SET @error='';
   /*----------------------------------------------------------------------------------------
   ResultDiag_ICD = R10  - from  R10^Boli u trbuhu i u zdjelici^I10
   ----------------------------------------------------------------------------------------*/
   DECLARE @id int,@HL7messageID int;

   BEGIN TRY
	  SELECT TOP 1 @id=ID,@HL7messageID=HL7_MESSAGE_ID FROM HL7_ORDERS WHERE JOBLISTID=@joblistid ORDER BY ID DESC;
      IF(@id is null or @HL7messageID is null or @examid is null)
      BEGIN
         SET @error='Error: HL7 order data not found';
	     RETURN;
      END;

	  /**********************************************************
	     1)  Update HL7_ORDERS.ExamID
	  **********************************************************/
      /* UPDATE HL7_ORDERS SET EXAMID=@examid WHERE ID=@id; */
	  UPDATE HL7_ORDERS SET EXAMID=@examid WHERE
	  joblistid IN (select joblistid from joblist where examid=@examid);
	  /**********************************************************
	     2)  auto-add some statusmask for exam sent from Traumatologija
	  **********************************************************/
	  /* update exam set StatusMask=StatusMask|536870912 where substring(exam.sentfrom,1,20)='Traumatologija' and examid=@examid; */
	  /**********************************************************
	     3)  auto assign referring user with usersID=36
	  **********************************************************/
	  /*
	  IF(SELECT ISNULL(examdevice,'') from exam where examid=@examid)<>'ECAM'
	  BEGIN
		IF(SELECT COUNT(1) FROM REFERINGUSER WHERE EXAMID=@examid AND USERSID=36)<1
	    BEGIN
			INSERT INTO REFERINGUSER(examid,usersid,editby,editdate) SELECT @examid,usersid,1,getdate() FROM USERS WHERE USERSID=36;
		END;
	  END;
	  */
	  /**********************************************************
	     3)  add @RESULTDIAG_ICD and BillingRequestNo to exam
	  **********************************************************/
	 /* DECLARE @RESULTDIAG_ICD varchar(max)=NULL */;
	 /*
     
	 SELECT @RESULTDIAG_ICD=COL_DATA FROM EXAM CROSS APPLY Split(CurrentDisease,' ') 
     WHERE COL_NO=1 AND EXAMID=@examid;

	 IF(ISNULL(@RESULTDIAG_ICD,'')<>'')
	   UPDATE EXAM SET ResultDiag_ICD=@RESULTDIAG_ICD WHERE ISNULL(ResultDiag_ICD,'')='' AND EXAMID=@examid;
	 
	 */
/*
	   UPDATE EXAM SET EXAM.BillingRequestNo=HL7_ORDERS.TFIELD2 FROM 
	   EXAM JOIN HL7_ORDERS ON EXAM.EXAMID=HL7_ORDERS.EXAMID 
	   WHERE ISNULL(EXAM.BillingRequestNo,'')='' AND HL7_ORDERS.TFIELD2 IS NOT NULL AND EXAM.EXAMID=@examid;
*/

   END TRY
   BEGIN CATCH
     SET @error='Error: failed to update data';
   END CATCH;

END

GO
