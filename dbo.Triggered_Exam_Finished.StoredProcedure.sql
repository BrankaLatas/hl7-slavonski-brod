USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Triggered_Exam_Finished]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Triggered_Exam_Finished]
	@examid int,
	@error varchar(max) output
AS
BEGIN
	SET NOCOUNT ON;
	SET @error='';
	IF(@examid is null)
		RETURN;
	/*
		INSERT INTO HL7_QUEUE to send message 'finished' to HIS
	*/
	DECLARE @HIS_ip   varchar(max),
			@HIS_port varchar(max);

	SELECT
			@HIS_ip = S_DATA
		FROM
			HL7_SETTINGS
		WHERE
			S_NAME = 'HIS_ADDR'

	SELECT
			@HIS_port = S_DATA
		FROM
			HL7_SETTINGS
		WHERE
			S_NAME = 'HIS_PORT';

	DECLARE @ExamTypeOrder  AS VARCHAR(MAX);
	DECLARE @ExamTypeResult AS VARCHAR(MAX);
	DECLARE @ExamFinishUser AS VARCHAR(MAX);
	DECLARE @ExamFinishUserCodeHIS AS VARCHAR(MAX);
	SET @ExamTypeOrder  = (SELECT JobList.ExamType     FROM JobList WHERE JobList.ExamID = @examid);
	SET @ExamTypeResult = (SELECT Exam.ExamType        FROM Exam    WHERE Exam.ExamID    = @examid);
	SET @ExamFinishUser = CAST((SELECT Exam.UsersOverviewID FROM Exam    WHERE Exam.ExamID    = @examid) AS VARCHAR(MAX));
	--SET @ExamFinishUserCodeHIS = (SELECT Users.UserCode FROM Users  WHERE Users.UsersID  = @ExamFinishUser);

	IF @ExamTypeOrder <> @ExamTypeResult
	BEGIN
		EXEC ISSA_HL7.dbo.Triggered_ExamType_Changed
			@examid,
			@ExamFinishUser,
			@error   output;
		EXEC HL7_Wait;
		EXEC HL7_Wait;
	END;

	/*EXEC ISSA_HL7.dbo.Triggered_Exam_Assigned
		@examid,
		@ExamFinishUser,
		@error   output;
	EXEC HL7_Wait;
	EXEC HL7_Wait;*/

	BEGIN TRY
		INSERT INTO 
			HL7_QUEUE(
				EXAMID,
				QSTATUS,
				SENDTIME,
				CMD,
				STAT,
				D_ADDR,
				D_PORT)
			VALUES (
				(select ExamID from EXAM where ExamID = @examid),
				0,
				GETDATE(),
				'SC',
				'F',
				@HIS_ip,
				@HIS_port);
	END TRY
	BEGIN CATCH
		SET @error='Failed to insert exam to output queue: ' + ERROR_MESSAGE();
	END CATCH;
END



GO
