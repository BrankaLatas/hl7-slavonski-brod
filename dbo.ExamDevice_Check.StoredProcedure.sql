USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[ExamDevice_Check]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[ExamDevice_Check]
@examtypeID int,
@examdeviceID varchar(256) output,
@examdeviceNAME varchar(256) output,
@error varchar(max) output
AS
BEGIN
SET NOCOUNT ON;
SET @error='';
/*--------------------------------------------------------------------------------*/
SELECT TOP 1 @examdeviceID=b.examdeviceID,@examdeviceNAME=b.NAME 
FROM EXAMTYPEDEVICELIST a join EXAMDEVICE b on A.ExamDevice=b.ExamDeviceID 
WHERE a.examTYPE=@examtypeID AND ISNULL(b.NotOperating,0)=0;
/*--------------------------------------------------------------------------------*/
IF(@examdeviceID is null)
  BEGIN
    SET @error='Exam device not set for exam type';
	RETURN;
  END;
/*--------------------------------------------------------------------------------*/
END;



GO
