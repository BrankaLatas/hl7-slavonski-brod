USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Patient_Add2]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Patient_Add2]
	@PatientIDexisting	int,
	@PatientCODE		varchar(256),
	@PatientNAME		varchar(256),
	@PatientDOB			datetime,
	@PatientSEXID		int,
	@PatientMBO			varchar(256),
	@PatientOIB			varchar(256),
	@PatientADDR		varchar(256),
	@PatientTEL			varchar(256),
	@PatientID			int				output, 
	@error				varchar(max)	output
AS
BEGIN
	SET NOCOUNT ON;
	SET @error='';
	/*--------------------------------------------------------------------------------*/
	DECLARE @retr		int,
			@ipos		int;
	DECLARE @newcode	varchar(50); /* new code with # (123#1 or 123#2 or first free number after #) */
	DECLARE @basecode	varchar(256);

	SET @ipos=CHARINDEX('#',@patientCODE);
	IF(@ipos>0)
		SET @basecode=SUBSTRING(@patientCODE,1,@ipos-1);
	ELSE
		SET @basecode=@patientCODE;

	SET @retr=0;
	WHILE(@retr<10000)
	BEGIN
		SET @newcode=null;
		SELECT TOP 10 
				ROW_NUMBER()over(order by c1) as id,
				@basecode+'#'+cast(ROW_NUMBER()over(order by c1)+@retr as varchar) as code,
				0 as patientid
			INTO #ids 
			FROM 
				(values(1),(2),(3),(4),(5),(6),(7),(8),(9),(10))as q(c1);
   
		UPDATE #ids 
			SET PatientID = Patient.PatientID 
				FROM #ids
				JOIN patient on patient.code = #ids.code;
   
		SELECT TOP 1
				@newcode = code
			FROM
				#ids
			WHERE
				PatientID = 0
			ORDER BY id;
		IF(@newcode is not null)
		BEGIN
			BREAK;
		END;
		SET @retr=@retr+10;
	END;
	DROP TABLE #ids;
	IF(@newcode is null)
	BEGIN
		SET @error='Failed to generate #code for patient';
		SET @patientID=null;
		RETURN;
	END;
	/*--------------------------------------------------------------------------------*/
	SET @error='';
	EXEC Patient_Backup @patientIDexisting,@error output;
	IF(@error<>'')
	BEGIN
		SET @patientID=null;
		RETURN;
	END;

	BEGIN TRY
		UPDATE PATIENT SET CODE=@newcode where PATIENTID=@patientIDexisting;
	END TRY
	BEGIN CATCH
		SELECT @error='Failed to update patient code [patientid='+cast(@patientID as varchar)+' new code='+@newcode+']: '+ERROR_MESSAGE();
		SET @patientID=null;
		RETURN;
	END CATCH;
	/*--------------------------------------------------------------------------------*/
	DECLARE @i int;
	SET @i=0;
	WHILE(@i<10)
	BEGIN
		EXEC Patient_Add1 
			@basecode,
			@patientNAME,
			@patientDOB,
			@patientSEXID,
			@patientMBO,
			@patientOIB,
			@patientADDR,
			@patientTEL,
			@patientID	output,
			@error		output;
		IF(@error LIKE '%Violation of PRIMARY KEY%')
			Exec HL7_Wait;
		ELSE
			BREAK;
		SET @i=@i+1;
	END;
	/*--------------------------------------------------------------------------------*/
END
GO
