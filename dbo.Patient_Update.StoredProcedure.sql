USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Patient_Update]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------------------------------------------------

CREATE procedure [dbo].[Patient_Update]
	@PatientID int,
	@patientCODE varchar(256),
	@PatientName varchar(256),
	@patientDOB datetime,
	@patientSEXID int,
	@PatientMBO varchar(256),
	@error varchar(max) output
AS
BEGIN
	SET NOCOUNT ON;
	SET @error='';

	EXEC Patient_Backup @patientID,@error output;

	IF(@error<>'')
	BEGIN
		SET @patientID=null;
		RETURN;
	END;

	BEGIN TRY
		UPDATE Issa.dbo.Patient SET
				PATIENT.Name   = @PatientName,
				BirthDate      = @patientDOB,
				SEXID          = @patientSEXID,
				ID             = @PatientMBO,
				EDITBY         = 441,
				EDITDATE       = GETDATE()
			where
				Patient.PatientID      = @PatientID;
	END TRY
	BEGIN CATCH
		SELECT @error='Failed to update patient data [patientid='+cast(@patientID as varchar)+']: '+ERROR_MESSAGE();
		RETURN;
	END CATCH;
	SET @error='';
END




GO
