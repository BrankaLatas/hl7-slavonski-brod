USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[ExamDevice_Get]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[ExamDevice_Get]
@code varchar(32),
@examdeviceID INT output,
@examtypeID int=null
AS
BEGIN
SET NOCOUNT ON;

SET @examdeviceID=NULL;

IF(ISNULL(@code,'')='' OR @examtypeid IS NULL)
  RETURN;


IF(@code='90')
  SELECT @examdeviceID=ExamDevice from EXAMTYPEDEVICELIST WHERE ExamType=@examtypeID AND ExamDevice=14;
ELSE
IF(@code='91')
  SELECT @examdeviceID=ExamDevice from EXAMTYPEDEVICELIST WHERE ExamType=@examtypeID AND ExamDevice=19;
ELSE
IF(@code='92')
  SELECT @examdeviceID=ExamDevice from EXAMTYPEDEVICELIST WHERE ExamType=@examtypeID AND ExamDevice=10;
ELSE
IF(@code='93')
  SELECT @examdeviceID=ExamDevice from EXAMTYPEDEVICELIST WHERE ExamType=@examtypeID AND ExamDevice=4;
ELSE
IF(@code='94')
  SELECT @examdeviceID=ExamDevice from EXAMTYPEDEVICELIST WHERE ExamType=@examtypeID AND ExamDevice=18;
ELSE
IF(@code='95')
  SELECT @examdeviceID=ExamDevice from EXAMTYPEDEVICELIST WHERE ExamType=@examtypeID AND ExamDevice=16;
ELSE
IF(@code='96')
  SELECT @examdeviceID=ExamDevice from EXAMTYPEDEVICELIST WHERE ExamType=@examtypeID AND ExamDevice=17;
ELSE
IF(@code='97')
  SELECT @examdeviceID=ExamDevice from EXAMTYPEDEVICELIST WHERE ExamType=@examtypeID AND ExamDevice=20;
/*

IF(ISNULL(@code,'')='')
  RETURN;
  
IF(@code='90')
  SELECT @examdeviceID=ExamDeviceID from EXAMDEVICE WHERE ExamDeviceID=14;
ELSE
IF(@code='91')
  SELECT @examdeviceID=ExamDeviceID from EXAMDEVICE WHERE ExamDeviceID=19;
ELSE
IF(@code='92')
  SELECT @examdeviceID=ExamDeviceID from EXAMDEVICE WHERE ExamDeviceID=10;
ELSE
IF(@code='93')
  SELECT @examdeviceID=ExamDeviceID from EXAMDEVICE WHERE ExamDeviceID=4;
ELSE
IF(@code='94')
  SELECT @examdeviceID=ExamDeviceID from EXAMDEVICE WHERE ExamDeviceID=18;
ELSE
IF(@code='95')
  SELECT @examdeviceID=ExamDeviceID from EXAMDEVICE WHERE ExamDeviceID=16;
ELSE
IF(@code='96')
  SELECT @examdeviceID=ExamDeviceID from EXAMDEVICE WHERE ExamDeviceID=17;
ELSE
IF(@code='97')
  SELECT @examdeviceID=ExamDeviceID from EXAMDEVICE WHERE ExamDeviceID=20;
*/

END



GO
