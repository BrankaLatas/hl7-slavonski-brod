USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Triggered_Exam_Send]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Triggered_Exam_Send]
	@examid int,
	@error varchar(max) output
AS
BEGIN
	SET NOCOUNT ON;
	SET @error='';

	IF(@examid is null)
		RETURN;
	  
	-- other RIS IP addr., port and app. name 
	DECLARE @ris2_ip  varchar(32) = '192.168.100.21';
	DECLARE @ris2_port        int = 1234;
	DECLARE @ris2_app varchar(32) = 'MEDIT';
	DECLARE @folder  varchar(256) = 'C:\Vamstec\HL73\Messages\';
  
	BEGIN TRY
  		INSERT INTO HL7_QUEUE(
				EXAMID, 
				QSTATUS, 
				SENDTIME, 
				CMD, 
				D_ADDR, 
				D_PORT, 
				CUSTOM3)
			VALUES (
				@ExamID,
				0,
				GETDATE(),
				'NW', 
				@ris2_ip, 
				@ris2_port, 
				@ris2_app);
	END TRY
	BEGIN CATCH
		SET @error='Failed to insert exam to output queue: '+ERROR_MESSAGE();
	END CATCH;
END


GO
