USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Users_Add2]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[Users_Add2]
@usersIDexisting int,
@usersNAME varchar(256),
@usersCODE varchar(256),
@usersID int output,
@error varchar(max) output
AS
BEGIN
SET NOCOUNT ON;
SET @error='';
/*--------------------------------------------------------------------------------*/
declare @retr int;
declare @newcode varchar(50); /* new code with # (123#1 or 123#2 or first free number after #) */

SET @retr=0;
WHILE(@retr<100)
   BEGIN
   SET @newcode=null;

   SELECT TOP 10 ROW_NUMBER()over(order by usersid) as id,@usersCODE+'#'+cast(ROW_NUMBER()over(order by usersid)+@retr as varchar) as code,0 as usersid
   into #ids from users;
   
   UPDATE #ids set usersid=users.usersid from #ids join users on users.UserCode=#ids.code;
   
   SELECT TOP 1 @newcode=code from #ids where usersid=0 ORDER BY id;
   
   IF(@newcode is not null)
     BREAK;
   
   SET @retr=@retr+10;
   END;

DROP TABLE #ids;
IF(@newcode is null)
  BEGIN
    SET @error='Failed to generate #code for users';
	SET @usersID=null;
	RETURN;
  END;
/*--------------------------------------------------------------------------------*/
SET @error='';
EXEC Users_Backup @usersIDexisting,@error output;
IF(@error<>'')
  BEGIN
  SET @usersID=null;
  RETURN;
  END;

BEGIN TRY
  UPDATE users SET USERCODE=@newcode where usersid=@usersIDexisting;
END TRY
BEGIN CATCH
   SELECT @error='Failed to update user code [usersid='+cast(@usersIDexisting as varchar)+' new code='+@newcode+']: '+ERROR_MESSAGE();
   SET @usersID=null;
   RETURN;
END CATCH;
/*--------------------------------------------------------------------------------*/
SET @error='';
EXEC Users_Add1 @usersCODE,@usersNAME,@usersID output,@error output;
/*--------------------------------------------------------------------------------*/
END








GO
