USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[ExportMessagesSB]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------

CREATE procedure [dbo].[ExportMessagesSB]
	@hl7_queue_id INT,
	@error varchar(MAX) output,
	@command VARCHAR(256)=NULL,
	@new_accno varchar(256)=NULL,
	@new_et varchar(256)=NULL
AS
BEGIN
	SET NOCOUNT ON;
	SET @error='';

	DECLARE @msg1      varchar(max);
	DECLARE @joblistid int,
			@examid    int;
	DECLARE @cmd       varchar(256),
			@status    varchar(256),
			@accno     varchar(256),
			@examdate  varchar(32),
			@doc       varchar(256),
			@etype     varchar(256),
			@ref       varchar(256),
			@our_accno varchar(256),
			@CUSTOM1   varchar(max);
	DECLARE @appname   varchar(256)='',
			@dport     int,
			@dserver   varchar(32),
			@dfolder   varchar(max);

	BEGIN TRY
		IF @hl7_queue_id IS NULL
		BEGIN
			SELECT TOP 1 
				@hl7_queue_id = ID,
				@joblistid    = JOBLISTID,
				@examid       = EXAMID,
				@cmd          = CMD,
				@status       = STAT,
				@new_accno    = CUSTOM1,
				@new_et       = CUSTOM2,
				@appname      = CUSTOM3,
				@dport        = D_PORT,
				@dserver      = D_ADDR,
				@dfolder      = D_FOLDER
				FROM 
					HL7_QUEUE 
				WHERE 
					    QSTATUS >= 0 
					AND QSTATUS <= 5 
					AND SENDTIME <= GETDATE()
				ORDER BY 
					QSTATUS,ID;
			IF @hl7_queue_id IS NULL
			BEGIN
				RETURN; /* nothing in export queue */
			END;
		END;
		ELSE
		BEGIN
			IF(ISNULL(@command,'')='')
				SELECT TOP 1
					@joblistid = JOBLISTID,
					@examid    = EXAMID,
					@cmd       = CMD,
					@status    = STAT,
					@new_accno = CUSTOM1,
					@new_et    = CUSTOM2,
					@appname   = CUSTOM3,
					@dport     = D_PORT,
					@dserver   = D_ADDR,
					@dfolder   = D_FOLDER
					FROM
						HL7_QUEUE
					WHERE 
							QSTATUS  >= 0 
						AND QSTATUS  <= 5 
						AND SENDTIME <= GETDATE() 
						AND ID        = @hl7_queue_id;
			ELSE
				SELECT TOP 1
					@joblistid = JOBLISTID,
					@examid    = EXAMID,
					@cmd       = CMD,
					@status    = STAT,
					@new_accno = CUSTOM1,
					@new_et    = CUSTOM2,
					@appname   = CUSTOM3,
					@dport     = D_PORT,
					@dserver   = D_ADDR,
					@dfolder   = D_FOLDER
					FROM
						HL7_QUEUE
					WHERE
						ID = @hl7_queue_id;
		END;
		IF(ISNULL(@command,'')<>'') SET @cmd = @command;
		IF @joblistid is NULL AND @examid is NULL
		BEGIN
			IF(@cmd is null) SET @error=''; /* no data */
		ELSE
			SET @error='JoblistID and ExamID empty ['+cast(@hl7_queue_id as varchar)+']';
			RETURN;
		END;
	END TRY


	BEGIN CATCH
		SET @error='Failed to get data for message: ' + ERROR_MESSAGE();
		RETURN;
	END CATCH;

	IF ISNULL(@cmd,'')=''
	BEGIN
		SET @error = 'Export message cmd field is empty';
		RETURN;
	END;

	/*----------------------------------------------------------------------------------------
	 Generate output HL7 message
	 No need to encapsulate it with message begin (char 11) and message end (char 28, char 13) strings,
	 HL7 service will do it if strings not found
	----------------------------------------------------------------------------------------*/

	CREATE TABLE #orc_obr(
		ID         INT IDENTITY(1,1),
		[status]   varchar(256),
		accno      varchar(256),
		examdate   varchar(32),
		doc        varchar(256),
		etype      varchar(256),
		ref        varchar(256),
		rad        varchar(256),
		ordinacija varchar(256),
		our_accno  varchar(256),
		clinical   varchar(max),
		diag       varchar(256));

	DECLARE @MSH varchar(max),
			@PID varchar(max),
			@ORC varchar(max),
			@OBR varchar(max);
	DECLARE @issa_status varchar(256);
	DECLARE @cnt int,
			@x   int;
	DECLARE @ordinacija varchar(256)='';

	--  Processing in case ExamID=0, this means this is a JobList message
	SET @msg1 = '';
	IF(ISNULL(@examid,0)=0) /* JobList message */
	BEGIN
		IF(@cmd<>'CA') /* We can send only Cancel message */
			RETURN;

		-- First we are going to generate MSH segment
		-- This is done by Export_Get_MSH procedure
		EXEC Export_Get_MSH
			'OMG^O19^OMG_O19', 
			@error output,
			@MSH   output;
		IF(@error<>'')
			RETURN;
	  
		-- Then we create the PID segment
		-- This is done by Export_Get_PID procedure
		EXEC Export_Get_PID 
			@joblistid,
			@examid,
			@error output,
			@PID output;
		IF(@error<>'')
			RETURN;

		-- OK, so we create a temporary message and put that into @msg1 variable
		SET @msg1 =   @MSH + CHAR(13)
					+ @PID + CHAR(13);

		-- Now we are fetching data for ORC and OBR segments
		-- This is done by inserting that data into #orc_obr table
		-- This query gets that data
		INSERT INTO #orc_obr(
				[status],
				accno,
				examdate,
				doc,
				etype,
				ref,
				ordinacija,
				our_accno)
			SELECT TOP 1 
				'',
				x.ACCNO, -- HIS Accession Number
				SUBSTRING(convert(varchar,JOBDATE,112)+replace(convert(varchar,JOBSTART,114),':',''),1,14), -- Exam date
				ISNULL(u.USERCODE+'^'+u.[NAME],''),    -- Radiologist
				ISNULL(et.CODEVALUE,'')+'^'+et.[NAME], -- Exam type
				ISNULL(u2.USERCODE+'^'+u2.[NAME],''),  -- Referring physician
				SUBSTRING(ed.CodeValue,1,255),         -- Issa Exam device
				ISNULL(a.NewExamNumber,'')             -- Issa Accession Number
			FROM 
				          JOBLIST    a 
				LEFT JOIN USERS      u  ON a.USERID=u.USERSID 
				LEFT JOIN USERS      u2 ON a.ReferingID=u2.USERSID 
				LEFT JOIN EXAMTYPE   et ON a.EXAMTYPEID=et.EXAMTYPEID
				LEFT JOIN EXAMDEVICE ed ON a.ExamDeviceID=ed.ExamDeviceID
				     JOIN HL7_ORDERS x  ON x.JOBLISTID=a.JOBLISTID
			WHERE
					a.JobListID      = @joblistid 
				AND a.StatusMask&256 = 256;


			-- Now we check whether we have inserted anything into #orc_obr
			SELECT @cnt = COUNT(1) FROM #orc_obr;
			-- If we didn't insert anything, we terminate the procedure
			IF(@cnt = 0)
			BEGIN
				SET @error = 'Failed to make ORC/OBR - no data';
				RETURN;
			END;

			-- Now we create actual segments
			SET @x = 0;
			WHILE (@x < @cnt) -- ORC and OBR data is fetched from the temporary #orc_obr table
			BEGIN
				SELECT 
					@issa_status = [status],
					@accno       = accno,
					@examdate    = examdate,
					@doc         = doc,
					@etype       = etype,
					@ref         = ref,
					@ordinacija  = ISNULL(ordinacija,''),
					@our_accno   = our_accno 
				FROM 
					#orc_obr
				WHERE
					id=(@x+1);

				IF (@etype is null)
				BEGIN
					SET @error='Failed to make ORC/OBR - exam type is empty';
					RETURN;
				END;

				IF (@accno is null)
				BEGIN
					SET @error='Failed to make ORC/OBR - accession number is empty, exam not in orders?';
					RETURN;
				END;
	     
			/* SET @ORC='ORC|CA|'+dbo.HL7_Escape(@accno,0)+'|'+dbo.HL7_Escape(@accno,0)+'||||||'+@examdate+'|'+dbo.HL7_Escape(@doc,1)+'|'+dbo.HL7_Escape(@doc,1)+'|'+dbo.HL7_Escape(@rad,1)+'|'; */

			-- We fill the string of the ORC segment
			SET @ORC = 'ORC|CA|' 
					 + dbo.HL7_Escape(@accno,0)
					 + '|'
					 + dbo.HL7_Escape(@our_accno,0)
					 + '||||||'
					 + @examdate
					 + '|'
					 + dbo.HL7_Escape(@doc,1)
					 + '||'
					 + @ordinacija;

			-- Then we fill the string of the OBR segment
			SET @OBR = 'OBR|1|'
					 + dbo.HL7_Escape(@accno,0)
					 + '|'
					 + dbo.HL7_Escape(@our_accno,0)
					 + '|'
					 + dbo.HL7_Escape(@etype,1)
					 + '|||'
					 + @examdate
					 + '||||||||||||||||||F||1|||||'
					 + dbo.HL7_Escape(@doc,1)
					 + '||||'
					 + @examdate
					 + '|';

			-- And finally, we add these two segments to our message
			SET @msg1 =   @msg1 
						+ @ORC + CHAR(13) 
						+ @OBR + CHAR(13);
			SET @x = @x + 1;
		END;

		-- Now we insert that message to our output log
		-- From there it is finally sent to the receiving system
		BEGIN TRY
		    INSERT INTO HL7_LOGS_OUT(HL7_QUEUE_ID,CREATED,QSTATUS,SENDTIME,MSG,D_ADDR,D_PORT,D_FOLDER)
			SELECT 
				ID,
				GETDATE(),
				0,
				CASE WHEN SENDTIME<GETDATE() THEN GETDATE() ELSE SENDTIME END,
				@msg1,
				@dserver,
				@dport,
				@dfolder
			FROM HL7_QUEUE 
			WHERE ID = @hl7_queue_id;
		END TRY
		BEGIN CATCH
			SET @error='Failed to add message: ' + ERROR_MESSAGE();
			RETURN;
		END CATCH;
		SET @error='';

		-- We did it, yeee!
		RETURN;
	END;
	-- This is the end of the code which handles JobList messages.

	-- Now we are in the part which processes SC outgoing messages
	-- These are sent every time an exam is finished
	DECLARE @UserResult_ID                INT = 0;
	DECLARE @UserResult_Name     VARCHAR(MAX) = '';
	DECLARE @UserResult_Code     VARCHAR(MAX) = '';
	DECLARE @UserUA_ID                    INT = 0;
	DECLARE @UserUA_Name         VARCHAR(MAX) = '';
	DECLARE @UserUA_Code         VARCHAR(MAX) = '';
	DECLARE @Device_HIS_Code     VARCHAR(MAX) = '';
	DECLARE @Device_HIS_Name     VARCHAR(MAX) = '';
	DECLARE @Device_Issa_Name    VARCHAR(MAX) = '';
	DECLARE @AccessionNumberHIS  VARCHAR(MAX) = '';
	DECLARE @AccessionNumberIssa VARCHAR(MAX) = '';
	DECLARE @ExamType            VARCHAR(MAX) = '';
	DECLARE @ExamTypeCodeHIS     VARCHAR(MAX) = '';
	--DECLARE @ExamDate            VARCHAR(MAX) = '';
	DECLARE @ExamStatus          VARCHAR(MAX) = '';

	IF(@cmd='SC' /* AND @status='F' */)
	BEGIN
		SET @Device_Issa_Name = ISNULL((SELECT Exam.ExamDevice FROM Exam WHERE Exam.ExamID = @ExamID), '');
		IF @Device_Issa_Name = ''
		BEGIN
			SET @error = 'Undefined device';
			RETURN;
		END;

		SET @Device_HIS_Code = ISNULL((SELECT TOP 1 ExamDevice.CodeValue     FROM ExamDevice WHERE ExamDevice.Name = @Device_Issa_Name), '');
		SET @Device_HIS_Name = ISNULL((SELECT TOP 1 ExamDevice.DeviceNameHIS FROM ExamDevice WHERE ExamDevice.Name = @Device_Issa_Name), '');

		SET @UserResult_ID   = ISNULL((SELECT TOP 1 Exam.UsersOverViewID     FROM Exam       WHERE Exam.ExamID     = @examid), '');
		SET @UserResult_Name = ISNULL((SELECT TOP 1 Users.Name               FROM Users      WHERE Users.UsersID   = @UserResult_ID), '');
		SET @UserResult_Code = ISNULL((SELECT TOP 1 Users.UserCode           FROM Users      WHERE Users.UsersID   = @UserResult_ID), '');

		SET @AccessionNumberIssa = ISNULL((SELECT TOP 1 Exam.AccessionNumber  FROM Exam       WHERE Exam.ExamID     = @examid), '');
		SET @AccessionNumberHIS  = ISNULL((SELECT TOP 1	JobList.NewExamNumber FROM JobList    WHERE JobList.ExamID  = @examid), '');
		SET @ExamType            = ISNULL((SELECT TOP 1	Exam.ExamType         FROM Exam       WHERE Exam.ExamID     = @examid), '');
		SET @ExamTypeCodeHIS     = ISNULL((SELECT TOP 1	ExamType.CodeValue    FROM ExamType   WHERE ExamType.Name   = @ExamType), '');
		SET @JobListID			 = ISNULL((SELECT TOP 1	JobList.JobListID     FROM JobList    WHERE JobList.ExamID  = @examid), '');
		
		SET @ExamDate =	(select SUBSTRING(convert(varchar,exam.ExamDate,112)++replace(convert(varchar,exam.ExamDate,114),':',''),1,14) from Exam where exam.ExamID = @ExamID);

		IF @AccessionNumberHIS = ''
		BEGIN
			SET @error = 'No order linked with exam';
			RETURN;
		END;
	   
		SET @msg1='';

		-- OK, so I make the MSH segment
		EXEC Export_Get_MSH 
	   		 'OMG^O19^OMG_O19',
			 @error output,
			 @MSH output;
		IF(@error<>'')
			RETURN;

		-- And the PID segment
		EXEC Export_Get_PID
			 @joblistid,
			 @examid,
			 @error output,
			 @PID output;
		IF(@error<>'')
			RETURN;
		
		SET @msg1 = @MSH + CHAR(13)
	   			+ @PID + CHAR(13);
			-- And now I build ORC and OBR segments
			SET @ORC = 'ORC|SC|'
					 + dbo.HL7_Escape(@AccessionNumberHIS,0)
					 + '|'
					 + dbo.HL7_Escape(@AccessionNumberIssa,0)
					 + '||F||||'
					 + @examdate
					 + '|'
					 + dbo.HL7_Escape(@UserResult_Code + '^' + @UserResult_Name,1)
					 + '||'
					 + dbo.HL7_Escape(@Device_HIS_Code + '^' + @Device_HIS_Name,1);

			SET @OBR = 'OBR|1|'
					 + dbo.HL7_Escape(@AccessionNumberHIS,0)
					 + '|'
					 + dbo.HL7_Escape(@AccessionNumberIssa,0)
					 + '|'
					 + dbo.HL7_Escape(@ExamTypeCodeHIS + '^' + @ExamType,1)
					 + '|||'
					 + @examdate
					 + '||||||||||||||||||F||1|||||'
					 + dbo.HL7_Escape(@UserResult_Code + '^' + @UserResult_Name,1)
					 + '||||'
					 + @examdate
					 + '|';

			SET @msg1 = @msg1
					  + @ORC + CHAR(13)
					  + @OBR + CHAR(13);	

		-- Now insert to HL7_LOGS_OUT, yeee!
		DECLARE @add INT=0;
		BEGIN TRY
			INSERT INTO HL7_LOGS_OUT(
					HL7_QUEUE_ID,
					CREATED,
					QSTATUS,
					SENDTIME,
					MSG,
					D_ADDR,
					D_PORT)
				VALUES (
					@hl7_queue_id,
					GETDATE(),
					0,
					DATEADD(second,-1,GETDATE()),
					@msg1,
					@dserver,
					@dport);
		END TRY
		BEGIN CATCH
			SET @error = 'Failed to add message: ' + ERROR_MESSAGE();
			RETURN;
		END CATCH;
		SET @error='';
		RETURN;
	END

	-- XO message, this one changes exam type
	IF(@cmd='XO' /* AND @status='F' */)
	BEGIN
		TRUNCATE TABLE #orc_obr;
		-- We fetch the data from Issa database and insert it into our #orc_obr table
		INSERT INTO 
			#orc_obr(
				[status],
				accno,
				examdate,
				etype,
				our_accno)
			SELECT DISTINCT 
				CASE a.STATUSMASK&1 WHEN 1 THEN 'F' ELSE 'IP' END, 
				ISNULL(b.NewExamNumber,''),  -- Accession number, taken from JobList table.
				SUBSTRING(convert(varchar,EXAMDATE,112)++replace(convert(varchar,EXAMDATE,114),':',''),1,14),
				ISNULL(et.CODEVALUE,'') + '^' + et.[NAME],     -- Exam type
				ISNULL(a.AccessionNumber,'')  -- Accession number, taken from Exam table.
			FROM 
						  EXAM        a 
					 JOIN JOBLIST     b on a.examid          = b.examid
				LEFT JOIN EXAMTYPE   et ON a.ExamType        = et.Name
				LEFT JOIN EXAMDEVICE ed ON a.ExamDevice      = ed.[Name]
			WHERE
				    a.ExamID         = @examid 
				AND b.StatusMask&256 = 0;

		-- In2 BIS Requires that we specify user who changed the Exam Type
		-- We passed that bit of information in the CUSTOM1 field
		DECLARE @UserXO_ID            INT = 0;
		DECLARE @UserXO_Name VARCHAR(MAX) = '';
		DECLARE @UserXO_Code VARCHAR(MAX) = '';

		SET @UserXO_ID = CAST(@CUSTOM1 AS INTEGER);
		IF ISNULL(@UserXO_ID,0) <> 0
		BEGIN
			SELECT
				@UserXO_Name = USERS.Name,
				@UserXO_Code = USERS.UserCode
			FROM
				USERS
			WHERE
				USERS.UsersID = @UserXO_ID;
		END
		ELSE
		BEGIN
			SET	@UserXO_Name = 'Goran Glumac';
			SET	@UserXO_Code = '2449';
		END;
			SELECT @cnt=COUNT(1) FROM #orc_obr;
		IF(@cnt=0)
		BEGIN
			SET @error='Failed to make exam ORC/OBR - no data';
			RETURN;
		END;
			SET @msg1='';
			-- OK, so I make the MSH segment
		EXEC Export_Get_MSH 
	   		 'OMG^O19^OMG_O19',
			 @error output,
			 @MSH output;
		IF(@error<>'')
			RETURN;
			-- And the PID segment
		EXEC Export_Get_PID
			 @joblistid,
			 @examid,
			 @error output,
			 @PID output;
		IF(@error<>'')
			RETURN;

		SET @msg1 = @MSH + CHAR(13)
				  + @PID + CHAR(13);

		-- I check how many records in #orc_obr are there, if none, I bugger off
		SELECT @cnt=COUNT(1) FROM #orc_obr;
		IF(@cnt=0)
		BEGIN
			SET @error='Failed to make exam ORC/OBR - no data';
			RETURN;
		END;
		SELECT 
				@issa_status = [status],
				@accno       = accno,
				@examdate    = examdate,
				@etype       = etype,
				@our_accno   = our_accno 
			FROM 
				#orc_obr 
			WHERE 
				id = 1;
		IF(@etype is null)
		BEGIN
			SET @error='Failed to make ORC/OBR - exam type is empty';
			RETURN;
		END;
		IF(@accno is null)
		BEGIN
			SET @error='Failed to make ORC/OBR - accession number is empty, exam not in orders?';
			RETURN;
		END;
		-- And now I build ORC and OBR segments
		SET @ORC = 'ORC|XO|'
				 + dbo.HL7_Escape(@accno,0)
				 + '|'
				 --+ dbo.HL7_Escape(@our_accno,0)
				 + '||||||'
				 + @examdate
				 + '|'
				 + @UserXO_Code + '^' + @UserXO_Name
				 + '||';
		SET @OBR = 'OBR||'
				 + dbo.HL7_Escape(@accno,0)
				 + '|'
				 --+ dbo.HL7_Escape(@our_accno,0)
				 + '|'
				 + dbo.HL7_Escape(@etype,1)
				 + '|||'
				 + @examdate
				 + '||||||||||||||||||F||1|||||'
				 --+ dbo.HL7_Escape(@doc,1)
				 + '||||'
				 --+ @examdate
				 + '|';

		SET @msg1 = @msg1
				  + @ORC + CHAR(13)
				  + @OBR + CHAR(13);	
		-- Now insert to HL7_LOGS_OUT, yeee!
		BEGIN TRY
			INSERT INTO
				HL7_LOGS_OUT(
					HL7_QUEUE_ID,
					CREATED,
					QSTATUS,
					SENDTIME,
					MSG,
					D_ADDR,
					D_PORT,
					D_FOLDER)
				SELECT 
					ID,
					GETDATE(),
					0,
					DATEADD(second,-1,GETDATE()),
					@msg1,
					@dserver,
					@dport,
					@dfolder
				FROM
					HL7_QUEUE
				WHERE
					ID=@hl7_queue_id;
		END TRY
		BEGIN CATCH
			SET @error = 'Failed to add message: ' + ERROR_MESSAGE();
			RETURN;
		END CATCH;
		SET @error='';
		RETURN;
	END

	-- UA message, this one is sent when exam is assigned to radiologist
	IF(@cmd='UA' /* AND @status='F' */)
	BEGIN
		SET @Device_Issa_Name = ISNULL((SELECT Exam.ExamDevice FROM Exam WHERE Exam.ExamID = @examid), '');
		IF @Device_Issa_Name = ''
		BEGIN
			SET @error = 'Undefined device';
			RETURN;
		END;

		SET @Device_HIS_Code = ISNULL((SELECT TOP 1 ExamDevice.CodeValue     FROM ExamDevice WHERE ExamDevice.Name = @Device_Issa_Name), '');
		SET @Device_HIS_Name = ISNULL((SELECT TOP 1 ExamDevice.DeviceNameHIS FROM ExamDevice WHERE ExamDevice.Name = @Device_Issa_Name), '');

		SET @UserResult_ID   = ISNULL((SELECT TOP 1 Exam.UsersOverViewID     FROM Exam       WHERE Exam.ExamID     = @examid), '');
		SET @UserResult_Name = ISNULL((SELECT TOP 1 Users.Name               FROM Users      WHERE Users.UsersID   = @UserResult_ID), '');
		SET @UserResult_Code = ISNULL((SELECT TOP 1 Users.UserCode           FROM Users      WHERE Users.UsersID   = @UserResult_ID), '');

		SET @AccessionNumberIssa = ISNULL((SELECT TOP 1 Exam.AccessionNumber  FROM Exam       WHERE Exam.ExamID     = @examid), '');
		SET @AccessionNumberHIS  = ISNULL((SELECT TOP 1	JobList.NewExamNumber FROM JobList    WHERE JobList.ExamID  = @examid), '');
		SET @ExamType            = ISNULL((SELECT TOP 1	Exam.ExamType         FROM Exam       WHERE Exam.ExamID     = @examid), '');
		SET @ExamTypeCodeHIS     = ISNULL((SELECT TOP 1	ExamType.CodeValue    FROM ExamType   WHERE ExamType.Name   = @ExamType), '');
		SET @JobListID			 = ISNULL((SELECT TOP 1	JobList.JobListID     FROM JobList    WHERE JobList.ExamID  = @examid), '');

		IF @AccessionNumberHIS = ''
		BEGIN
			SET @error = 'No order linked with exam';
			RETURN;
		END;

		--SET @ExamStatus = ISNULL((SELECT TOP 1 (CASE Exam.STATUSMASK&1 WHEN 1 THEN 'F' ELSE 'IP' END) FROM Exam WHERE Exam.ExamID = @examid), '');

		/*IF @ExamStatus = 'F'
		BEGIN
			SET @error = 'Exam is finished, UA message cannot be sent.';
			RETURN;
		END;*/

		SET @ExamDate   = ISNULL((SELECT TOP 1
			SUBSTRING(convert(varchar,Exam.ExamDate,112)++replace(convert(varchar,EXAMDATE,114),':',''),1,14)
			FROM Exam WHERE Exam.ExamID = @examid), '');

		-- In2 BIS Requires that we specify user who assigned the User
		-- We passed that bit of information in the CUSTOM1 field

		SET @UserUA_ID = CAST(ISNULL((SELECT TOP 1 Custom1 FROM HL7_QUEUE WHERE ID = @hl7_queue_id),0) AS INT);

		IF ISNULL(@UserUA_ID,0) <> 0
		BEGIN
			SELECT
				@UserUA_Name = USERS.Name,
				@UserUA_Code = USERS.UserCode
			FROM
				USERS
			WHERE
				USERS.UsersID = @UserUA_ID;
		END
		ELSE
		BEGIN
			SET	@UserUA_Name = 'Goran Glumac';
			SET	@UserUA_Code = '2449';
		END;

		SET @msg1='';
		-- OK, so I make the MSH segment
		EXEC Export_Get_MSH 
	   		 'OMG^O19^OMG_O19',
			 @error output,
			 @MSH output;
		IF(@error<>'')
			RETURN;
		-- And the PID segment
		EXEC Export_Get_PID
			 @joblistid,
			 @examid,
			 @error output,
			 @PID output;
		IF(@error<>'')
			RETURN;

		SET @msg1 = @MSH + CHAR(13)
				  + @PID + CHAR(13);

		-- And now I build ORC and OBR segments
		SET @ORC = 'ORC|UA|'
				 + @AccessionNumberHIS
				 + '||||||||'
				 + @UserUA_Code + '^' + @UserUA_Name
				 + '|'
				 + @UserResult_Code + '^' + @UserResult_Name
				 + '|'
				 + @Device_HIS_Code + '^' + @Device_HIS_Name;
		SET @OBR = 'OBR||'
				 + @AccessionNumberHIS
				 + '|'
				 + '||1';
		SET @msg1 = @msg1
				  + @ORC + CHAR(13)
				  + @OBR + CHAR(13);	

		-- Now insert to HL7_LOGS_OUT, yeee!
		BEGIN TRY
			INSERT INTO
				HL7_LOGS_OUT(
					HL7_QUEUE_ID,
					CREATED,
					QSTATUS,
					SENDTIME,
					MSG,
					D_ADDR,
					D_PORT,
					D_FOLDER)
				SELECT 
					ID,
					GETDATE(),
					0,
					DATEADD(second,-1,GETDATE()),
					@msg1,
					@dserver,
					@dport,
					@dfolder
				FROM
					HL7_QUEUE
				WHERE
					ID=@hl7_queue_id;
		END TRY
		BEGIN CATCH
			SET @error = 'Failed to add message: ' + ERROR_MESSAGE();
			RETURN;
		END CATCH;
		SET @error='';
		RETURN;
	END
END
GO
