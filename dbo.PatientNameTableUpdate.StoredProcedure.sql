USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[PatientNameTableUpdate]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------
-- This procedure takes patient data and tries to enter it into the Patient table

CREATE PROCEDURE [dbo].[PatientNameTableUpdate]
	@PatientID	int,
	@error		varchar(max)  output
AS
BEGIN
	SET NOCOUNT ON;
	SET @error='';

	DECLARE @PatientName AS VARCHAR(256);
	DECLARE @Name1 AS VARCHAR(256),
			@Name2 AS VARCHAR(256),
			@Name3 AS VARCHAR(256),
			@Name4 AS VARCHAR(256),
			@Name5 AS VARCHAR(256);
	DECLARE @tmp   AS VARCHAR(256),
			@a	   AS VARCHAR(256);
	DECLARE @i	   AS INTEGER,
			@j	   AS INTEGER,
			@l	   AS INTEGER;

	SET @PatientName = '';
	SET @Name1 = '';
	SET @Name2 = '';
	SET @Name3 = '';
	SET @Name4 = '';
	SET @Name5 = '';
	SELECT
			@PatientName = PATIENT.Name
		FROM
			PATIENT
		WHERE
			PATIENT.PatientID = @PatientID;
	IF @PatientName = ''
	BEGIN
		SET @error='Patient not found.';
		RETURN;
	END
	ELSE
	BEGIN
		SET @i = 0;
		SET @j = 0;
		SET @l = LEN(@PatientName);
		SET @tmp = '';
		WHILE @i <= @l
		BEGIN
			SET @a = SUBSTRING(@PatientName,@i,1);
			IF @a <> '^' AND @a <> ' '
			BEGIN
				SET @tmp = @tmp + @a;
			END;
			IF (@a = '^' OR @a = ' ' OR @i=(@l)) AND LEN(@tmp) > 1
			BEGIN
				SET @j = @j + 1;
				IF @j = 1
				BEGIN
					SET @Name1 = @tmp;
				END;
				IF @j = 2
				BEGIN
					SET @Name2 = @tmp;
				END;
				IF @j = 3
				BEGIN
					SET @Name3 = @tmp;
				END;
				IF @j = 4
				BEGIN
					SET @Name4 = @tmp;
				END;
				IF @j = 5
				BEGIN
					SET @Name5 = @tmp;
				END;
				SET @tmp = '';
			END;
			SET @i = @i + 1;
		END;
		SET @Name1 = REPLACE(@Name1, 'ć','c')
		SET @Name1 = REPLACE(@Name1, 'č','c')
		SET @Name1 = REPLACE(@Name1, 'Ć','C')
		SET @Name1 = REPLACE(@Name1, 'Č','C')
		SET @Name1 = REPLACE(@Name1, 'ž','z')
		SET @Name1 = REPLACE(@Name1, 'Ž','z')
		SET @Name1 = REPLACE(@Name1, 'š','s')
		SET @Name1 = REPLACE(@Name1, 'Š','s')
		SET @Name1 = REPLACE(@Name1, 'đ','d')
		SET @Name1 = REPLACE(@Name1, 'Đ','D')

		SET @Name2 = REPLACE(@Name2, 'ć','c')
		SET @Name2 = REPLACE(@Name2, 'č','c')
		SET @Name2 = REPLACE(@Name2, 'Ć','C')
		SET @Name2 = REPLACE(@Name2, 'Č','C')
		SET @Name2 = REPLACE(@Name2, 'ž','z')
		SET @Name2 = REPLACE(@Name2, 'Ž','z')
		SET @Name2 = REPLACE(@Name2, 'š','s')
		SET @Name2 = REPLACE(@Name2, 'Š','s')
		SET @Name2 = REPLACE(@Name2, 'đ','d')
		SET @Name2 = REPLACE(@Name2, 'Đ','D')

		SET @Name3 = REPLACE(@Name3, 'ć','c')
		SET @Name3 = REPLACE(@Name3, 'č','c')
		SET @Name3 = REPLACE(@Name3, 'Ć','C')
		SET @Name3 = REPLACE(@Name3, 'Č','C')
		SET @Name3 = REPLACE(@Name3, 'ž','z')
		SET @Name3 = REPLACE(@Name3, 'Ž','z')
		SET @Name3 = REPLACE(@Name3, 'š','s')
		SET @Name3 = REPLACE(@Name3, 'Š','s')
		SET @Name3 = REPLACE(@Name3, 'đ','d')
		SET @Name3 = REPLACE(@Name3, 'Đ','D')

		SET @Name4 = REPLACE(@Name4, 'ć','c')
		SET @Name4 = REPLACE(@Name4, 'č','c')
		SET @Name4 = REPLACE(@Name4, 'Ć','C')
		SET @Name4 = REPLACE(@Name4, 'Č','C')
		SET @Name4 = REPLACE(@Name4, 'ž','z')
		SET @Name4 = REPLACE(@Name4, 'Ž','z')
		SET @Name4 = REPLACE(@Name4, 'š','s')
		SET @Name4 = REPLACE(@Name4, 'Š','s')
		SET @Name4 = REPLACE(@Name4, 'đ','d')
		SET @Name4 = REPLACE(@Name4, 'Đ','D')

		SET @Name5 = REPLACE(@Name5, 'ć','c')
		SET @Name5 = REPLACE(@Name5, 'č','c')
		SET @Name5 = REPLACE(@Name5, 'Ć','C')
		SET @Name5 = REPLACE(@Name5, 'Č','C')
		SET @Name5 = REPLACE(@Name5, 'ž','z')
		SET @Name5 = REPLACE(@Name5, 'Ž','z')
		SET @Name5 = REPLACE(@Name5, 'š','s')
		SET @Name5 = REPLACE(@Name5, 'Š','s')
		SET @Name5 = REPLACE(@Name5, 'đ','d')
		SET @Name5 = REPLACE(@Name5, 'Đ','D')

		BEGIN TRY
			INSERT INTO
				Issa.dbo.PatientNames(
					PatientID,
					Name_1,
					Name_2,
					Name_3,
					Name_4,
					Name_5)
				VALUES (
					@PatientID,
					@Name1,
					@Name2,
					@Name3,
					@Name4,
					@Name5)
		END TRY
		BEGIN CATCH
			SET @error='Insert failed';
			RETURN;
		END CATCH;
		SET @error='';
	END;
END
GO
