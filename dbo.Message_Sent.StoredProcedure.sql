USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Message_Sent]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Message_Sent]
	@hl7_logs_out_id int,
	@error varchar(max) output
AS
BEGIN
SET NOCOUNT ON;
SET @error='';

/*

        This procedure is executed (if setting 'SQL'->'MESSAGE_SENT' enabled) 
		every time message is sent to some other HL7 system.
		Take good care it does not execute (that it returns) when not needed.

*/

DECLARE @examid INT;

BEGIN TRY

SELECT @examid=a.examid from HL7_QUEUE a join HL7_LOGS_OUT b 
ON a.ID=b.HL7_QUEUE_ID AND b.ID=@hl7_logs_out_id AND a.CUSTOM3='MEDIT';
IF(@examid IS NULL)
BEGIN
  SET @error='Exam not found';
  RETURN;
END;

/* 
   This is ISSA database table, not ISSA_HL7 - HL7QUEUE without _ in the name, synonym, not this database table.
   These entries are read by qsvc.exe - service to export ISSA images and add them to DICOM server output queue.
*/

INSERT INTO HL7QUEUE(qstatus,qdate,examid,examnumber,patientid,newexamnumber)
select case statusmask & 2 when 2 then '-200' else '-100' end,
dateadd(minute,1,getdate()),examid,examnumber,patientid,'qsvc' FROM 
EXAM WHERE EXAMID=@examid;

END TRY
BEGIN CATCH
  SET @error=ERROR_MESSAGE();
END CATCH;

END


GO
