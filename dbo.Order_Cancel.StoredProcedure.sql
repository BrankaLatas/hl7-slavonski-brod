USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Order_Cancel]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[Order_Cancel]
@hl7messageID INT,
@error VARCHAR(MAX) output
AS
BEGIN
SET NOCOUNT ON;
SET @error='';

DECLARE @joblistid int;

SET @joblistid=NULL;

DECLARE @dtab table(id int, joblistid int);

INSERT INTO @dtab(id,joblistid)
SELECT ROW_NUMBER()over(order by joblistid) id, joblistid FROM HL7_ORDERS WHERE ACCNO IN
(select FIELD_DATA from HL7_ORC where FIELD_POS=2 AND HL7_MESSAGE_ID=@hl7messageID);

IF(select count(1) from @dtab)<1
BEGIN
  SET @error='Can not cancel order, order does not exist';
  EXEC Shared_LogError @hl7messageID,@error,2,'Cancel order';
  RETURN;
END;
/**************************************************************************************
   BACKUP JOBLIST
**************************************************************************************/
DECLARE @x int,@total int;
SET @x=1;
SET @total=(Select count(1) from @dtab);

WHILE(@x<=@total)
BEGIN
  SELECT @joblistid=joblistid from @dtab WHERE id=@x;
  SET @x=@x+1;
  
  EXEC Joblist_Backup @joblistid,@error output;
  IF @error<>''
  BEGIN
     EXEC Shared_LogError @hl7messageID,@error,2,'Cancel order';
     RETURN;
  END;

  BEGIN TRY
     UPDATE JOBLIST SET STATUSMASK=STATUSMASK|256,EDITDATE=GETDATE(),EDITBY=1 WHERE JOBLISTID=@joblistid;
  END TRY
  BEGIN CATCH
     SET @error='Can not mark order as canceled ['+ERROR_MESSAGE()+']';
     EXEC Shared_LogError @hl7messageID,@error,2,'Cancel order';
    RETURN;
  END CATCH;

END;
/* All good */
/*----------------------------------------------------------------------------------------*/
SET @error=''; /* No error = OK for c# server */
EXEC Shared_LogError @hl7messageID,@error,998,'Cancel order';
/*----------------------------------------------------------------------------------------*/

END







GO
