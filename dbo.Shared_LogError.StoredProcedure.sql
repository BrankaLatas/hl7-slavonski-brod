USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Shared_LogError]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








------------------------------------------------------------------------------

CREATE procedure [dbo].[Shared_LogError]
@hl7messageID int,
@txt varchar(max),
@code int=NULL,
@operation varchar(256)='New order'
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
   IF @code is null
   BEGIN
     UPDATE HL7_LOGS_IN SET OPERATION=@operation, LOG_MSG=CASE WHEN ISNULL(LOG_MSG,'')='' THEN @txt ELSE LOG_MSG+char(13)+char(10)+@txt END, LOGTIME=GETDATE() WHERE HL7_MESSAGE_ID=@hl7messageID;
   END;
   ELSE
   BEGIN
     UPDATE HL7_LOGS_IN SET OPERATION=@operation, MSTATUS=@code,LOG_MSG=CASE WHEN ISNULL(LOG_MSG,'')='' THEN @txt ELSE LOG_MSG+char(13)+char(10)+@txt END, LOGTIME=GETDATE() WHERE HL7_MESSAGE_ID=@hl7messageID;
   END;
END TRY
BEGIN CATCH
END CATCH;

END







GO
