USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[GetReportSB]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[GetReportSB]
@examid int,
@error varchar(MAX) output,
@report varchar(max) output,
@issaany varchar(MAX) = NULL
AS
BEGIN
	DECLARE @html as varchar(MAX)
	DECLARE @BiRads AS VARCHAR(10)
	DECLARE @ACR    AS VARCHAR(10)
	DECLARE @ii AS INTEGER
	DECLARE @jj AS INTEGER

	DECLARE @ExamSummaryA AS VARCHAR(2000)
	DECLARE @ExamSummaryB AS VARCHAR(2000)
	DECLARE @ExamSummary AS VARCHAR(2000)
	SET @ExamSummary = ''
	SET @ExamSummaryA = ''
	SET @ExamSummaryB = ''

	DECLARE @PatientID       AS INTEGER
	DECLARE @PatientName     AS VARCHAR(50)
	DECLARE @PatientDobDay   AS VARCHAR(2)
	DECLARE @PatientDobMonth AS VARCHAR(2)
	DECLARE @PatientDobYear  AS VARCHAR(4)
	DECLARE @PatientAddress  AS VARCHAR(50)
	DECLARE @PatientMBO      AS VARCHAR(15)
	DECLARE @PatientDOB      AS VARCHAR(20)
	DECLARE @PatientSex		 AS VARCHAR(1)

	DECLARE @DatumPrimljenoDay     AS VARCHAR(2)
	DECLARE @DatumPrimljenoMonth   AS VARCHAR(2)
	DECLARE @DatumPrimljenoYear    AS VARCHAR(4)
	DECLARE @DatumPrimljeno        AS VARCHAR(20)
	DECLARE @DatumOdgovorenoDay    AS VARCHAR(2)
	DECLARE @DatumOdgovorenoMonth  AS VARCHAR(2)
	DECLARE @DatumOdgovorenoYear   AS VARCHAR(4)
	DECLARE @DatumOdgovorenoSat    AS VARCHAR(4)
	DECLARE @DatumOdgovorenoMinuta AS VARCHAR(4)
	DECLARE @DatumOdgovoreno       AS VARCHAR(40)

	DECLARE @ExamSentFrom          AS VARCHAR(100)
	DECLARE @ExamClinicalDiagnosis AS VARCHAR(200)
	DECLARE @ExamNumber			   AS VARCHAR(20)
	DECLARE @ExamStatus			   AS VARCHAR(200)
	DECLARE @ExamType			   AS VARCHAR(100)
	DECLARE @ExamDevice			   AS VARCHAR(100)
	DECLARE @ExamCurrentDisease	   AS VARCHAR(200)

	DECLARE @ExamResult            AS VARCHAR(MAX)
	DECLARE @ExamResultRTF         AS VARCHAR(MAX)
	DECLARE @ExamOpinion           AS VARCHAR(MAX)
	DECLARE @ExamMacroReport       AS VARCHAR(MAX)
	DECLARE @ExamMacroReportRTF    AS VARCHAR(MAX)
	DECLARE @ExamNote              AS VARCHAR(MAX)

	DECLARE @ExamTypist 		   AS VARCHAR(30)
	DECLARE @ExamTypistID		   AS INTEGER
	DECLARE @ExamResultDay		   AS VARCHAR(2)
	DECLARE @ExamResultMonth   	   AS VARCHAR(2)
	DECLARE @ExamResultYear        AS VARCHAR(4)
	DECLARE @ExamResultDate		   AS VARCHAR(20)
	DECLARE @ExamResultHour        AS VARCHAR(2)
	DECLARE @ExamResultMinute      AS VARCHAR(2)
	
	DECLARE @ExamPhysicianID      AS INTEGER
	DECLARE @ExamPhysicianName    AS VARCHAR(50)
	DECLARE @ExamPhysicianPrefix  AS VARCHAR(20)
	DECLARE @ExamPhysicianSufix   AS VARCHAR(20)
	DECLARE @ExamPhysicianSpecial AS VARCHAR(50)
	DECLARE @ExamPhysicianCode    AS VARCHAR(50)
	DECLARE @ExamPhysicianLicph  AS VARCHAR(50) 
	DECLARE @ExamPhysicianLevel   AS INTEGER

	DECLARE @ExamSpecialistID      AS INTEGER
	DECLARE @ExamSpecialistName    AS VARCHAR(80)
	DECLARE @ExamSpecialistPrefix  AS VARCHAR(50)
	DECLARE @ExamSpecialistSufix   AS VARCHAR(50)
	DECLARE @ExamSpecialistCode    AS VARCHAR(50)
	DECLARE @ExamSpecialistLicsp   AS VARCHAR(50) 
	DECLARE @ExamSpecialistSpecial AS VARCHAR(80)

	DECLARE @ExamReferingID      AS INTEGER
	DECLARE @ExamReferingName    AS VARCHAR(50)
	DECLARE @ExamReferingPrefix  AS VARCHAR(50)
	DECLARE @ExamReferingSufix   AS VARCHAR(50)
	DECLARE @ExamReferingSpecial AS VARCHAR(50)
	DECLARE @ExamRefering        AS VARCHAR(120)
	DECLARE @ExamReferingInstitution AS VARCHAR(100)
	DECLARE @ExamReferingAddress AS VARCHAR(200)
	
	DECLARE @AddendumID            AS INTEGER
	DECLARE @Addendum              AS VARCHAR(3000)
	DECLARE @DatumAddendumDay      AS VARCHAR(2)
	DECLARE @DatumAddendumMonth    AS VARCHAR(2)
	DECLARE @DatumAddendumYear     AS VARCHAR(4)
	DECLARE @DatumAddendumSat      AS VARCHAR(4)
	DECLARE @DatumAddendumMinuta   AS VARCHAR(4)
	DECLARE @DatumAddendum         AS VARCHAR(40)

	DECLARE @AddendumPhysicianID      AS INTEGER
	DECLARE @AddendumPhysicianName    AS VARCHAR(50)
	DECLARE @AddendumPhysicianPrefix  AS VARCHAR(20)
	DECLARE @AddendumPhysicianSufix   AS VARCHAR(20)
	DECLARE @AddendumPhysicianSpecial AS VARCHAR(50)
	DECLARE @AddendumPhysicianCode    AS VARCHAR(50)
	DECLARE @AddendumPhysicianLevel   AS INTEGER

	DECLARE @ExamTech AS VARCHAR(50)
	
	SET @BiRads = ISNULL(CAST((SELECT ExtraMemo_1 FROM Exam WHERE Exam.ExamID = @ExamID) AS VARCHAR(10)),'')
	SET @ACR    = ISNULL(CAST((SELECT ExtraMemo_2 FROM Exam WHERE Exam.ExamID = @ExamID) AS VARCHAR(10)),'')

	SET @PatientID       =      (SELECT PatientID                 FROM Exam    WHERE ExamID    = @ExamID)
	SET @PatientName     = CAST((SELECT Name                      FROM Patient WHERE PatientID = @PatientID) AS VARCHAR(50))
	SET @PatientDobDay   = CAST((SELECT DATEPART(DD, BirthDate)   FROM Patient WHERE PatientID = @PatientID) AS VARCHAR(2))
	SET @PatientDobMonth = CAST((SELECT DATEPART(MM, BirthDate)   FROM Patient WHERE PatientID = @PatientID) AS VARCHAR(2))
	SET @PatientDobYear  = CAST((SELECT DATEPART(YYYY, BirthDate) FROM Patient WHERE PatientID = @PatientID) AS VARCHAR(4))
	SET @PatientMBO      = CAST((SELECT ID                        FROM Patient WHERE PatientID = @PatientID) AS VARCHAR(15))
	SET @PatientAddress  = CAST((SELECT Address        FROM Patient WHERE PatientID = @PatientID) AS VARCHAR(50))
	SET @PatientSex      = CAST((SELECT SexID                	  FROM Patient WHERE PatientID = @PatientID) AS VARCHAR(1))


	SET @PatientName = REPLACE(@PatientName, '^', ' ')
	SET @PatientSex = REPLACE(REPLACE(REPLACE (@PatientSex, '1', 'M'), '2', 'Ž'), '3', 'O')
	
	SET @DatumPrimljenoDay   = CAST((SELECT DATEPART(DD,   ExamDate) FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(2))
	SET @DatumPrimljenoMonth = CAST((SELECT DATEPART(MM,   ExamDate) FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(2))
	SET @DatumPrimljenoYear  = CAST((SELECT DATEPART(YYYY, ExamDate) FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(4))
	
	SET @ExamResultDay   = CAST((SELECT DATEPART(DD,   ResultDate) FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(2))
	SET @ExamResultMonth = CAST((SELECT DATEPART(MM,   ResultDate) FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(2))
	SET @ExamResultYear  = CAST((SELECT DATEPART(YYYY, ResultDate) FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(4))
	SET @ExamResultHour = CAST((SELECT DATEPART(hour, ResultDate) FROM Exam where ExamID = @ExamID) AS VARCHAR(2))
	SET @ExamResultMinute = CAST((SELECT DATEPART(minute, ResultDate) FROM Exam where ExamID = @ExamID) AS VARCHAR(2))

	SET @DatumOdgovorenoDay    = CAST((SELECT DATEPART(DD,     VerifyDate) FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(2))
	SET @DatumOdgovorenoMonth  = CAST((SELECT DATEPART(MM,     VerifyDate) FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(2))
	SET @DatumOdgovorenoYear   = CAST((SELECT DATEPART(YYYY,   VerifyDate) FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(4))
	SET @DatumOdgovorenoSat    = CAST((SELECT DATEPART(HH,     VerifyDate) FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(4))
	SET @DatumOdgovorenoMinuta = CAST((SELECT DATEPART(MINUTE, VerifyDate) FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(4))
	IF LEN(@DatumOdgovorenoMinuta) = 1
	BEGIN
		SET @DatumOdgovorenoMinuta = '0' + @DatumOdgovorenoMinuta
	END
	IF LEN(@DatumOdgovorenoSat) = 1
	BEGIN
		SET @DatumOdgovorenoSat = '0' + @DatumOdgovorenoSat
	END
	

	SET @PatientDOB      = @PatientDobDay      + '. ' + @PatientDobMonth      + '. ' + @PatientDobYear      + '.'
	SET @DatumPrimljeno  = @DatumPrimljenoDay  + '. ' + @DatumPrimljenoMonth  + '. ' + @DatumPrimljenoYear  + '.'
	SET @DatumOdgovoreno = @DatumOdgovorenoDay + '.' + @DatumOdgovorenoMonth + '.' + @DatumOdgovorenoYear + '. ' + @DatumOdgovorenoSat + ':' + @DatumOdgovorenoMinuta
	SET @ExamResultDate = @ExamResultDay + '.' +  @ExamResultMonth + '. ' + @ExamResultYear + '.' + ' ' + @ExamResultHour + ':' + @ExamResultMinute
	--SET @DatumOdgovoreno = CAST((SELECT VerifyDate FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(20))

	SET @ExamSentFrom          = CAST((SELECT SentFrom       FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(100))
	SET @ExamClinicalDiagnosis = CAST((SELECT ClinicalDiagnosis FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(200))
	SET @ExamNumber			   = CAST((SELECT ExamNumber FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(20))
	SET @ExamStatus		       = CAST((SELECT ExamStatus FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(200))
	SET @ExamType		       = CAST((SELECT ExamType FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(100))
	SET @ExamDevice 		   = CAST((SELECT ExamDevice FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(100))
	SET @ExamCurrentDisease	   = CAST((SELECT CurrentDisease FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(200))


	SET @ExamPhysicianID  = (SELECT UsersID         FROM Exam WHERE ExamID = @ExamID)
	SET @ExamSpecialistID = (SELECT UsersOverViewID FROM Exam WHERE ExamID = @ExamID)
	SET @ExamReferingID   = (SELECT UsersIDRefering FROM Exam WHERE ExamID = @ExamID)
	SET @ExamTypistID     = (SELECT usersIdtype     FROM Exam WHERE ExamID = @ExamID)

	SET @ExamTech = CAST((SELECT Technician FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(50))
	
	SET @ExamTypist = CAST((SELECT Name    FROM Users WHERE UsersID = @ExamTypistID) AS VARCHAR(30))
	
	SET @ExamPhysicianName     = CAST((SELECT Name    FROM Users WHERE UsersID = @ExamPhysicianID ) AS VARCHAR(30))
	SET @ExamPhysicianPrefix   = CAST((SELECT Title   FROM Users WHERE UsersID = @ExamPhysicianID ) AS VARCHAR(30))
	SET @ExamPhysicianSufix    = ''
	SET @ExamPhysicianSpecial  = CAST((SELECT Special FROM Users WHERE UsersID = @ExamPhysicianID ) AS VARCHAR(50))
	SET @ExamPhysicianLevel    = CAST((SELECT LevelID FROM Users WHERE UsersID = @ExamPhysicianID ) AS VARCHAR(30))
	SET @ExamPhysicianCode     = CAST((SELECT Fax     FROM Users WHERE UsersID = @ExamPhysicianID ) AS VARCHAR(30))
	SET @ExamPhysicianLicph    = CAST((SELECT Telephone FROM Users WHERE UsersID = @ExamPhysicianID) AS VARCHAR(30))
	

	SET @ExamSpecialistName    = CAST((SELECT Name    FROM Users WHERE UsersID = @ExamSpecialistID) AS VARCHAR(50))
	SET @ExamSpecialistPrefix  = CAST((SELECT Title   FROM Users WHERE UsersID = @ExamSpecialistID) AS VARCHAR(30))
	SET @ExamSpecialistSufix   = ''
	SET @ExamSpecialistSpecial = CAST((SELECT Special   FROM Users WHERE UsersID = @ExamSpecialistID) AS VARCHAR(50))
	SET @ExamSpecialistCode    = CAST((SELECT Fax       FROM Users WHERE UsersID = @ExamSpecialistID) AS VARCHAR(50))
	SET @ExamSpecialistLicsp   = CAST((SELECT Telephone FROM Users WHERE UsersID = @ExamSpecialistID) AS VARCHAR(30))

	SET @ExamResult            = CAST((SELECT Result      FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(MAX))

	SET @ExamResult = REPLACE(@ExamResult, CHAR(13), '<br>')

	SET @ExamResultRTF         = CAST((SELECT ResultRTF   FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(MAX))
	SET @ExamOpinion           = CAST((SELECT Opinion     FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(MAX))
	SET @ExamMacroReport       = CAST((SELECT MacroReport FROM Exam WHERE ExamID = @ExamID) AS VARCHAR(MAX))

	SET @ExamReferingName      = CAST((SELECT Name    FROM Users WHERE UsersID = @ExamReferingID  ) AS VARCHAR(50))
	SET @ExamReferingPrefix    = CAST((SELECT Title   FROM Users WHERE UsersID = @ExamReferingID  ) AS VARCHAR(50))
	SET @ExamReferingSufix     = ''
	SET @ExamReferingSpecial   = CAST((SELECT Special FROM Users WHERE UsersID = @ExamReferingID  ) AS VARCHAR(50))
	SET @ExamReferingInstitution = (SELECT Institution FROM Users WHERE UsersID = @ExamReferingID)
	SET @ExamReferingAddress	= (SELECT Address FROM Users WHERE UsersID = @ExamReferingID)
	SET @ExamRefering = @ExamReferingName + ', dr.med.'
	IF @ExamReferingPrefix <> ''
	BEGIN
		SET @ExamRefering = @ExamReferingPrefix + ' ' + @ExamRefering
	END
	--IF @ExamReferingSpecial <> ''
	--BEGIN
		--SET @ExamRefering = @ExamRefering + ', ' + @ExamReferingSpecial
	--END

	SET @Addendum = ''
	-- Vadjenje podataka za Addendum
	IF (SELECT COUNT(AddendumID) FROM Addendum WHERE ExamID = @ExamID) > 0
	BEGIN
		SET @AddendumID           =      (SELECT TOP 1 AddendumID       FROM Addendum WHERE ExamID = @ExamID)
		SET @Addendum             =      (SELECT Addendum               FROM Addendum WHERE AddendumID = @AddendumID)
		SET @AddendumPhysicianID  =      (SELECT UsersID                FROM Addendum WHERE AddendumID = @AddendumID)
		SET @DatumAddendumDay     = CAST((SELECT DATEPART(DD,     Edit) FROM Addendum WHERE AddendumID = @AddendumID) AS VARCHAR(2))
		SET @DatumAddendumMonth   = CAST((SELECT DATEPART(MM,     Edit) FROM Addendum WHERE AddendumID = @AddendumID) AS VARCHAR(2))
		SET @DatumAddendumYear    = CAST((SELECT DATEPART(YYYY,   Edit) FROM Addendum WHERE AddendumID = @AddendumID) AS VARCHAR(4))
		SET @DatumAddendumSat     = CAST((SELECT DATEPART(HH,     Edit) FROM Addendum WHERE AddendumID = @AddendumID) AS VARCHAR(4))
		SET @DatumAddendumMinuta  = CAST((SELECT DATEPART(MINUTE, Edit) FROM Addendum WHERE AddendumID = @AddendumID) AS VARCHAR(4))
		IF LEN(@DatumAddendumMinuta) = 1
		BEGIN
			SET @DatumAddendumMinuta = '0' + @DatumAddendumMinuta
		END
		IF LEN(@DatumAddendumSat) = 1
		BEGIN
			SET @DatumAddendumSat = '0' + @DatumAddendumSat
		END
		SET @AddendumPhysicianName     = CAST((SELECT Name    FROM Users WHERE UsersID = @AddendumPhysicianID ) AS VARCHAR(30))
		SET @AddendumPhysicianPrefix   = CAST((SELECT Title   FROM Users WHERE UsersID = @AddendumPhysicianID ) AS VARCHAR(30))
		SET @AddendumPhysicianSufix    = ''
		SET @AddendumPhysicianSpecial  = CAST((SELECT Special FROM Users WHERE UsersID = @AddendumPhysicianID ) AS VARCHAR(50))
		SET @AddendumPhysicianLevel    = CAST((SELECT LevelID FROM Users WHERE UsersID = @AddendumPhysicianID ) AS VARCHAR(30))
		SET @AddendumPhysicianCode     = CAST((SELECT Fax     FROM Users WHERE UsersID = @AddendumPhysicianID ) AS VARCHAR(30))
	END

	SET @html = ''
	SET @html = @html + '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">'
	SET @html = @html + '<html>';
	SET @html = @html + '<meta name="viewport" content="width=device-width, initial-scale=1">'
	SET @html = @html + '<style>'
	SET @html = @html + '* {box-sizing: border-box;}'
	SET @html = @html + '.column {float: left; width: 31%; padding: 10px; height: 60px;}'
	SET @html = @html + '.row:after {content: ""; display: table; clear: both;}'
	SET @html = @html + '</style>'
	SET @html = @html + '	</head>' + CHAR(10);
	SET @html = @html + '<link rel="stylesheet" type="text/css" href="http://192.168.8.200/sb.css">' + CHAR(10)
	SET @html = @html + '	<body leftmargin=70px topmargin=70px onload="myFunction()">' + CHAR(10);
	SET @html = @html + '		<table class = "pageheader">' + CHAR(10);
	SET @html = @html + '			<tr>' + CHAR(10);
	SET @html = @html + '				<td class="logo" width="111" >' + CHAR(10);
	SET @html = @html + '				<img border="0" src="http://192.168.8.200/LogoS.jpg" width="113" height="67"></td>' + CHAR(10);
	SET @html = @html + '				<td>' + CHAR(10);
	SET @html = @html + '					<table class = "KBDheader"  width="594" style="line-height: 100%; margin-left: 0; margin-top: 0; margin-bottom: 0">' + CHAR(10);
	SET @html = @html + '						<tr>' + CHAR(10);
	SET @html = @html + '						<td class="h9a">' + CHAR(10);
	SET @html = @html + '							<p class="h9" align="center"><b>OPĆA BOLNICA "DR. JOSIP BENČEVIĆ" SLAVONSKI BROD, 046204628</b></td>' + CHAR(10);
	SET @html = @html + '						</tr>' + CHAR(10);
	SET @html = @html + '						<tr>' + CHAR(10);
	SET @html = @html + '							<td class="h9a" align="center"><b>ODJEL ZA KLINIČKU RADIOLOGIJU</b></td>' + CHAR(10);
	SET @html = @html + '						</tr>' + CHAR(10);
	SET @html = @html + '						<tr>' + CHAR(10);
	SET @html = @html + '							<td class="h9a" align="center">Voditelj: Drago Mitrečić, dr. med.</td>' + CHAR(10);
	SET @html = @html + '						</tr>' + CHAR(10);
	SET @html = @html + '						<tr>' + CHAR(10);
	SET @html = @html + '							<td class="h9a" align="center">tel.: 035/201-523 &#8226; email: radiologija@bolnicasb.hr &#8226; Andrije Štampara 42, 35000 Slavonski Brod</td>' + CHAR(10);
	SET @html = @html + '						</tr>' + CHAR(10);
	SET @html = @html + '					</table>' + CHAR(10);
	SET @html = @html + '				</td>' + CHAR(10);
	SET @html = @html + '			</tr>' + CHAR(10);
	SET @html = @html + '		</table>' + CHAR(10);
	SET @html = @html + '		<br>' + CHAR(10);
	SET @html = @html + '		<table class = "nobottom" width="710">' + CHAR(10);
	SET @html = @html + '			<tr>' + CHAR(10);
	SET @html = @html + '				<td class="h5"><b>Prezime i ime: ' + ISNULL(@PatientName,'') + '</b></td>' + CHAR(10);
	SET @html = @html + '				<td class="h5" width="327"><b>Spol: </b>' + ISNULL(@PatientSex,'') + '</td>' + CHAR(10);
	SET @html = @html + '			</tr>' + CHAR(10);
	SET @html = @html + '			<tr>' + CHAR(10);
	SET @html = @html + '				<td class="h5" id="mbo0" style="border:medium none #000000; "><b>MBO:  </b>' + ISNULL(@PatientMBO,'') + '</td>' + CHAR(10);
	SET @html = @html + '				<td class="h5" style=""border:medium none #000000; "><b>Datum rođenja: </b>' + ISNULL(@PatientDOB,'') + '</td>' + CHAR(10);
	SET @html = @html + '			</tr>' + CHAR(10);
	SET @html = @html + '			<tr>' + CHAR(10);
	SET @html = @html + '				<td class="h5" id="mbo" style=""border:medium none #000000; ">&nbsp;</td>' + CHAR(10);
	SET @html = @html + '				<td class="h5" style=""border:medium none #000000; "">&nbsp;</td>' + CHAR(10);
	SET @html = @html + '			</tr>' + CHAR(10);
	SET @html = @html + '		</table>' + CHAR(10);
	SET @html = @html + '		<table  class = "left" width="710">' + CHAR(10);
	SET @html = @html + '			<tr>' + CHAR(10);
	SET @html = @html + '	<td class="h5" style="border-left-color: #000000; border-right-color: #000000; border-top: 1px solid #000000; border-bottom-color: #000000" colspan="2">&nbsp;</td>' + CHAR(10);
	SET @html = @html + '			</tr>' + CHAR(10);
	SET @html = @html + '			<tr>' + CHAR(10);
	SET @html = @html + '				<td class="h5" width="380px"><b>Vrsta pregleda: </b>' + ISNULL(@ExamType,'') + '</td>' + CHAR(10);
	SET @html = @html + '				<td class="h5"><b>Uređaj: </b>' + ISNULL(@ExamDevice,'') + '</td>' + CHAR(10);
	SET @html = @html + '			</tr>' + CHAR(10);
	SET @html = @html + '			<tr>' + CHAR(10);
	SET @html = @html + '				<td class="h5"><b>Upućen od: </b>' + ISNULL(@ExamSentFrom,'') + '</td>' + CHAR(10);
	SET @html = @html + '				<td class="h5"><b>Datum pregleda: </b>' + ISNULL(@DatumPrimljeno,'') + '</td>' + CHAR(10);
	SET @html = @html + '			</tr>' + CHAR(10);
	SET @html = @html + '			<tr>' + CHAR(10);
	SET @html = @html + '				<td class="h5"><b>Uputna dijagnoza: </b>' + ISNULL(@ExamCurrentDisease,'') + '</td>' + CHAR(10);
	SET @html = @html + '				<td class="h5"><b>Broj pregleda: </b>' + ISNULL(@ExamNumber,'') + '</td>' + CHAR(10);
	SET @html = @html + '			</tr>' + CHAR(10);
	SET @html = @html + '		</table>' + CHAR(10);
	SET @html = @html + '		<table  class = "nobottom1" width="709">' + CHAR(10);
	SET @html = @html + '			<tr>' + CHAR(10);

	IF (@ExamTypist <> '')
	BEGIN
		SET @html = @html + '				<td class="h5" width="107px" id="administrator0" style="border:medium none #000000; "><b>Nalaz napisao: </b>' + ISNULL(@ExamTypist,'') + '</td>' + CHAR(10);
	END

	SET @html = @html + '			</tr>' + CHAR(10);
	SET @html = @html + '			<tr>' + CHAR(10);
	SET @html = @html + '				<td class="h5" width="107px" id="administrator" style="border:medium none #000000; ">&nbsp;</td>' + CHAR(10);
	SET @html = @html + '			</tr>' + CHAR(10);
	SET @html = @html + '		</table>' + CHAR(10);
	SET @html = @html + '		<table class = "top1" width="708">' + CHAR(10);
	SET @html = @html + '			<tr>' + CHAR(10);
	SET @html = @html + '				<td class = "h1" id = "naslov" style="border-left-color: #000000; border-right-color: #000000; border-top: 1px solid #000000; border-bottom-color: #000000" colspan="2">' + CHAR(10);
	SET @html = @html + '				&nbsp;</td>' + CHAR(10);
	SET @html = @html + '			</tr>' + CHAR(10);
	SET @html = @html + '			<tr>' + CHAR(10);
	SET @html = @html + '				<td class = "h1" id = "naslov">' + CHAR(10);
	SET @html = @html + '				<p align="center"><b><font size="4">NALAZ I MIŠLJENJE</font></b></td>' + CHAR(10);
	SET @html = @html + '			</tr>' + CHAR(10);
	SET @html = @html + '		</table>' + CHAR(10);
	SET @html = @html + '		<table  class = "nobottom" width="710">' + CHAR(10);
	SET @html = @html + '			<tr>' + CHAR(10);
	SET @html = @html + '				<td>' + ISNULL(@ExamResult,'') + '</td>' + CHAR(10);
	--- Birads i Acr
	IF (@BiRads <> '')
	BEGIN
		SET @html = @html + '
	
			<tr>
				<td>'
		SET @html = @html + 'BI-RADS: ' + @BiRads
		SET @html = @html + '
				</td>
			</tr>'
	END
	IF (@ACR <> '')
	BEGIN
		SET @html = @html + '
			<tr>
				<td>'
		SET @html = @html + 'ACR: ' + @ACR
		SET @html = @html + '
				</td>
			</tr>'
	END
	SET @html = @html + '            <tr>' + CHAR(10);
	SET @html = @html + '				<td class="h5"></td>' + CHAR(10);
	SET @html = @html + '			</tr>' + CHAR(10);
	SET @html = @html + '			<tr>' + CHAR(10);
	SET @html = @html + '				<td class="h5"><b>Datum i vrijeme nalaza: </b>' + ISNULL(@ExamResultDate,'') + '</td>' + CHAR(10);
		SET @html = @html + '						<br>' + CHAR(10);
	SET @html = @html + '			</tr>' + CHAR(10);
	SET @html = @html + '			<tr>' + CHAR(10);			
	SET @html = @html + '				<td class="h5" ><b>Radiološki tehnolog: </b>' + ISNULL(@ExamTech,'') + '</td>' + CHAR(10);
	SET @html = @html + '				<td></td>' + CHAR(10);
	SET @html = @html + '			</tr>' + CHAR(10);
	SET @html = @html + '		</table>' + CHAR(10);
	SET @html = @html + '		<br>' + CHAR(10);
	SET @html = @html + '		<table>' + CHAR(10);
	SET @html = @html + '			<tr>' + CHAR(10);
	SET @html = @html + '				<td valign="top">&nbsp;</td>  ' + CHAR(10);  
	SET @html = @html + '				<td width="1" >&nbsp;</td>' + CHAR(10);      
	SET @html = @html + '				<td width="485">' + CHAR(10);
	SET @html = @html + '					<div align="left">' + CHAR(10);
	SET @html = @html + '						<input name="button1" type="button" onclick="ClipBoard(&#39;%29%&#39;);" value="Slike" >' + CHAR(10);
	SET @html = @html + '						<br>' + CHAR(10);
	SET @html = @html + '						<input name="holdtext" type="text" style="color:white;border:0px" value="test" size="10" >' + CHAR(10);
	SET @html = @html + '					</div> ' + CHAR(10); 
	SET @html = @html + '				</td>' + CHAR(10);
	SET @html = @html + '			</tr>' + CHAR(10);
	SET @html = @html + '		</table>' + CHAR(10);
	SET @html = @html + '		<br>' + CHAR(10);
	SET @html = @html + CHAR(10)

	--Nalaz radio specijalizant
	IF @ExamPhysicianLevel = 1
	BEGIN
		IF @ExamSpecialistPrefix <> ''
		BEGIN
			SET @ExamSpecialistName = @ExamSpecialistPrefix + @ExamSpecialistName
		END

		SET @html = @html + '<div class="row" style = "text-align:center">' + CHAR(10)
		SET @html = @html + '<div class="column" style = "text-align:center" >' + CHAR(10)
		SET @html = @html + '<b>Liječnik:</b>' + CHAR(10)
		SET @html = @html + ' <p>' + ISNULL(@ExamPhysicianName,'') + '</p>' + CHAR(10)
		SET @html = @html + ' <p>specijalizant radiologije</p>' + CHAR(10)
		SET @html = @html + ' <p>' + ISNULL(@ExamPhysicianLicph, '')+ '</p>' + CHAR(10)
		SET @html = @html + ' </div>' + CHAR(10)
		SET @html = @html + '  <div class="column" style = "text-align:center">' + CHAR(10)
		SET @html = @html + '   <b>Specijalist:</b>' + CHAR(10)
		SET @html = @html + '   <p>' + ISNULL(@ExamSpecialistName,'') + '</p>' + CHAR(10)
		SET @html = @html + '   <p>' + ISNULL(@ExamSpecialistSpecial,'') + '</p>' + CHAR(10)
		SET @html = @html + '   <p>' + ISNULL(@ExamSpecialistCode,'') + '</p>' + CHAR(10)
		SET @html = @html + '  	<p>' + ISNULL(@ExamSpecialistLicsp,'') + '</p>' + CHAR(10)
		SET @html = @html + '  </div>' + CHAR(10)
		SET @html = @html + '</div>' + CHAR(10)

	END

	--Sve radio specijalist
	IF @ExamPhysicianLevel = 8
	BEGIN
		IF @ExamSpecialistPrefix <> ''
		BEGIN
			SET @ExamSpecialistName = @ExamSpecialistPrefix + ' ' + @ExamSpecialistName
		END

		SET @html = @html + '<table style = "width:710px; text-align:center">' + CHAR(10)
		SET @html = @html + '<tr>' + CHAR(10)
		SET @html = @html + '<td class="doctor" width = "355">&nbsp;</td><td class="doctor" width = "345"><b>Specijalist:</b></td>' + CHAR(10)
		SET @html = @html + '</tr>' + CHAR(10)
		SET @html = @html + '<tr>' + CHAR(10)
		SET @html = @html + '	<td class="doctor">' + '</td><td class="doctor">' + ISNULL(@ExamSpecialistName,'') + CHAR(10)
		SET @html = @html + '</tr>' + CHAR(10)
		SET @html = @html + '<tr>' + CHAR(10)
		SET @html = @html + '	<td></td><td class="doctor">' + ISNULL(@ExamSpecialistSpecial,'') + '</td>' + CHAR(10)
		SET @html = @html + '</tr>' + CHAR(10)
		SET @html = @html + '<tr>' + CHAR(10)
		SET @html = @html + '	<td></td><td class="doctor">' + ISNULL(@ExamSpecialistCode,'') + '</td>' + CHAR(10)
		SET @html = @html + '</tr>' + CHAR(10)
		SET @html = @html + '<tr>' + CHAR(10)
		SET @html = @html + '	<td></td><td class="doctor">' + ISNULL(@ExamSpecialistLicsp,'') + '</td>' + CHAR(10)
		SET @html = @html + '</tr>' + CHAR(10)
		SET @html = @html + '</table>' + CHAR(10)

	END

	--------------addendum

	DECLARE @Nadopuna AS VARCHAR(MAX)
	DECLARE @NadopunaDatum AS VARCHAR(50)

	SET @Nadopuna = ''
	SET @NadopunaDatum = @DatumAddendumDay + '.' +  @DatumAddendumMonth + '.' + @DatumAddendumYear +'. ' + @DatumAddendumSat + ':' + @DatumAddendumMinuta 
	IF @Addendum <> ''
	BEGIN
		SET @Nadopuna = '<b>Addendum: </b>' + '<br>' + @Addendum + '<br>' + '(' + @AddendumPhysicianName + ', dr. med.' + ' - ' + @NadopunaDatum + ')' 
		SET @html = @html + '<table>' + CHAR(10)
		SET @html = @html + '	<tr>' + CHAR(10)
		SET @html = @html + '		<td><br><br></td>' + CHAR(10)
		SET @html = @html + '	</tr>' + CHAR(10)
		SET @html = @html + '	<tr>' + CHAR(10)
		SET @html = @html + '		<td class="normal" width="661px">' + '<br>' + ISNULL(@Nadopuna,'')  + '</td>' + CHAR(10)
		SET @html = @html + '	</tr>' + CHAR(10)
		SET @html = @html + '</table>' + CHAR(10)
	END
	
	SET @html = @html + '		<br>' + CHAR(10);
	SET @html = @html + '		<br>' + CHAR(10);
	SET @html = @html + '		<br>' + CHAR(10);
	SET @html = @html + '		<footer>'
	SET @html = @html + '		<table class = "footer">' + CHAR(10);
	SET @html = @html + '			<tr>' + CHAR(10);
	SET @html = @html + '				<td class="h9b">' + CHAR(10);
	SET @html = @html + '				<b>Ovaj nalaz je izdan elektronskim putem i kao takav valjan je bez potpisa i pečata ovlaštenog liječnika.</b>' + CHAR(10);
	SET @html = @html + '				</td>' + CHAR(10);
	SET @html = @html + '			</tr>' + CHAR(10);
	SET @html = @html + '		</table>' + CHAR(10);
	SET @html = @html + '		</footer>'
	SET @html = @html + '	</body>' + CHAR(10);
	SET @html = @html + '</html>' + CHAR(10);	
	SET @html = @html + '' + CHAR(10);
	SET @html = @html + '</body>' + CHAR(10);
	SET @html = @html + '</html>' + CHAR(10);

	SET @html = REPLACE(@html, 'Ž' collate Croatian_CS_AS, '&#381;');
	SET @html = REPLACE(@html, 'Š' collate Croatian_CS_AS, '&#352;');
	SET @html = REPLACE(@html, 'Č' collate Croatian_CS_AS, '&#268;');
	SET @html = REPLACE(@html, 'Ć' collate Croatian_CS_AS, '&#262;');
	SET @html = REPLACE(@html, 'Đ' collate Croatian_CS_AS, '&#272;');
	SET @html = REPLACE(@html, 'ž' collate Croatian_CS_AS, '&#382;');
	SET @html = REPLACE(@html, 'š' collate Croatian_CS_AS, '&#353;');
	SET @html = REPLACE(@html, 'č' collate Croatian_CS_AS, '&#269;');
	SET @html = REPLACE(@html, 'ć' collate Croatian_CS_AS, '&#263;');
	SET @html = REPLACE(@html, 'đ' collate Croatian_CS_AS, '&#273;');
	
	SET @error = '';
	SET @Report = @html;
  
END



GO
