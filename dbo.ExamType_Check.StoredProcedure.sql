USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[ExamType_Check]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[ExamType_Check]
	@hl7messageID int,
	@examtypeID   int          output,
	@examtypeCODE varchar(256) output,
	@examtypeNAME varchar(256) output,
	@error        varchar(max) output,
	@segment_cnt  int = null /* 1=default, OBR segments can repeat in message (ORC, OBR pairs) */
AS
BEGIN
	SET NOCOUNT ON;
	SET @error='';
	DECLARE @ctemp varchar(max);
	/*--------------------------------------------------------------------------------*/
	/* 1018^CT Abdomena s gastrografinom */
	-- We read the ordered exam type from OBR-4 field
	-- Be careful, we read the whole thing, code and exam type, separated by ^
	SELECT
			@ctemp=FIELD_DATA
		FROM
			HL7_OBR 
		WHERE
			    SEGMENT_CNT    = isnull(@segment_cnt,1)
			AND FIELD_POS      = 4 
			AND HL7_MESSAGE_ID = @hl7messageID;

	DECLARE @pos int;
	-- So, we check where is our ^ located within the OBR-4
	SET @pos = CHARINDEX('^',@ctemp);
	-- If position is 0, that means thre is no ^, bugger off!
	if(isnull(@pos,0)=0)
	BEGIN
		SET @error='Invalid exam type code';
		RETURN;
  	END;

	-- OK, now we try to parse the field and get the code and exam type
	BEGIN TRY
		SET @examtypeCODE = LEFT (@ctemp,@pos-1);
		SET @examtypeNAME = RIGHT(@ctemp,len(@ctemp)-@pos);
	END TRY
	BEGIN CATCH
		SET @error='Failed to parse exam type';
		RETURN;
	END CATCH;

	-- If code part is empty, we raise drama.
	IF isnull(@examtypeCODE,'')=''
	BEGIN
		SET @error        = 'Exam type ['+isnull(@ctemp,'')+'] invalid';
		SET @examtypeID   = NULL;
		SET @examtypeCODE = NULL;
		SET @examtypeNAME = NULL;
   		RETURN;
	END;

	/*--------------------------------------------------------------------------------*/
	/* CODEVALUE is not unique in ExamType table */

	-- OK, now we found our ExamType code and ExamType name, we try to find them in our ExamType table
	
	SELECT TOP 1 
			@examtypeID   = ExamTypeId,
			@examtypeNAME = [NAME]
		FROM
			EXAMTYPE
		WHERE
			CODEVALUE = @examtypeCODE; /* and Name=@examtypeNAME */

	IF(@examtypeID is null)
	BEGIN
		SET @error='Exam type with CODE ['+isnull(@examtypeCODE,'')+'] not found';
		RETURN;
	END;
/*--------------------------------------------------------------------------------*/
END;






GO
