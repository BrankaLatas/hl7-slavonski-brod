USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Order_New]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--updated
CREATE PROCEDURE [dbo].[Order_New]
	@hl7messageID INT=NULL,
	@error VARCHAR(MAX) output
AS
BEGIN
	SET NOCOUNT ON;

	/*--------------------------------------------------------------------------------
  	ORDER SHOULD HAVE SAME ACCESSION NUMBER (1) OR NOT (0)
	--------------------------------------------------------------------------------*/
	DECLARE @SameAccno INT=0, @FirstAccno VARCHAR(256)=NULL;

	/*--------------------------------------------------------------------------------
 	Data that we extract from received message. 
 	Joblist.UID, Exam.UID, Exam.ExamID, Exam.UsersID will be generated inside functions just before insert
	--------------------------------------------------------------------------------*/
	DECLARE @FINTABLE HL7_TABLE1; /* HL7_TABLE1 = user defined table type */

	DECLARE @PatientID        int,
		    @MessageType      varchar(256),
		    @ReferringID      int,
		    @ExamTypeID       int,
		    @ExamTypeCode     varchar(256),
		    @ExamTypeName     varchar(256),
		    @ExamDeviceID     int=NULL, 
		    @ExamDeviceName   varchar(256)=NULL,
		    @AccessionnNumber varchar(256),
		    @AdmissionID      varchar(256),
		    @BillingNO        varchar(256),
		    @PayType          INT,
		    @PatientType      varchar(32)='';

	DECLARE @AccessionNumberHIS  VARCHAR(256),
			@AccessionNumberIssa VARCHAR(256),
			@AdmissionType       VARCHAR(256);

	DECLARE @ctemp            varchar(max);

	/*--------------------------------------------------------------------------------
	  NEW ORDER (@cmd='NW')
	--------------------------------------------------------------------------------*/

	DECLARE @tids table(accno varchar(256));

	-- So, we begin by getting AccessionNumber for the HIS order from ORC-2 field
	-- Theoretically, message could have several ORC segments with different Accession Numbers
	-- We are putting all of them to table tids
	INSERT INTO
			@tids(accno)
		SELECT
				FIELD_DATA 
			FROM 
				HL7_ORC 
			WHERE
				    FIELD_POS      = 2
				AND HL7_MESSAGE_ID = @hl7messageID;

	-- Check, maybe we have this order already
	-- First we check in our JobList table
	IF(SELECT COUNT(1) FROM HL7_ORDERS a JOIN @tids c ON c.accno=a.ACCNO JOIN JOBLIST b ON a.JOBLISTID=b.JOBLISTID AND b.STATUSMASK&256=0)>0
	BEGIN
		SET @error='Order already exists';
		EXEC Shared_LogError @hl7messageID,@error,2;
		RETURN;
	END;

	-- Also, we check in our Exam table, who knows
	IF(SELECT COUNT(1) FROM HL7_ORDERS a JOIN @tids c ON c.accno=a.ACCNO JOIN EXAM b ON a.EXAMID=b.EXAMID)>0
	BEGIN
		SET @error='Order already exists';
		EXEC Shared_LogError @hl7messageID,@error,2;
		RETURN;
	END;

	/*--------------------------------------------------------------------------------
	 PATIENT
	--------------------------------------------------------------------------------*/
	SET @patientID=NULL;
	SET @error='';

	-- We check patient data, add patient if it doesn't exist (I guess)
	EXEC Patient_Check
		 	@hl7messageID,
		 	@patientID output,
		 	@error     output;

	IF isnull(@patientID,0)<1
	BEGIN
		SET @error='Failed to process patient data ['+@error+']';
		EXEC Shared_LogError @hl7messageID,@error,2;
		RETURN;
	END;

	-- We get patient type (OK, it is actually admission type) from PV1-2
	/* E=hitni, I=stacionarni (bolnicki), O=poliklinicki (ambulantni), U=nepoznati */
	SELECT 
			@patientTYPE = FIELD_DATA
		FROM
			HL7_PV1
		WHERE
				SEGMENT_CNT    = 1
			AND FIELD_POS      = 2
			AND HL7_MESSAGE_ID = @hl7messageID;
	IF (@PatientType <> 'O') AND (@PatientType <> 'I')
	BEGIN
		SET @PatientType = 'O';
	END;

	/*---------------------------------------------------------------------------------------
	 REPEATING SEGMENTS NOW (ORC/OBR) ...
	----------------------------------------------------------------------------------------*/
	DECLARE @cnt int,
			@x   int;

	-- Now we check how many ORC segments are in the message
	SELECT
			@cnt = max(SEGMENT_CNT) 
		FROM
			HL7_ORC
		WHERE 
			HL7_MESSAGE_ID = @hl7messageID;

	SET @x = 0;

	-- And now we process each of those segments
	WHILE @x < @cnt
	BEGIN
		SET @x=@x+1;
		/*----------------------------------------------------------------------------------------
		EXAM TYPE, DEVICE
		----------------------------------------------------------------------------------------*/

		-- First we try to figure out the requested exam type from the incoming message
		-- We put that information into @examtypeID, @examtypeCODE and @examtypeNAME variables
		SET @examtypeID=NULL;
		SET @examtypeCODE=NULL;
		SET @examtypeNAME=NULL;
		SET @error='';
		EXEC ExamType_Check
			@hl7messageID,
			@examtypeID   output,
			@examtypeCODE output,
			@examtypeNAME output,
			@error        output,
			@x;

   		IF isnull(@examtypeID,0)<1
		BEGIN
			SET @error='Failed to process exam type data: '+@error;
			EXEC Shared_LogError @hl7messageID,@error,2;
			RETURN;
		END;
 
		-- Now we get the ExamDevice for this order
		-- In here it is done based on the Exam Type
		IF(@examdeviceID IS NULL)
			EXEC ExamDevice_Check
				@examtypeID,
				@examdeviceID   output,
				@examdeviceNAME output,
				@error          output;

		/*----------------------------------------------------------------------------------------
		ACCESSION NUMBER
		----------------------------------------------------------------------------------------*/
		SELECT
				@accessionnnumber = FIELD_DATA
			FROM
				HL7_ORC
			WHERE
					SEGMENT_CNT  = @x
				AND FIELD_POS    = 2
				AND HL7_MESSAGE_ID = @hl7messageID;

		SELECT
				@AccessionNumberHIS = FIELD_DATA
			FROM
				HL7_ORC
			WHERE
					SEGMENT_CNT  = @x
				AND FIELD_POS    = 2
				AND HL7_MESSAGE_ID = @hl7messageID;

		-- Your Accession Number is empty, you assholes!
		IF isnull(@AccessionNumberHIS,'')=''
		BEGIN
			SET @error='Accession no. is empty';
			EXEC Shared_LogError @hl7messageID,@error,2;
			RETURN;
		END;

		-- I have no idea what this does, must check.
		IF(@SameAccno=1)
		BEGIN
			IF(ISNULL(@FirstAccno,'')='')
			SET @FirstAccno=@accessionnnumber;
		END;
		/*----------------------------------------------------------------------------------------
		ORDER DATE, TIME, DATETIME
		----------------------------------------------------------------------------------------*/
		/* 20190116112034.0000+0100 */

		SET @ctemp=null;
		-- We read order time from ORC-9
		SELECT
				@ctemp = SUBSTRING(FIELD_DATA,1,14)
			FROM
				HL7_ORC
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 9
				AND HL7_MESSAGE_ID = @hl7messageID;
   
		-- It could be that time in incoming message is empty
		-- We can make drama out of it (see commented code) or we could just say, who cares
		-- and put the current datetime
		IF isnull(@ctemp,'')=''
		BEGIN
			/*
				SET @error='Order date and time is empty';
				EXEC Shared_LogError @messageID,@error,2;
			*/
			SET @ctemp = null;
			SELECT @ctemp=CONVERT(varchar,GETDATE(),112)+REPLACE(CONVERT(varchar,GETDATE(),114),':',''); /* 112 = yyyymmdd, 114=hh:mi:ss:mmm(24h) */
		END;
		ELSE
		BEGIN
			/* No secods */
			IF(LEN(@ctemp)=12)
			SET @ctemp=@ctemp+'00';
		END;

		DECLARE @s1 varchar(32),@s2 varchar(32),@s3 varchar(32);
		DECLARE @d1 DATETIME,@t1 DATETIME,@dt1 DATETIME;
   
		SET @d1=NULL;
		SET @t1=NULL;
		SET @dt1=NULL;

		BEGIN TRY
			SET @s1=SUBSTRING(@ctemp,1,8); /* just date */
			SET @s2='1899-12-30 '+SUBSTRING(@ctemp,9,2)+':'+SUBSTRING(@ctemp,11,2)+':'+SUBSTRING(@ctemp,13,2); /* just time */
			SET @s3=REPLACE(@s2,'1899-12-30 ',@s1+' '); /* date and time */
			SELECT @d1=CONVERT(datetime,@s1,112),@t1=CONVERT(datetime,@s2,120),@dt1=CONVERT(datetime,@s3,120);
		END TRY
		BEGIN CATCH
			SET @ERROR='Failed to convert ['+@s1+']['+@s2+']['+@s3+'] to DATE,TIME,DATETIME';
			EXEC Shared_LogError @hl7messageID,@error,2;
			RETURN;
		END CATCH;
		-- Whatevs, time is set now, move it on.

		/*----------------------------------------------------------------------------------------
		ADMISSION ID = URGENT OR NOT
		----------------------------------------------------------------------------------------*/
		--We get the admission type from TQ1-9
		SELECT TOP 1
				@AdmissionID = FIELD_DATA
			FROM
				HL7_TQ1
			WHERE
					SEGMENT_CNT   = @x
				AND FIELD_POS     = 9
				AND HL7_MESSAGE_ID = @hl7messageID;

		/*----------------------------------------------------------------------------------------
		SentFrom
		1) PV1-7  = 0100340^Vuljak^Mladen
		2) ORC-13 = 1669^690102GA02^^^^^^^Citološka amb. - ginekološka 2 PZZŽ  - ambulantni tj. svi ostali

		E=hitni, I=stacionarni (ambulantni), O=poliklinicki (bolnicki), U=nepoznati
		----------------------------------------------------------------------------------------*/
		DECLARE @ReferringGPFirstName varchar(max) = ''; -- Doktor sa "crvene uputnice"
		DECLARE @ReferringGPLastName  varchar(max) = ''; -- Doktor sa "crvene uputnice"
		DECLARE @ReferringGPCode      varchar(max) = ''; -- Doktor sa "crvene uputnice"

		DECLARE @ReferringUserID_HIS    varchar(max) = ''; -- 
		DECLARE @ReferringUserCode_HIS  varchar(max) = '';
		DECLARE @ReferringUserFirstName varchar(max) = '';
		DECLARE @ReferringUserLastName  varchar(max) = '';
		
		DECLARE @OrderingFacilityID_HIS   varchar(max) = ''; -- Uputno radiliste - ID
		DECLARE @OrderingFacilityName_HIS varchar(max) = ''; -- Uputno radiliste - Naziv
		DECLARE @OrderingFacilityCode_HIS varchar(max) = ''; -- Uputno radiliste - Sifra

		DECLARE @ReceivingFacilityID_HIS   varchar(max) = ''; -- Prihvatno radiliste - ID
		DECLARE @ReceivingFacilityName_HIS varchar(max) = ''; -- Prihvatno radiliste - Naziv
		DECLARE @ReceivingFacilityCode_HIS varchar(max) = ''; -- Prihvatno radiliste - Sifra

		DECLARE @RequisitionUID varchar(max) = ''; -- ID Cezih uputnice
		--------------
		SELECT @OrderingFacilityID_HIS = COL_DATA
			FROM HL7_ORC CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 13
				AND HL7_MESSAGE_ID = @hl7messageID
				AND COL_NO         = 1;
		SELECT @OrderingFacilityCode_HIS = COL_DATA
			FROM HL7_ORC CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 13
				AND HL7_MESSAGE_ID = @hl7messageID
				AND COL_NO         = 2;
		SELECT @OrderingFacilityName_HIS = COL_DATA
			FROM HL7_ORC CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 13
				AND HL7_MESSAGE_ID = @hl7messageID
				AND COL_NO         = 9;
		--------------------
		SELECT @ReceivingFacilityID_HIS = COL_DATA
			FROM HL7_ORC CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 17
				AND HL7_MESSAGE_ID = @hl7messageID
				AND COL_NO         = 1;
		SELECT @ReceivingFacilityCode_HIS = COL_DATA
			FROM HL7_ORC CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 17
				AND HL7_MESSAGE_ID = @hl7messageID
				AND COL_NO         = 2;
		SELECT @ReceivingFacilityName_HIS = COL_DATA
			FROM HL7_ORC CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 17
				AND HL7_MESSAGE_ID = @hl7messageID
				AND COL_NO         = 9;
		--------------------
		SELECT @ReferringGPFirstName = COL_DATA
			FROM HL7_ORC CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 12
				AND HL7_MESSAGE_ID = @hl7messageID
				AND COL_NO         = 3;
		SELECT @ReferringGPLastName  = COL_DATA
			FROM HL7_ORC CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 12
				AND HL7_MESSAGE_ID = @hl7messageID
				AND COL_NO         = 2;
		SELECT @ReferringGPCode      = COL_DATA
			FROM HL7_ORC CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 12
				AND HL7_MESSAGE_ID = @hl7messageID
				AND COL_NO         = 1;
		---------------
		SELECT @ReferringUserID_HIS     = COL_DATA
			FROM HL7_ORC CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 12
				AND HL7_MESSAGE_ID = @hl7messageID
				AND COL_NO         = 1;
		SELECT @ReferringUserCode_HIS   = COL_DATA
			FROM HL7_ORC CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 12
				AND HL7_MESSAGE_ID = @hl7messageID
				AND COL_NO         = 16;
		SELECT @ReferringUserFirstName   = COL_DATA
			FROM HL7_ORC CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 12
				AND HL7_MESSAGE_ID = @hl7messageID
				AND COL_NO         = 3;
		SELECT @ReferringUserLastName     = COL_DATA
			FROM HL7_ORC CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 12
				AND HL7_MESSAGE_ID = @hl7messageID
				AND COL_NO         = 2;
		----------------------
		SELECT @RequisitionUID = FIELD_DATA
			FROM  HL7_PV1
			WHERE SEGMENT_CNT    = 1
			  AND FIELD_POS      = 5
			  AND HL7_MESSAGE_ID = @hl7messageID;
		---------------------

		DECLARE @SentFrom varchar(max) = '';
		DECLARE @Condition1 INTEGER = 0;
		DECLARE @Condition2 INTEGER = 0;
		DECLARE @Condition3 INTEGER = 0;
		
		SET @ReferringID=NULL;
		IF (@PatientType = 'O')
		BEGIN
			-- If PV1-2 is "0", we have three options:
			--  - Admission is regular outpatient with filled requisition form (Crvena uputnica)
			--    In this case, following applies:
			--      - PV1-5 has Requisition UID
			--      - ORC-13 and ORC-17 are identical
			--      - TQ1-9  is equal to "R"
			--  - Admission is regular outpatient, sent from some other specialist office
			--    In this case, following applies:
			--      - PV1-5 has Requisition UID
			--      - ORC-13 and ORC-17 are different
			--      - ORC-13 is the Ordering facility
			--      - TQ1-9  is equal to "R"
			--  - Admission is urgent sent from some other specialist office
			--    In this case, following applies:
			--      - PV1-5 is empty
			--      - ORC-13 and ORC-17 are different
			--      - ORC-13 is the Ordering facility
			--      - TQ1-9  is equal to "S"
			--SET @ReferringID = 55;
			--SET @SentFrom = @ReferringGPFirstName + ' ' + @ReferringGPLastName + ' (' + @ReferringGPCode + ')';
			
			IF ISNULL(@RequisitionUID,'') <> ''
			BEGIN
				SET @Condition1 = 1;
			END;
			IF ISNULL(@OrderingFacilityID_HIS,'') <> ISNULL(@ReceivingFacilityID_HIS,'')
			BEGIN
				SET @Condition2 = 1;
			END;
			IF ISNULL(@OrderingFacilityID_HIS,'') = ISNULL(@ReceivingFacilityID_HIS,'')
			BEGIN
				IF ISNULL(@OrderingFacilityID_HIS,'') <> ''	
				BEGIN
					SET @Condition2 = 2;
				END;
			END;
			-- Ambulantni pacijent sa crvenom uputnicom
			--IF (@Condition1 = 1) AND (@Condition2 = 2)
			IF (@Condition2 = 2)
			BEGIN
				SET @SentFrom = @ReferringGPFirstName + ' ' + @ReferringGPLastName + ' (' + @ReferringGPCode + ')';
				SET @ReferringID = 55;
			END;

			-- Ambulantni pacijent iz druge ambulante
			IF (@Condition1 = 1) AND (@Condition2 = 1)
			BEGIN
				SET @SentFrom = @OrderingFacilityName_HIS + ' (' + @OrderingFacilityID_HIS + ') ';
			END;

			-- OHBP pacijent
			IF (@Condition1 = 0) AND (@Condition2 = 1)
			BEGIN
				SET @SentFrom = @OrderingFacilityName_HIS + ' (' + @OrderingFacilityID_HIS + ') ';
			END;
		END;
		IF (@patientTYPE = 'I')
		BEGIN
			SELECT @sentfrom = COL_DATA
				FROM HL7_ORC CROSS APPLY Split(FIELD_DATA,'^') 
				WHERE
						SEGMENT_CNT    = @x
					AND FIELD_POS      = 19
					AND HL7_MESSAGE_ID = @hl7messageID
					AND COL_NO         = 2;
		END;
   	
		/*----------------------------------------------------------------------------------------
		BILLING_REQUEST_NO = MCS_610105/0168386B20980000008C (PV1-5)
		----------------------------------------------------------------------------------------*/
		-- OK, this is Cezih ID of the requisition form
		-- I pick it up from PV1-5
		SELECT 
				@billingNO = FIELD_DATA
			FROM
				HL7_PV1
			WHERE
					SEGMENT_CNT    = 1
				AND FIELD_POS      = 5
				AND HL7_MESSAGE_ID = @hl7messageID;

		-- If it is longer than 64 characters, bugger off, I will not take it.
		IF(LEN(@billingNO)>64)
			SET @billingNO='';

		/*----------------------------------------------------------------------------------------
		PAYTYPE = HZZO / Placa sam / Suradna ustanova
		----------------------------------------------------------------------------------------*/
		-- This is the field where In2 HIS indicates who will be charged for a particular procedure.
		-- We read it from ORC-10
		SET @ctemp='';
		SELECT
				@ctemp = ISNULL(FIELD_DATA,'')
			FROM
				HL7_ORC
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 10
				AND HL7_MESSAGE_ID = @hl7messageID;

		-- You woud probably jhave to update this with other institutions
		IF(@ctemp='1')
			SET @ctemp='HZZO';
		ELSE
		IF(@ctemp='2')
			SET @ctemp='Plaća sam';
		ELSE
		IF(@ctemp='3')
			SET @ctemp='Suradna ustanova';
			ELSE
			SET @ctemp='';

		SET @paytype=NULL;
		SELECT 
				@paytype = PayTypeID
			FROM
				ExamPayTypeList
			WHERE
				Name = @ctemp;

		/*----------------------------------------------------------------------------------------
		REFERRING
		----------------------------------------------------------------------------------------*/
		-- Now we take care od the referring physician
		-- This is done by directly forwarding the MessageID to Users_Check procedure
		-- Still, I have no idea how this thing knows who the referring is.
		IF @referringID is null
		BEGIN
			SET @referringID=NULL;
			SET @error='';
			EXEC Users_Check
				@hl7messageID,
				@referringID   output,
				@error         output,
				@x;
		END;
	
		/*----------------------------------------------------------------------------------------
		RESULTDIAG_ICD = R10^Boli u trbuhu i u zdjelici^I10
		----------------------------------------------------------------------------------------*/
		--This is ICD-10 diagnosis which is incoming from the HIS order
		-- We read it from DG1-3, first subfield
		DECLARE @RESULTDIAG_ICD varchar(max);
		SET @RESULTDIAG_ICD = null;
		SELECT
				@RESULTDIAG_ICD = COL_DATA 
			FROM
				HL7_DG1 CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE 
				SEGMENT_CNT    = @x
			AND FIELD_POS      = 3
			AND HL7_MESSAGE_ID = @hl7messageID
			AND COL_NO         = 1;
	   
		/*----------------------------------------------------------------------------------------
		x 189^Kontracepcija drugo^^192^Klinička dijagnoza
		DG1|1||M16^Koksartroza /artroza kuka/^I10|||A
		----------------------------------------------------------------------------------------*/
		--We take the Current Disease from incoming HIS order
		-- We read it from DG1-3 field
		DECLARE @currentdesease varchar(max)='';
   
		SELECT 
				@currentdesease = REPLACE(FIELD_DATA,'^',' ')
			FROM
				HL7_DG1
			WHERE
					SEGMENT_CNT    = 1
				AND FIELD_POS      = 3
				AND HL7_MESSAGE_ID = @hl7messageID;

		/*----------------------------------------------------------------------------------------
		189^Kontracepcija drugo^^192^Klinička dijagnoza
		----------------------------------------------------------------------------------------*/
		-- We take Preceeding Exams from incoming HIS order
		-- We read it from OBR-45 field
		DECLARE @PreceedingExams varchar(max);

		SELECT
				@PreceedingExams = COL_DATA 
   			FROM
   				HL7_OBR CROSS APPLY Split(FIELD_DATA,'^') 
   			WHERE
   					SEGMENT_CNT    = @x
   				AND FIELD_POS      = 45
   				AND HL7_MESSAGE_ID = @hl7messageID
   				AND COL_NO          = 2;

		/*----------------------------------------------------------------------------------------
		189^Kontracepcija drugo^^192^Klinička dijagnoza
		----------------------------------------------------------------------------------------*/
		-- We take Exam Status from incoming HIS order
		-- We read it from OBR-40 field
		DECLARE @ExamStatus varchar(max);
		SELECT
				@ExamStatus = COL_DATA 
   			FROM
   				HL7_OBR CROSS APPLY Split(FIELD_DATA,'^') 
   			WHERE
   					SEGMENT_CNT    = @x
   				AND FIELD_POS      = 40
   				AND HL7_MESSAGE_ID = @hl7messageID
   				AND COL_NO         = 5;

		-- We take Clinical Diagnosis from incoming HIS order
		-- We read it from OBR-40 field
		DECLARE @ClinicalDiagnosis varchar(max);
		SELECT
				@ClinicalDiagnosis = COL_DATA
			FROM
				HL7_NTE CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 4
				AND HL7_MESSAGE_ID = @hl7messageID
				AND COL_NO         = 2;

		-- So there is something I am reading from OBR-8, I have no idea what it is
		-- I guess it's some date or time of something or whatevs...
		SELECT
				@ctemp = SUBSTRING(FIELD_DATA,1,14)
			FROM
				HL7_OBR
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 8
				AND HL7_MESSAGE_ID = @hl7messageID;

		IF isnull(@ctemp,'')<>''
		BEGIN
			IF(LEN(@ctemp)=12)
				SET @ctemp=@ctemp+'00';
    		BEGIN TRY
				SET @s1=SUBSTRING(@ctemp,1,8); /* just date */
				SET @s2='1899-12-30 '+SUBSTRING(@ctemp,9,2)+':'+SUBSTRING(@ctemp,11,2)+':'+SUBSTRING(@ctemp,13,2); /* just time */
				SET @s3=REPLACE(@s2,'1899-12-30 ',@s1+' '); /* date and time */
				SELECT @dt1=CONVERT(datetime,@s3,120);
			END TRY
			BEGIN CATCH
			END CATCH;
		END;

		-- Now the Comment field
		-- We read it from OBR-13 field
		DECLARE @comment VARCHAR(MAX);
		SELECT
				@comment = FIELD_DATA
			FROM
				HL7_OBR
			WHERE
					SEGMENT_CNT    = @x
				AND FIELD_POS      = 13
				AND HL7_MESSAGE_ID = @hl7messageID;

		-- And finally, lo and behold, we have all the data we need, yeee!					
		-- For some reason, we want to check if our Accession Number or Admission ID are of wrong size
		IF( LEN(@accessionnnumber)>64 OR LEN(@admissionid)>30)
		BEGIN
			SET @error='Accession number/Admission ID too long';
			RETURN;
		END;

		-- And now we do our grand insert into our temporary table
		-- One insert per segment
		INSERT INTO 
			@FINTABLE(
				HL7_MESSAGE_ID,
				MSGTYPE,
				SEGMENT_CNT,
				PATIENTID,
				REFERRINGID,
				EXAMTYPEID,
				EXAMTYPENAME,
				DEVICEID,
				DEVICENAME,
				ORDER_DATE,
				ORDER_TIME,
				ORDER_DATETIME,
				ACCESSIONNUMBER,
				ACCESSIONNUMBER2,
				ADMISSIONID,
				SENTFROM,
				CURRENTDISEASE,
				RESULTDIAG_ICD,
				PreceedingExams,
				EXAMSTATUS,
				ClinicalDiagnosis,
				BILLING_REQUEST_NO,
				PAYTYPE,
				PATIENTTYPE,
				COMMENT)
			VALUES(
				@hl7messageID,
				@messageTYPE,
				@x,
				@patientID,
				@referringID,
				@examtypeID,
				@examtypeNAME,
				@examdeviceID,
				@examdeviceNAME,
				@d1,
				@t1,
				@dt1,
				@accessionnnumber,
				@FirstAccno,
				@admissionid,
				@sentfrom,
				@currentdesease,
				@RESULTDIAG_ICD,
				@PreceedingExams,
				@ExamStatus,
				@ClinicalDiagnosis,
				@billingNO,
				@paytype,
				@patientTYPE,
				@comment);
	END;
	EXEC Import2JobList 
		@FINTABLE,
		@error output;
	IF @error<>''
   		EXEC Shared_LogError @hl7messageID,@error, 2;
	ELSE
   		EXEC Shared_LogError @hl7messageID,@error,998;
END
GO
