USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Export_Get_MSH]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[Export_Get_MSH]
	@msgtype varchar(256),
	@error varchar(max) output,
	@msh varchar(max) output,
	@app varchar(256)=''
AS
BEGIN
SET NOCOUNT ON;
/*----------------------------------------------------------------------------------------
                               MSH
----------------------------------------------------------------------------------------*/
IF(ISNULL(@msgtype,'')='')
BEGIN
  SET @error='Messge type empty';
  RETURN;
END;

DECLARE @our_app varchar(256),@our_fac varchar(256),@his_app varchar(256),@his_fac varchar(256),@dt varchar(256),@d varchar(256);
EXEC Shared_ReadSetting @our_app output, 'OUR_APP';
EXEC Shared_ReadSetting @our_fac output, 'OUR_FAC';
EXEC Shared_ReadSetting @his_app output, 'HIS_APP';
EXEC Shared_ReadSetting @his_fac output, 'HIS_FAC';

IF isnull(@our_app,'')=''
  SET @our_app='ISSA';
IF isnull(@his_app,'')=''
  SET @his_app='BIS';
IF @our_fac IS NULL
  SET @our_fac='';
IF @his_fac IS NULL
  SET @his_fac='';


BEGIN TRY

SELECT @dt=convert(varchar,getdate(),112)+replace(convert(varchar,getdate(),114),':','');
SELECT @d=SUBSTRING(@dt,1,14);
SET @MSH='MSH|^~\&|'+dbo.HL7_Escape(@our_app,0)+'|'+dbo.HL7_Escape(@our_fac,0)+'|'+dbo.HL7_Escape(@his_app,0)+'|'+dbo.HL7_Escape(@his_fac,0)+'|'+@d+'||'+@msgtype+'|'+@dt+'|P|2.3';

if(isnull(@app,'')='MEDIT')
BEGIN
  SELECT @his_app='MEDIT',@his_fac='NEURON',@our_app='ISSA',@our_fac='CO';

  select @dt=replace(replace(replace(replace(convert(varchar(128),SYSDATETIMEOFFSET()),'-',''),':',''),'.',''),' ','');
  select @d=substring(@dt,1,14)+'.'+substring(@dt,15,4)+substring(@dt,len(@dt)-4,5);
  select @dt=substring(@dt,1,len(@dt)-5);

  SET @MSH='MSH|^~\&|'+dbo.HL7_Escape(@our_app,0)+'|'+dbo.HL7_Escape(@our_fac,0)+'|'+dbo.HL7_Escape(@his_app,0)+'|'+dbo.HL7_Escape(@his_fac,0)+'|'+@d+'||'+@msgtype+'|'+@dt+'|P^T|2.5||||||8859/2';
END;

IF @MSH is null
BEGIN
   SET @error='Failed to make MSH';
   RETURN;
END;
END TRY
BEGIN CATCH
   SET @error='Failed to make MSH: '+ERROR_MESSAGE();
   RETURN;
END CATCH;

END


GO
