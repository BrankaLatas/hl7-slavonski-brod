USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Export_Get_PID]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[Export_Get_PID]
	@joblistid int,
	@examid int,
	@error varchar(max) output,
	@pid varchar(max) output
AS
BEGIN
SET NOCOUNT ON;
/*----------------------------------------------------------------------------------------
                               PID
----------------------------------------------------------------------------------------*/
/* 
PID|||4724670^^^^PI||KOKIĆ^MATEA||19990406|F|||| 
*/
DECLARE @pcode varchar(256),@pname varchar(256),@dob varchar(32),@sex varchar(32);

BEGIN TRY
IF @examid is not null
BEGIN
	SELECT TOP 1
			@pcode = CODE,
			@pname = NAME,
			@dob   = convert(varchar,BIRTHDATE,112),
			@sex   = s.SEX 
		FROM
				 PATIENT a
			JOIN SEX     s on a.SEXID     = s.SEXID
			JOIN EXAM    b ON a.PATIENTID = b.PATIENTID 
		WHERE
			b.EXAMID = @examid;
END
ELSE
BEGIN
   SELECT TOP 1 @pcode=ISNULL(ID,CODE),@pname=NAME,@dob=convert(varchar,BIRTHDATE,112),@sex=s.SEX 
   FROM PATIENT a JOIN SEX s on a.SEXID=s.SEXID JOIN JOBLIST b ON a.PATIENTID=b.PATIENTID 
   WHERE b.JOBLISTID=@joblistid;
END;
END TRY
BEGIN CATCH
   SET @error='Failed to make PID: '+ERROR_MESSAGE();
   RETURN;
END CATCH;

IF(ISNULL(@pcode,'')='')
BEGIN
   SET @error='Failed to make PID - no code data';
   RETURN;
END;
	
SET @pid='PID|||'+dbo.HL7_Escape(@pcode,0)+'^^^^PI'+'||'+dbo.HL7_Escape(@pname,1)+'||'+@dob+'|'+@sex+'||||';
IF @pid is null
BEGIN
   SET @error='Failed to make PID - invalid data';
   RETURN;
END;

END






GO
