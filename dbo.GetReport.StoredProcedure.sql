USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[GetReport]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[GetReport]
@examid int,
@error varchar(MAX) output,
@report varchar(max) output,
@issaany varchar(MAX) = NULL
AS
BEGIN

DECLARE @nl varchar(8)=CHAR(13)+CHAR(10);

declare
@f1  varchar(max),
@f2  varchar(max),
@f3  varchar(max),
@f4  varchar(max),
@f5  varchar(max),
@f6  varchar(max),
@f7  varchar(max),
@f8  varchar(max),
@f9  varchar(max),
@f10 varchar(max),
@f11 varchar(max),
@f12 varchar(max),
@f13 varchar(max),
@f14 varchar(max),
@f15 varchar(max),
@f16 varchar(max),
@f17 varchar(max),
@f18 varchar(max),
@f19 varchar(max),
@f20 varchar(max),
@f21 varchar(max),
@f22 varchar(max),
@f23 varchar(max);

select
@f1 =e.examnumber,
@f2 =REPLACE(p.name,'^',' '),
@f3 =convert(varchar,p.BIRTHDATE,104),
@f4 =p.code,
@f5 =p.address,
@f6 =convert(varchar,e.EXAMDATE,104)+' '+convert(varchar,e.EXAMDATE,108),
@f7 =e.sentfrom,
@f8 =e.ClinicalDiagnosis,
@f9 =e.technician,
@f10='<td class="t10">Nalaz napisao: '+t.name+'</td> <!-- Typist -->',
@f11=e.result,
@f12=u.name,
@f13=s.name,
@f14=s.special,
@f15=s.fax,
@f16='', /* @issaany */
@f17=e.currentdisease,
@f18=e.ResultDiag_ICD,
@f19=convert(varchar,e.ResultDate,104)+' '+convert(varchar,e.ResultDate,108),
@f20=s.fax,
@f21=e.SentFrom,
@f22=r.Name,
@f23=u.Special
from      exam e 
   join patient p on p.patientid=e.patientid
left join users u on u.usersid=e.usersid
left join users t on t.usersid=e.usersidType
left join users s on s.usersid=e.UsersOverViewID
left join users r on r.usersid=e.UsersIDRefering

where e.examid=@examid;

IF(ISNULL(@f11,'')<>'')
BEGIN
  SET @f11=REPLACE((SELECT REPLACE(@f11,@nl,'<br>') AS [text()] FOR XML PATH('')),'&lt;br&gt;','<br>');
END;

SET @error='';

SET @report='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="http://192.168.8.200/sb.css">
		<link rel="stylesheet" type="text/css" href="sb.css">
		<meta content="text/html; charset=iso-8859-2" http-equiv="Content-Type">
		<title>Radiolo&#353;ki nalaz</title>
	</head>
	<body leftmargin=70px topmargin=70px onload="myFunction()">
		<table class = "pageheader">
			<tr>
				<td>
					<table class = "KBDheader" width="658">
						<tr>
							<td class="h8"><b>OP&#262;A BOLNICA &quot;DR. JOSIP 
							BENČEVIĆ&quot; SLAVONSKI BROD, 046204628</b><br></td>
						</tr>
						<tr>
							<td class="h9">Andrije Štampara 42, 35 000 SLAVONSKI 
							BROD</td>
						</tr>
						<tr>
							<td class="h8"><b>ODJEL ZA KLINIČKU RADIOLOGIJU</b></td>
						</tr>
						<tr>
							<td class="h8">Voditelj odjela: Drago Mitrečić, dr.med.</td>
						</tr>
						<tr>
							<td class="h8"> </td>
						</tr>
						<tr>
							<td class="h9"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table class = "KBDheader">
			<tr>
				<td><h1>Specijalistički nalaz radiologa</h1></td>
			</tr>
			<tr>
				<td class="h8">Pregled broj: '+ISNULL(@f1,'')+'</td> <!-- Broj pregleda -->
			</tr>
		</table>
		<br>
		<table  class = "pageheader">
			<tr>
				<td class="ime">'+ISNULL(@f2,'')+'</td> <!-- Prezime i ime -->
			</tr>
			<tr>
				<td>Ro&#273;en '+ISNULL(@f3,'')+',
				Adresa: '+ISNULL(@f5,'')+'</td> <!-- Datum rodjenja i adresa -->
			</tr>
			<tr>
				<td>MBOO: '+ISNULL(@f4,'')+'</td> <!-- Broj iskaznice -->
			</tr>
			<tr>
				<td>Datum nalaza: '+ISNULL(@f6,'')+'</td> <!-- Datum pregleda -->
			</tr>
			<tr>
				<td>Upu&#263;en od: '+ISNULL(@f21,'')+'</td> 
			</tr>
			<tr>
				<td class="t10">Uputni lije&#269;nik: '+ISNULL(@f22,'')+'</td>
			</tr>
		</table>
		<table class = "pageheader">
			<tr>
				<td class="t9"><b>Nalaz i mi&#353;ljenje</b></td>
			</tr>
			<tr> <!-- Nalaz -->
				<td class="t10">'+ISNULL(@f11,'')+'
				</td>
			</tr>
		</table>
		<p><span style="font-size: 90%">Ing. med. radiologije: '+ISNULL(@f9,'')+'</span><br>
		</p>
		<table style = "width:661px">
			<tr>
				<td class="potpis" width = "200px" id="specijalizantime"><b>
				'+ISNULL(@f12,'')+'
				</b></td>
				<td width = "200px"></td>
				<td class="potpis" id="specijalistime"><b>
				'+ISNULL(@f13,'')+'
				</b></td>
			</tr>
			<tr>
				<td class="potpis" width = "200px" id="specijalizantspec"></td>
				<td width = "200px"></td>
				<td class="potpis">
				'+ISNULL(@f14,'')+'
				</td>
			</tr>
			<tr>
				<td class="potpis" width = "200px" id="specijalizantcode">
				('+ISNULL(@f23,'')+')
				</td>
				<td width = "200px"></td>
				<td class="potpis">
				('+ISNULL(@f20,'')+')
				</td>
			</tr>
		</table>
		<br>
		<table>
			<tr>
				<td width = "400px"><b><i>Datum izdavanja:</i></b> '+ISNULL(@f19,'')+'</td>
				<td class="potpis"></td>
			</tr>
		</table>
		<br>
		<table>
			<tr>
				<td valign="top">&nbsp;</td>    
				<td width="1" >&nbsp;</td>      
				<td width="485">
					<div align="left">  
						<input name="button1" type="button" onclick="ClipBoard(&#39;'+ISNULL(@issaany,'')+'&#39;);" value="Slike" >
						<br>
						<input name="holdtext" type="text" style="color:white;border:0px" value="test" size="10" >
					</div>  
				</td>
			</tr>
		</table>
		<br>
		<script language="JavaScript" type="text/javascript">
			function ClipBoard(txt) {
				holdtext.innerText = txt;
				Copied = holdtext.createTextRange();
				Copied.execCommand("RemoveFormat");
				Copied.execCommand("Copy");
			};
			function myFunction() {
				var a; //specijalist
				var b; //specijalizant
				a = document.getElementById("specijalistime").innerHTML;
				b = document.getElementById("specijalizantime").innerHTML;
				if (a == b) {
					document.getElementById("specijalizantime").innerHTML = "";
					document.getElementById("specijalizantspec").innerHTML = "";
					document.getElementById("specijalizantcode").innerHTML = "";
				}
			}
		</script>
	</body>
</html>';

	SET @report = REPLACE(@report, 'DŽ' collate Croatian_CS_AS, 'D&#381;');
	SET @report = REPLACE(@report, 'Dž' collate Croatian_CS_AS, 'D&#382;');
	SET @report = REPLACE(@report, 'dŽ' collate Croatian_CS_AS, 'd&#381;');
	SET @report = REPLACE(@report, 'dž' collate Croatian_CS_AS, 'd&#382;');

	SET @report = REPLACE(@report, 'Ž' collate Croatian_CS_AS, '&#381;');
	SET @report = REPLACE(@report, 'Š' collate Croatian_CS_AS, '&#352;');
	SET @report = REPLACE(@report, 'Č' collate Croatian_CS_AS, '&#268;');
	SET @report = REPLACE(@report, 'Ć' collate Croatian_CS_AS, '&#262;');
	SET @report = REPLACE(@report, 'Đ' collate Croatian_CS_AS, '&#272;');
	SET @report = REPLACE(@report, 'ž' collate Croatian_CS_AS, '&#382;');
	SET @report = REPLACE(@report, 'š' collate Croatian_CS_AS, '&#353;');
	SET @report = REPLACE(@report, 'č' collate Croatian_CS_AS, '&#269;');
	SET @report = REPLACE(@report, 'ć' collate Croatian_CS_AS, '&#263;');
	SET @report = REPLACE(@report, 'đ' collate Croatian_CS_AS, '&#273;');
  
END


GO
