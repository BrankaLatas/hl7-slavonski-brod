USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Import_SB]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Import_SB]
@hl7_message_id INT=NULL,
@error VARCHAR(MAX) output
AS
BEGIN
	SET NOCOUNT ON;
	/*--------------------------------------------------------------------------------
 	Data that we extract from received message. 
 	Joblist.UID, Exam.UID, Exam.ExamID, Exam.UsersID will be generated inside functions just before insert
	--------------------------------------------------------------------------------*/

	DECLARE @messageID   int,
		    @messageTYPE varchar(256),
		    @patientID   int;
	DECLARE @ctemp       varchar(max);

	SET @messageID = NULL;

	IF @hl7_message_id is NULL
	BEGIN
		SELECT TOP 1 
				@messageID   = ID,
				@messageTYPE = MSGTYPE 
			FROM 
				HL7_MESSAGE
			ORDER BY 
				ID 
			DESC;
		SET @hl7_message_id=@messageID;
	END;
	ELSE
	BEGIN
		SELECT
				@messageID   = ID,
				@messageTYPE = MSGTYPE 
			FROM 
				HL7_MESSAGE
			WHERE
				ID = @hl7_message_id;
	END;

	IF @messageID is NULL
	BEGIN
		IF @hl7_message_id is NULL
			SET @error='No messages in queue';
		ELSE
			SET @error='Message with ID='+cast(@hl7_message_id as varchar)+' not found';
   		RETURN;
	END;


	-- We check the message type
	-- First the obvious - if it is ADT^A08 - patient update message, we do the patient update.

	IF isnull(@messageTYPE,'') = 'ADT^A08^ADT_A01'
	BEGIN
		SET @patientID=NULL;
		SET @error='';
		DECLARE @PatientID_HIS    AS VARCHAR(256);
		DECLARE @PatientID_HZZO   AS VARCHAR(256);
		DECLARE @PatientID_OIB    AS VARCHAR(256);
		DECLARE @PatientLastName  AS VARCHAR(256);
		DECLARE @PatientFirstName AS VARCHAR(256);
		DECLARE @PatientName      AS VARCHAR(256);
		DECLARE @PatientDOBString AS VARCHAR(64);
		DECLARE @PatientDOBDay    AS VARCHAR(64);
		DECLARE @PatientDOB		  AS DATETIME;
		DECLARE @PatientDOBMonth  AS VARCHAR(64);
		DECLARE @PatientDOBYear   AS VARCHAR(64);
		DECLARE @PatientSex       AS VARCHAR(64);
		DECLARE @PatientSexID     AS INTEGER;
		DECLARE @PatientAddress   AS VARCHAR(256);
		DECLARE @PatientCity      AS VARCHAR(256);
		DECLARE @PatientCnt AS INTEGER;
		DECLARE @PatientCodeIssuerIssa AS INTEGER;
		
		SELECT @PatientID_HIS = COL_DATA
			FROM HL7_PID CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE
					SEGMENT_CNT    = 1
				AND FIELD_POS      = 3
				AND HL7_MESSAGE_ID = @hl7_message_ID
				AND COL_NO         = 1;
		SET @PatientCnt = (SELECT COUNT(1) FROM PATIENT WHERE PATIENT.Code = @PatientID_HIS);
		IF @PatientCnt = 0
		BEGIN
			RETURN;
		END
		ELSE
		BEGIN
			SET @PatientID             = (SELECT PatientID  FROM PATIENT WHERE PATIENT.Code      = @PatientID_HIS);
			SET @PatientCodeIssuerIssa = (SELECT CodeIssuer FROM PATIENT WHERE PATIENT.PatientID = @PatientID);
			IF @PatientCodeIssuerIssa <> 3
			BEGIN
				RETURN;
			END;
		END;
		SET @PatientID_HZZO = '';
		SELECT @PatientID_HZZO = COL_DATA
			FROM HL7_PID CROSS APPLY Split(FIELD_DATA,'~')
			WHERE
					SEGMENT_CNT    = 1
				AND FIELD_POS      = 3
				AND HL7_MESSAGE_ID = @hl7_message_ID
				AND COL_NO         = 3;
		SET @PatientID_HZZO = ISNULL(LEFT(@PatientID_HZZO,CHARINDEX('^',@PatientID_HZZO)-1),'');
		IF LEN(@PatientID_HZZO) < 9
		BEGIN 
			SET @PatientID_HZZO = '';
		END;
		SELECT @PatientLastName  = COL_DATA
			FROM HL7_PID CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE
					SEGMENT_CNT    = 1
				AND FIELD_POS      = 5
				AND HL7_MESSAGE_ID = @hl7_message_ID
				AND COL_NO         = 1;
		SELECT @PatientFirstName = COL_DATA
			FROM HL7_PID CROSS APPLY Split(FIELD_DATA,'^') 
			WHERE
					SEGMENT_CNT    = 1
				AND FIELD_POS      = 5
				AND HL7_MESSAGE_ID = @hl7_message_ID
				AND COL_NO         = 2;
		SET @PatientName = @PatientLastName + '^' + @PatientFirstName;
		SELECT @PatientDOBString = FIELD_DATA
			FROM 
				HL7_PID 
			WHERE
				SEGMENT_CNT    = 1
			AND FIELD_POS      = 7
			AND HL7_MESSAGE_ID = @hl7_message_ID;
		SET @PatientDOBYear  = LEFT(@PatientDOBString,4);
		SET @PatientDOBMonth = SUBSTRING(@PatientDOBString,5,2);
		SET @PatientDOBDay   = SUBSTRING(@PatientDOBString,7,2);
		SET @PatientDOB = CAST(@PatientDOBYear + '-' + @PatientDOBMonth + '-' + @PatientDOBDay + ' 00:00:00.000' AS DATETIME);
		SELECT @PatientSex = FIELD_DATA
			FROM 
				HL7_PID 
			WHERE
				SEGMENT_CNT    = 1
			AND FIELD_POS      = 8
			AND HL7_MESSAGE_ID = @hl7_message_ID;
		SET @PatientSexID = 0;
		IF @PatientSex = 'M'
		BEGIN
			SET @PatientSexID = 1;
		END;
		IF @PatientSex = 'F'
		BEGIN
			SET @PatientSexID = 2;
		END;
		--EXEC Patient_Check @messageID,@patientID output,@error output;
		--EXEC Patient_Update	@patientID, @PatientID_HIS,	@PatientName, @PatientDOB, @PatientSexID, @PatientID_HZZO, @error;
		EXEC Patient_Update	@patientID, @PatientID_HIS,	@PatientName, @PatientDOB, @PatientSexID, @PatientID_HZZO, @error;

		IF (@error<>'')
		BEGIN
			EXEC Shared_LogError @messageID,@error,2,'Update patient data';
		END;
   		ELSE
		BEGIN
			EXEC Shared_LogError @messageID,@error,998;
		END;
		RETURN;
	END
	ELSE
	-- If it is not patient update, then we support only ORM^O01 and OMG^O19
	-- Anything else we do not support, bugger off
	IF isnull(@messageTYPE,'') NOT LIKE 'OMG^O19%' AND isnull(@messageTYPE,'') NOT LIKE 'ORM^O01%'
	BEGIN
		SET @error='Unsupported message';
		EXEC Shared_LogError @messageID,@error,2,'Unknown';
		RETURN;
	END;

	/*--------------------------------------------------------------------------------
	   OMG^O19 - ORDER MESSAGE
	--------------------------------------------------------------------------------*/
	/* All operations in one message should be the same so we can read it from first segment */
	-- OK, now that we got all the way to here, it seems we have an order message
	DECLARE @cmd varchar(256),
		    @f25 varchar(256); /* NW, CA, SC, ... */

	-- First we read the command from ORC-1
	SELECT 
			@cmd = FIELD_DATA
		FROM 
			HL7_ORC 
		WHERE
			    SEGMENT_CNT    = 1
			AND FIELD_POS      = 1
			AND HL7_MESSAGE_ID = @messageID;

	-- What the hell are we reading from OBR-25?!
	SELECT 
			@f25 = FIELD_DATA
		FROM
			HL7_OBR
		WHERE 
			    SEGMENT_CNT    = 1
			AND FIELD_POS      = 25
			AND HL7_MESSAGE_ID = @messageID;


	-- Now we read the calling application
	DECLARE @caller_app varchar(32)='';
	SELECT 
			@caller_app = FIELD_DATA
		FROM
			HL7_MSH 
		WHERE 
				SEGMENT_CNT    = 1
			AND FIELD_POS      = 2 
			AND HL7_MESSAGE_ID = @messageID;

	-- Oops! But could it be that In2 tried to send us ORM^O01?
	-- Bugger off!
	IF isnull(@messageTYPE,'') LIKE 'ORM^O01%'
	BEGIN
  		SET @error='Unsupported message';
  		EXEC Shared_LogError @messageID,@error,2,'Unknown';
  		RETURN;
	END;

	-- OK, now it appears everything is OK.
	-- We check what kind of message is this.

	-- .
	IF @cmd='SC' -- SC means that we should edit our order
	BEGIN
		SET @error='';
		EXEC Order_Edit @messageID, @error output;
		RETURN;
	END
	ELSE
	IF @cmd='CA' -- CA means we should cancel our order
	BEGIN
		SET @error='';
		EXEC Order_Cancel @messageID,@error output;
		RETURN;
	END
	ELSE
	IF @cmd='NW' -- NW means new order, yee!
	BEGIN
		SET @error='';
		EXEC Order_New @messageID,@error output;
		RETURN;
	END
	ELSE
	/* New procedure on existing order after our request,
 	or in this case 'add material' this is message response */
	IF @cmd='NA' --This is for adding new order after our request, specifically for In2
	BEGIN
		-- I'm not sure exactly what this does, I have to figure it out later
		-- I'm just gonna beautify this code
		SET @error='';
		BEGIN TRY
			DECLARE @old_accno varchar(256),
				   @new_accno varchar(256),
				   @examid    int,
				   @etype     varchar(max),
				   @queueid   INT;

			SELECT 
					@new_accno = FIELD_DATA 
				FROM 
					HL7_ORC
				WHERE
					    SEGMENT_CNT    = 1 
					AND FIELD_POS      = 2 
					AND HL7_MESSAGE_ID = @messageID;
			SELECT
					@old_accno = FIELD_DATA 
				FROM 
					HL7_ORC 
				WHERE
					    SEGMENT_CNT    = 1
					AND FIELD_POS      = 3
					AND HL7_MESSAGE_ID = @messageID;
			SELECT
					@etype = FIELD_DATA
				FROM 
					HL7_OBR
				WHERE
					    SEGMENT_CNT    = 1
					AND FIELD_POS      = 4 
					AND HL7_MESSAGE_ID = @messageID;

			IF(ISNULL(@old_accno,'')='')
			BEGIN
		  		SET @error='No original accession number';
          		EXEC Shared_LogError @messageID,@error,2;
		  		RETURN;
			END;

			IF(ISNULL(@etype,'')='')
			BEGIN
				SET @error='No procedure / material data';
				EXEC Shared_LogError @messageID,@error,2;
				RETURN;
			END;

			SET @queueid = NULL;
			SET @examid  = NULL;
			SELECT TOP 1 
					@examid=examid
				FROM
					HL7_ORDERS
				WHERE
					ACCNO = @old_accno
				ORDER BY 
					ID 
				DESC;
			IF(ISNULL(@examid,0)>0)
				SELECT TOP 1 @queueid=ID from HL7_QUEUE WHERE EXAMID=@examid ORDER BY ID DESC;

			IF(ISNULL(@examid,0)=0 OR ISNULL(@queueid,0)=0)
			BEGIN
				SET @error='Can not find original order for NA message';
				EXEC Shared_LogError @messageID,@error,2;
				RETURN;
			END;
			EXEC ExportMessages @queueid,@error output,'SC',@new_accno,@etype;
			IF(@error<>'')
				EXEC Shared_LogError @messageID,@error,2;
			ELSE
				EXEC Shared_LogError @messageID,@error,998;
		END TRY
		BEGIN CATCH
			SET @error='Failed to process NA message: '+ERROR_MESSAGE();
			EXEC Shared_LogError @messageID,@error,2;
		END CATCH;
		RETURN;
	END
	ELSE
	BEGIN
		SET @error='Unsupported order control code ['+@cmd+']';
		EXEC Shared_LogError @messageID,@error,2,'Unknown';
	END;
END


GO
