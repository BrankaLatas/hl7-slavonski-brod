USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[GenerateExamFinishedMessage]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[GenerateExamFinishedMessage]
	@hl7_queue_id INT,
	@error varchar(MAX) output
AS
BEGIN
	SET NOCOUNT ON;
	SET @error='';

	DECLARE @msg1      varchar(max);
	DECLARE @msg       varchar(max);
	DECLARE @JobListID int,
			@ExamID    int;
	DECLARE	@Status    			 varchar(256),
			@AccessionNumberIssa varchar(256),
			@AccessionNumberHIS  varchar(256),
			@UserResultName		 varchar(256),
			@UserResultCode		 varchar(32),
			@ExamTypeCode		 varchar(256),
			@ExamTypeName		 varchar(256),
			@ExamDeviceCode		 varchar(32),
			@ExamDeviceName		 varchar(256),
			@ResultVerifyDate	 varchar(256)
	DECLARE @AppName   varchar(256)='',
			@DPort     int,
			@DServer   varchar(32),
			@DFolder   varchar(max);

	BEGIN TRY
		SELECT TOP 1
			@JobListID = JOBLISTID,
			@ExamID    = EXAMID,
			@status    = STAT,
			@AppName   = CUSTOM3,
			@DPort     = D_PORT,
			@DServer   = D_ADDR,
			@DFolder   = D_FOLDER
			FROM
				HL7_QUEUE
			WHERE
				ID = @hl7_queue_id;
	END TRY
	BEGIN CATCH
		SET @error='Failed to get data for message: ' + ERROR_MESSAGE();
		RETURN;
	END CATCH;

	DECLARE @Orders TABLE(
		ID                  INT IDENTITY(1,1),
		AccessionNumberHIS  varchar(256),
		AccessionNumberIssa varchar(256),
		ExamTypeCode        varchar(256),
		ExamTypeName        varchar(256));

	DECLARE @MSH varchar(max),
			@PID varchar(max),
			@ORC varchar(max),
			@OBR varchar(max);
	DECLARE @Issa_Status varchar(256);
	DECLARE @cnt int,
			@x   int;

	SET @msg1 = '';
	SET @msg  = '';


	--TRUNCATE TABLE #order;

	INSERT INTO @Orders(
			AccessionNumberHIS,
			AccessionNumberIssa,
			ExamTypeCode,
			ExamTypeName)
		SELECT
				x.AccessionNumberHIS,
				a.AccessionNumber,
				et.CODEVALUE,
				et.[NAME]
			FROM 
		     	 EXAM        a 
		 	JOIN JOBLIST     b ON a.examid     = b.examid
	   LEFT JOIN EXAMTYPE   et ON b.ExamTypeID = et.ExamTypeId
	   LEFT JOIN USERS       u ON u.USERSID    = isnull(a.UsersOverViewID,10)
			JOIN HL7_ORDERS  x ON x.JOBLISTID  = b.JobListID
		WHERE
			a.ExamID         = @examid;
   
	SELECT @cnt = COUNT(1) FROM @Orders; --Number of linked orders
	IF(@cnt=0)
	BEGIN
		SET @error='Failed to make exam ORC/OBR - no data';
		RETURN;
	END;
   
	SET @msg1='';

	EXEC Export_Get_MSH 
   		 'OMG^O19^OMG_O19',
		 @error output,
		 @MSH output;
	IF(@error<>'')
		RETURN;
    
	EXEC Export_Get_PID
			@joblistid,
			@examid,
			@error output,
			@PID output;
	IF(@error<>'')
		RETURN;
	
	SET @msg1 = @MSH + CHAR(13)
   			  + @PID + CHAR(13);

   	SELECT
   			@UserResultCode   = Users.UserCode,
   			@UserResultName   = Users.Name,
   			@ResultVerifyDate = SUBSTRING(convert(varchar,Exam.VerifyDate,112)+replace(convert(varchar,Exam.VerifyDate,114),':',''),1,14),
			@ExamDeviceCode = ExamDevice.CodeValue,
			@ExamDeviceName	= ExamDevice.DeviceNameHIS
		FROM
					   Exam
			INNER JOIN Users      ON Exam.UsersOverviewID = Users.UsersID
			INNER JOIN ExamDevice ON Exam.ExamDevice      = ExamDevice.Name
		WHERE
			Exam.ExamID = @ExamID;

	SET @x = 0;
	WHILE(@x < @cnt)
	BEGIN
		SELECT
			@AccessionNumberHIS  = AccessionNumberHIS,
			@AccessionNumberIssa = AccessionNumberIssa,
			@ExamTypeCode = ExamTypeCode,
			@ExamTypeName = ExamTypeName
		FROM 
			@Orders
		WHERE 
			id = (@x + 1);
		IF(@ExamTypeName is null)
		BEGIN
			SET @error='Failed to make ORC/OBR - exam type is empty';
			RETURN;
		END;
		IF(@AccessionNumberIssa is null)
		BEGIN
			SET @error='Failed to make ORC/OBR - accession number is empty, exam not in orders?';
			RETURN;
		END;
		SET @ORC = 'ORC|SC|'
				 + dbo.HL7_Escape(@AccessionNumberHIS,0)
				 + '|'
				 + dbo.HL7_Escape(@AccessionNumberIssa,0)
				 + '||F||||'
				 + @ResultVerifyDate
				 + '|'
				 + @UserResultCode + '^' + dbo.HL7_Escape(@UserResultName,1)
				 + '||'
				 + @ExamDeviceCode + '^' + @ExamDeviceName;

		SET @OBR = 'OBR|1|'
				 + dbo.HL7_Escape(@AccessionNumberHIS,0)
				 + '|'
				 + dbo.HL7_Escape(@AccessionNumberIssa,0)
				 + '|'
				 + dbo.HL7_Escape(@ExamTypeCode,1) + '^' + dbo.HL7_Escape(@ExamTypeName,1)
				 + '|||'
				 + @ResultVerifyDate
				 + '||||||||||||||||||F||1|||||'
				 + @UserResultCode + '^' + dbo.HL7_Escape(@UserResultName,1)
				 + '||||'
				 + @ResultVerifyDate
				 + '|';

		SET @msg1 = @msg1
				  + @ORC + CHAR(13)
				  + @OBR + CHAR(13);	
		SET @x = @x + 1;
	END;

	DECLARE @add INT=0;
	BEGIN TRY
		INSERT INTO HL7_LOGS_OUT_TEST(
				HL7_QUEUE_ID,
				CREATED,
				QSTATUS,
				SENDTIME,
				MSG,
				D_ADDR,
				D_PORT,
				D_FOLDER)
			VALUES(
				@hl7_queue_id,
				GETDATE(),
				0,
				GETDATE(),
				@msg1,
				@dserver,
				@dport,
				@dfolder);
	END TRY
	BEGIN CATCH
		SET @error = 'Failed to add message: ' + ERROR_MESSAGE();
		RETURN;
	END CATCH;
	SET @error='';
	RETURN;
END
GO
