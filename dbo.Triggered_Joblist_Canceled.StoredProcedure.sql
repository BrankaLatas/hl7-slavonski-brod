USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Triggered_Joblist_Canceled]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[Triggered_Joblist_Canceled]
	@joblistid int,
	@error varchar(max) output
AS
BEGIN
   SET NOCOUNT ON;
   SET @error='';
   
   if(@joblistid is null)
      RETURN;

   DECLARE @message_id INT,@examid INT;

   BEGIN TRY
     INSERT INTO HL7_QUEUE(JOBLISTID,QSTATUS,CMD) 
	 SELECT JOBLISTID,0,'CA' FROM HL7_ORDERS WHERE JOBLISTID=@joblistid AND EXAMID IS NULL;
   END TRY
   BEGIN CATCH
     SET @error=ERROR_MESSAGE();
   END CATCH;

END






GO
