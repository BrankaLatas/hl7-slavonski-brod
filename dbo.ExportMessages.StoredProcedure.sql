USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[ExportMessages]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------

CREATE procedure [dbo].[ExportMessages]
	@hl7_queue_id INT,
	@error varchar(MAX) output,
	@command VARCHAR(256)=NULL,
	@new_accno varchar(256)=NULL,
	@new_et varchar(256)=NULL
AS
BEGIN
SET NOCOUNT ON;
SET @error='';

DECLARE @msg1      varchar(max);
DECLARE @joblistid int,
		@examid    int;
DECLARE @cmd       varchar(256),
		@status    varchar(256),
		@accno     varchar(256),
		@examdate  varchar(32),
		@doc       varchar(256),
		@etype     varchar(256),
		@ref       varchar(256),
		@our_accno varchar(256);
DECLARE @appname   varchar(256)='',
		@dport     int,
		@dserver   varchar(32),
		@dfolder   varchar(max);

BEGIN TRY
	IF @hl7_queue_id IS NULL
	BEGIN
		SELECT TOP 1 
			@hl7_queue_id = ID,
			@joblistid    = JOBLISTID,
			@examid       = EXAMID,
			@cmd          = CMD,
			@status       = STAT,
			@new_accno    = CUSTOM1,
			@new_et       = CUSTOM2,
			@appname      = CUSTOM3,
			@dport        = D_PORT,
			@dserver      = D_ADDR,
			@dfolder      = D_FOLDER
			FROM 
				HL7_QUEUE 
			WHERE 
				    QSTATUS >= 0 
				AND QSTATUS <= 5 
				AND SENDTIME <= GETDATE()
			ORDER BY 
				QSTATUS,ID;
		IF @hl7_queue_id IS NULL
		BEGIN
			RETURN; /* nothing in export queue */
		END;
	END;
	ELSE
	BEGIN
		IF(ISNULL(@command,'')='')
			SELECT TOP 1
				@joblistid = JOBLISTID,
				@examid    = EXAMID,
				@cmd       = CMD,
				@status    = STAT,
				@new_accno = CUSTOM1,
				@new_et    = CUSTOM2,
				@appname   = CUSTOM3,
				@dport     = D_PORT,
				@dserver   = D_ADDR,
				@dfolder   = D_FOLDER
				FROM
					HL7_QUEUE
				WHERE 
						QSTATUS  >= 0 
					AND QSTATUS  <= 5 
					AND SENDTIME <= GETDATE() 
					AND ID        = @hl7_queue_id;
		ELSE
			SELECT TOP 1
				@joblistid = JOBLISTID,
				@examid    = EXAMID,
				@cmd       = CMD,
				@status    = STAT,
				@new_accno = CUSTOM1,
				@new_et    = CUSTOM2,
				@appname   = CUSTOM3,
				@dport     = D_PORT,
				@dserver   = D_ADDR,
				@dfolder   = D_FOLDER
				FROM
					HL7_QUEUE
				WHERE
					ID = @hl7_queue_id;
	END;
	IF(ISNULL(@command,'')<>'') SET @cmd = @command;
	IF @joblistid is NULL AND @examid is NULL
	BEGIN
		IF(@cmd is null) SET @error=''; /* no data */
	ELSE
		SET @error='JoblistID and ExamID empty ['+cast(@hl7_queue_id as varchar)+']';
		RETURN;
	END;
END TRY


BEGIN CATCH
	SET @error='Failed to get data for message: ' + ERROR_MESSAGE();
	RETURN;
END CATCH;

IF ISNULL(@cmd,'')=''
BEGIN
	SET @error = 'Export message cmd field is empty';
	RETURN;
END;

/*----------------------------------------------------------------------------------------
 Generate output HL7 message
 No need to encapsulate it with message begin (char 11) and message end (char 28, char 13) strings,
 HL7 service will do it if strings not found
----------------------------------------------------------------------------------------*/

CREATE TABLE #orc_obr(
	ID         INT IDENTITY(1,1),
	[status]   varchar(256),
	accno      varchar(256),
	examdate   varchar(32),
	doc        varchar(256),
	etype      varchar(256),
	ref        varchar(256),
	rad        varchar(256),
	ordinacija varchar(256),
	our_accno  varchar(256),
	clinical   varchar(max),
	diag       varchar(256));

DECLARE @MSH varchar(max),
		@PID varchar(max),
		@ORC varchar(max),
		@OBR varchar(max);
DECLARE @issa_status varchar(256);
DECLARE @cnt int,
		@x   int;
DECLARE @ordinacija varchar(256)='';

SET @msg1 = '';
IF(ISNULL(@examid,0)=0) /* JobList message */
BEGIN
	IF(@cmd<>'CA') /* We can send only Cancel message */
		RETURN;

	-- First we are going to generate MSH segment
	-- This is done by Export_Get_MSH procedure
	EXEC Export_Get_MSH
		'OMG^O19^OMG_O19', 
		@error output,
		@MSH   output;
	IF(@error<>'')
		RETURN;
  
	-- Then we create the PID segment
	-- This is done by Export_Get_PID procedure
	EXEC Export_Get_PID 
		@joblistid,
		@examid,
		@error output,
		@PID output;
	IF(@error<>'')
		RETURN;

	-- OK, so we create a temporary message and put that into @msg1 variable
	SET @msg1 =   @MSH + CHAR(13)
				+ @PID + CHAR(13);

	-- Now we are fetching data for ORC and OBR segments
	-- This is done by inserting that data into #orc_obr table
	-- This query gets that data
	INSERT INTO #orc_obr(
			[status],
			accno,
			examdate,
			doc,
			etype,
			ref,
			ordinacija,
			our_accno)
		SELECT TOP 1 
			'',
			x.ACCNO, -- HIS Accession Number
			SUBSTRING(convert(varchar,JOBDATE,112)+replace(convert(varchar,JOBSTART,114),':',''),1,14), -- Exam date
			ISNULL(u.USERCODE+'^'+u.[NAME],''),    -- Radiologist
			ISNULL(et.CODEVALUE,'')+'^'+et.[NAME], -- Exam type
			ISNULL(u2.USERCODE+'^'+u2.[NAME],''),  -- Referring physician
			SUBSTRING(ed.CodeValue,1,255),         -- Issa Exam device
			ISNULL(a.NewExamNumber,'')             -- Issa Accession Number
		FROM 
			          JOBLIST    a 
			LEFT JOIN USERS      u  ON a.USERID=u.USERSID 
			LEFT JOIN USERS      u2 ON a.ReferingID=u2.USERSID 
			LEFT JOIN EXAMTYPE   et ON a.EXAMTYPEID=et.EXAMTYPEID
			LEFT JOIN EXAMDEVICE ed ON a.ExamDeviceID=ed.ExamDeviceID
			     JOIN HL7_ORDERS x  ON x.JOBLISTID=a.JOBLISTID
		WHERE
				a.JobListID      = @joblistid 
			AND a.StatusMask&256 = 256;


	-- Now we check whether we have inserted anything into #orc_obr
	SELECT @cnt = COUNT(1) FROM #orc_obr;
	-- If we didn't insert anything, we terminate the procedure
	IF(@cnt = 0)
	BEGIN
		SET @error = 'Failed to make ORC/OBR - no data';
		RETURN;
	END;

	-- Now we create actual segments
	SET @x = 0;
	WHILE (@x < @cnt) -- ORC and OBR data is fetched from the temporary #orc_obr table
	BEGIN
		SELECT 
			@issa_status = [status],
			@accno       = accno,
			@examdate    = examdate,
			@doc         = doc,
			@etype       = etype,
			@ref         = ref,
			@ordinacija  = ISNULL(ordinacija,''),
			@our_accno   = our_accno 
		FROM 
			#orc_obr
		WHERE
			id=(@x+1);

		IF (@etype is null)
		BEGIN
			SET @error='Failed to make ORC/OBR - exam type is empty';
			RETURN;
		END;

		IF (@accno is null)
		BEGIN
			SET @error='Failed to make ORC/OBR - accession number is empty, exam not in orders?';
			RETURN;
		END;
     
		/* SET @ORC='ORC|CA|'+dbo.HL7_Escape(@accno,0)+'|'+dbo.HL7_Escape(@accno,0)+'||||||'+@examdate+'|'+dbo.HL7_Escape(@doc,1)+'|'+dbo.HL7_Escape(@doc,1)+'|'+dbo.HL7_Escape(@rad,1)+'|'; */

		-- We fill the string of the ORC segment
		SET @ORC = 'ORC|CA|' 
				 + dbo.HL7_Escape(@accno,0)
				 + '|'
				 + dbo.HL7_Escape(@our_accno,0)
				 + '||||||'
				 + @examdate
				 + '|'
				 + dbo.HL7_Escape(@doc,1)
				 + '||'
				 + @ordinacija;

		-- Then we fill the string of the OBR segment
		SET @OBR = 'OBR|1|'
				 + dbo.HL7_Escape(@accno,0)
				 + '|'
				 + dbo.HL7_Escape(@our_accno,0)
				 + '|'
				 + dbo.HL7_Escape(@etype,1)
				 + '|||'
				 + @examdate
				 + '||||||||||||||||||F||1|||||'
				 + dbo.HL7_Escape(@doc,1)
				 + '||||'
				 + @examdate
				 + '|';

		-- And finally, we add these two segments to our message
		SET @msg1 =   @msg1 
					+ @ORC + CHAR(13) 
					+ @OBR + CHAR(13);
		SET @x = @x + 1;
	END;

	-- Now we insert that message to our output log
	-- From there it is finally sent to the receiving system
	BEGIN TRY
	    INSERT INTO HL7_LOGS_OUT(HL7_QUEUE_ID,CREATED,QSTATUS,SENDTIME,MSG,D_ADDR,D_PORT,D_FOLDER)
		SELECT 
			ID,
			GETDATE(),
			0,
			CASE WHEN SENDTIME<GETDATE() THEN GETDATE() ELSE SENDTIME END,
			@msg1,
			@dserver,
			@dport,
			@dfolder
		FROM HL7_QUEUE 
		WHERE ID = @hl7_queue_id;
	END TRY
	BEGIN CATCH
		SET @error='Failed to add message: ' + ERROR_MESSAGE();
		RETURN;
	END CATCH;
	SET @error='';

	-- We did it, yeee!
	RETURN;
END;

/*   Exam message    */

/*
      63^B15^^^^^^^B15 - Stacionar - torakalna kirurgija
	  Pretpostavljamo da je uputno radiliste u istoj poruci isto za sve zahvate
*/
/*
DECLARE @njihovo_uputno VARCHAR(MAX)='',@hl7_message_id INT=0;
SELECT TOP 1 @hl7_message_id=HL7_MESSAGE_ID FROM HL7_ORDERS WHERE EXAMID=@examid;
IF(ISNULL(@hl7_message_id,0)>0)
BEGIN
  SELECT @njihovo_uputno=COL_DATA 
  FROM HL7_ORC CROSS APPLY Split(FIELD_DATA,'^') 
  WHERE SEGMENT_CNT=1 and FIELD_POS=13 AND HL7_MESSAGE_ID=@hl7_message_id AND COL_NO=1;
END;
*/


-- Now we are in the part which processes SC outgoing messages
DECLARE @mat_cnt INT=0;

IF(@cmd='SC' /* AND @status='F' */)
BEGIN
	TRUNCATE TABLE #orc_obr;

	INSERT INTO #orc_obr([status],accno,examdate,doc,etype,ref,ordinacija,our_accno)
		SELECT DISTINCT CASE a.STATUSMASK&1 WHEN 1 THEN 'F' ELSE 'IP' END, 
		x.ACCNO,
		SUBSTRING(convert(varchar,EXAMDATE,112)++replace(convert(varchar,EXAMDATE,114),':',''),1,14),
		ISNULL(u.USERCODE+'^'+u.[NAME],''),ISNULL(et.CODEVALUE,'')+'^'+et.[NAME],ISNULL(u2.USERCODE+'^'+u2.[NAME],''),
		SUBSTRING(ed.CodeValue,1,255),ISNULL(a.AccessionNumber,'')
	FROM 
		EXAM a 
			JOIN      JOBLIST b on a.examid=b.examid
			LEFT JOIN EXAMTYPE et ON b.ExamTypeID=et.ExamTypeId
			LEFT JOIN USERS u ON u.USERSID=case isnull(a.UsersOverViewID,'') when '' then ISNULL(NULLIF(a.USERSID,1),a.USERSIDRESULT) else a.UsersOverViewID end
			LEFT JOIN USERS u2 ON a.UsersIDRefering=u2.USERSID 
			LEFT JOIN EXAMDEVICE ed ON a.ExamDevice=ed.[Name]
			JOIN      HL7_ORDERS x ON x.JOBLISTID=b.JobListID
	WHERE
			a.ExamID         = @examid 
		AND b.StatusMask&256 = 0;
   
	SELECT @cnt=COUNT(1) FROM #orc_obr;
	IF(@cnt=0)
	BEGIN
		SET @error='Failed to make exam ORC/OBR - no data';
		RETURN;
	END;
   
	SET @msg1='';

	EXEC Export_Get_MSH 
   		 'OMG^O19^OMG_O19',
		 @error output,
		 @MSH output;
	IF(@error<>'')
		RETURN;
    
	EXEC Export_Get_PID
		 @joblistid,
		 @examid,
		 @error output,
		 @PID output;
	IF(@error<>'')
		RETURN;
	
	SET @msg1 = @MSH + CHAR(13)
   			  + @PID + CHAR(13);
   
	EXEC Export_Get_MSH 
		 'OMG^O19^OMG_O19',
		 @error output,
		 @MSH output;
	IF(@error<>'')
		RETURN;
   
   SELECT @cnt=COUNT(1) FROM #orc_obr;
   IF(@cnt=0)
   BEGIN
      SET @error='Failed to make exam ORC/OBR - no data';
      RETURN;
   END;

	SET @x=0;
	WHILE(@x<@cnt)
	BEGIN
		SELECT 
			@issa_status = [status],
			@accno       = accno,
			@examdate    = examdate,
			@doc         = doc,
			@etype       = etype,
			@ref         = ref,
			@ordinacija  = ISNULL(ordinacija,''),
			@our_accno   = our_accno 
		FROM 
			#orc_obr 
		WHERE 
			id = (@x+1);
		IF(@etype is null)
		BEGIN
			SET @error='Failed to make ORC/OBR - exam type is empty';
			RETURN;
		END;
		IF(@accno is null)
		BEGIN
			SET @error='Failed to make ORC/OBR - accession number is empty, exam not in orders?';
			RETURN;
		END;
		SET @ORC = 'ORC|SC|'
				 + dbo.HL7_Escape(@accno,0)
				 + '|'
				 + dbo.HL7_Escape(@our_accno,0)
				 + '||F||||'
				 + @examdate
				 + '|'
				 + dbo.HL7_Escape(@doc,1)
				 + '||'
				 + @ordinacija;

		SET @OBR = 'OBR|1|'
				 + dbo.HL7_Escape(@accno,0)
				 + '|'
				 + dbo.HL7_Escape(@our_accno,0)
				 + '|'
				 + dbo.HL7_Escape(@etype,1)
				 + '|||'
				 + @examdate
				 + '||||||||||||||||||F||1|||||'
				 + dbo.HL7_Escape(@doc,1)
				 + '||||'
				 + @examdate
				 + '|';

		SET @msg1 = @msg1
				  + @ORC + CHAR(13)
				  + @OBR + CHAR(13);	
		SET @x=@x+1;
	END;

	DECLARE @add INT=0;
	IF(@mat_cnt>0)
		SET @add=60;
	BEGIN TRY
		INSERT INTO HL7_LOGS_OUT(HL7_QUEUE_ID,CREATED,QSTATUS,SENDTIME,MSG,D_ADDR,D_PORT,D_FOLDER)
		SELECT ID,GETDATE(),0,DATEADD(second,@add,CASE WHEN SENDTIME<GETDATE() THEN GETDATE() ELSE SENDTIME END),@msg1,@dserver,@dport,@dfolder
		FROM HL7_QUEUE WHERE ID=@hl7_queue_id;
	END TRY
	BEGIN CATCH
		SET @error = 'Failed to add message: ' + ERROR_MESSAGE();
		RETURN;
	END CATCH;
	SET @error='';
	RETURN;
END

ELSE


-- Now we are in the part which is creating NW messages
-- This is the case if we forward requests for result to external RIS systems
-- In this specific case, we explicitly require that other system is named 'MEDIT'
-- This version is for special case o CO clinics
-- No HIS here, only data from Exam table
IF(@cmd='NW' AND @appname='MEDIT') /* MEDIT message 'forward' */
BEGIN
	DECLARE @clinical varchar(max)='',@diag varchar(256)='';
	TRUNCATE TABLE #orc_obr;

	-- OK, we fill the temporary #orc_obr table with this query
	INSERT INTO #orc_obr(
					[status],
					accno,
					examdate,
					doc,
					etype,
					ref,
					ordinacija,
					our_accno,
					clinical,
					diag)
		SELECT DISTINCT 
			CASE a.STATUSMASK&1 WHEN 1 THEN 'F' ELSE 'IP' END, 
			CAST(a.examid as varchar),               -- We set accession number in these messages to our Exam.ExamID
			SUBSTRING(convert(varchar,EXAMDATE,112) + replace(convert(varchar,EXAMDATE,114),':',''),1,14), --OK, datetime
			ISNULL(u.USERCODE + '^' + u.[NAME], ''), -- Radiologist (set to the group of the external institution)
			ISNULL(et.CODEVALUE,'')+'^'+et.[NAME],   -- Exam type and code
			ISNULL(u2.USERCODE+'^'+u2.[NAME],''),    -- Referring physician
			'',           							 -- "BIS uputno radiliste", empty in this case
			ISNULL(a.AccessionNumber   ,''),         -- Our accession number, taken from Exam table
			ISNULL(a.ClinicalDiagnosis ,''),         -- Clinical diagnosis, taken from Exam table
			''          							 -- ICD 10 diagnosis, empty in this case
	FROM          EXAM       a 
		--	 JOIN JOBLIST    b ON a.examid          = b.examid
		LEFT JOIN EXAMTYPE  et ON a.ExamType        = et.Name  -- Join goes on ExamType Name field
		LEFT JOIN USERS      u ON u.USERSID         = case isnull(a.UsersOverViewID,'') when '' then ISNULL(NULLIF(a.USERSID,1),a.USERSIDRESULT) else a.UsersOverViewID end
		LEFT JOIN USERS     u2 ON a.UsersIDRefering = u2.USERSID 
		--	 JOIN HL7_ORDERS x ON x.JOBLISTID       = b.JobListID
		--LEFT JOIN HL7_ORC    h ON h.HL7_MESSAGE_ID  = x.HL7_MESSAGE_ID AND  h.FIELD_POS = 13
		--LEFT JOIN HL7_DG1   dg ON dg.HL7_MESSAGE_ID = x.HL7_MESSAGE_ID AND dg.FIELD_POS = 3
	WHERE
   			a.ExamID         = @examid;
   		--AND b.StatusMask&256 = 0

   	-- If we didn't enter anything into temporary #orc_obr table, we exit
	SELECT @cnt=COUNT(1) FROM #orc_obr;
	IF(@cnt=0)
	BEGIN
		SET @error='Failed to make exam ORC/OBR - no data';
		RETURN;
	END;

	-- Now we build the actual message
	-- First the MSH segment
	SET @msg1='';
	EXEC Export_Get_MSH 
			'OMG^O19^OMG_O19',
			@error output,
			@MSH output,
			@appname;
	IF(@error<>'')
		RETURN;

	-- Now, for some reason, instead of creating the PID segment the usual way, we build it here
	-- We have to figure out why this is happening this way

	DECLARE @pcode varchar(256),@pname varchar(256),@dob varchar(32),@sex varchar(32),@addr varchar(256);
   /*
   SELECT TOP 1 @pcode=ISNULL(ID,CODE),@pname=NAME,@dob=convert(varchar,BIRTHDATE,112),@sex=s.SEX 
   FROM PATIENT a JOIN SEX s on a.SEXID=s.SEXID JOIN EXAM b ON a.PATIENTID=b.PATIENTID WHERE b.EXAMID=@examid;
   */

	-- This is the part of the data which we pick up from our database
	SELECT TOP 1 
			@pcode = a.Code,
			@pname = a.NAME,
			@dob   = convert(varchar,a.BIRTHDATE,112),
			@sex   = s.SEX 
		FROM         PATIENT a 
			 	JOIN SEX     s ON a.SEXID     = s.SEXID 
			 	JOIN EXAM    b ON a.PATIENTID = b.PATIENTID
		WHERE 
			b.EXAMID = @examid;

	-- We leave the address field blank
	SET @addr = '';

	-- These are used for specifying date of birth in correct format for HL7
	DECLARE  @tz varchar(64),
			 @dt varchar(64);
	SELECT @dt = replace(convert(varchar(128),SYSDATETIMEOFFSET()),':','');
	SELECT @tz = '000000.0000'+substring(@dt,len(@dt)-4,5);

	-- OK, and now we create actual PID segment
	SET @pid = 'PID|||' 
			 + dbo.HL7_Escape(@pcode,0)
			 + '||'
			 + dbo.HL7_Escape(@pname,1)
			 + '||'
			 + @dob
			 + @tz
			 + '|'
			 + @sex
			 + '|||'
			 + @addr
			 + '|';
	
	-- We add MSH and PID to our message
	SET @msg1 = @MSH + CHAR(13)
			  + @PID + CHAR(13);

	-- One more check for ORC and OBR data, if #orc_obr is empty we exit
	SELECT @cnt=COUNT(1) FROM #orc_obr;
	IF(@cnt=0)
	BEGIN
		SET @error='Failed to make exam ORC/OBR - no data';
		RETURN;
	END;

	
	SET @x = 0;
	WHILE(@x < @cnt)
	BEGIN
		SELECT 
				@issa_status = [status],
				@accno       = accno,
				@examdate    = examdate,
				@doc         = doc,
				@etype       = etype,
		    	@ref         = ref,
		    	@ordinacija  = ISNULL(ordinacija,''),
		    	@our_accno   = our_accno,
		    	@clinical    = clinical,
		    	@diag        = diag 
		    FROM 
		    	#orc_obr
		    WHERE 
		    	id=(@x+1);
	-- And one more check for sanity of ORC and OBR data
	-- If there is no Exam type specified, return
	IF(@etype is null)
	BEGIN
		SET @error='Failed to make ORC/OBR - exam type is empty';
		RETURN;
	END;

	-- If there is no Accession number specified, return
	IF(@accno is null)
	BEGIN
		SET @error='Failed to make ORC/OBR - accession number is empty, exam not in orders?';
		RETURN;
	END;

     /*
				MSH|^~\&|ISSA|OBVZ|MEDIT|NEURON|20190419105421.0989+0200||OMG^O19^OMG_O19|MSGID|P^T|2.5||||||8859/2
				PID|||AAAAAAAAA||PREZIME^IME||YYYYMMDD000000.0000+0200|M|||STREET^^CITY^^ZIP|

				ORC|NW|BBBBBB|||||||YYYYMMDDHHMMSS.0000+0200|||CCCC^PR^IM|DDD^EEEEE^^^^^^^UP RADILIŠTE|
				OBR|1|BBBBBB||FFFF^ZAHVAT|
				NTE||||^Kliničko pitanje
				DG1|1||GGG^MKB10 DIJAGNOZA^I10|
	 */

	-- Now we declare NTE and DG1 segments
	DECLARE @NTE varchar(max) = '',
			@DG1 varchar(max) = '';

	-- Again, suffixes for HL7 time
	SELECT @dt = replace(convert(varchar(128),SYSDATETIMEOFFSET()),':','');
	SELECT @tz = '.0000'+substring(@dt,len(@dt)-4,5);

	-- Now we build actual segments
	SET @ORC = 'ORC|NW|'
			 + dbo.HL7_Escape(@accno,0)
			 + '|||||||'
			 + @examdate
			 + @tz
			 + '|||'
			 + dbo.HL7_Escape(@ref,1)
			 + '|'
			 + dbo.HL7_Escape(@ordinacija,1)
			 + '|';

	SET @OBR = 'OBR|1|'
			 + dbo.HL7_Escape(@accno,0)
			 + '||'
			 + dbo.HL7_Escape(@etype,1)
			 + '|';

	SET @NTE = 'NTE||||^'
			 + dbo.HL7_Escape(@clinical,0)
			 + '|';

	SET @DG1 = 'DG1|1||'
			 + dbo.HL7_Escape(@diag,1)
			 + '|';

	SET @msg1 = @msg1
			  + @ORC + CHAR(13)
			  + @OBR + CHAR(13)
			  + @NTE + CHAR(13)
			  + @DG1 + CHAR(13);

	SET @x=@x+1;
	BREAK; /* change of plan, send only 1st ordered procedure in case of mutliple prcedures for 1 exam */
END;

BEGIN TRY
	INSERT INTO HL7_LOGS_OUT(
			HL7_QUEUE_ID,
			CREATED,
			QSTATUS,
			SENDTIME,
			MSG,
			D_ADDR,
			D_PORT,
			D_FOLDER)
		SELECT 
			ID,
			GETDATE(),
			0,
			GETDATE(),
			@msg1,
			@dserver,
			@dport,
			@dfolder
		FROM 
			HL7_QUEUE 
		WHERE 
			ID = @hl7_queue_id;
END TRY
BEGIN CATCH
	SET @error='Failed to add message: '+ERROR_MESSAGE();
	RETURN;
END CATCH;
SET @error='';
RETURN;
END;
/*****************************************************************************************************/

END
GO
