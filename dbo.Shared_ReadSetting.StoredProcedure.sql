USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Shared_ReadSetting]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








------------------------------------------------------------------------------

CREATE procedure [dbo].[Shared_ReadSetting]
@output varchar(max) output,
@name varchar(256),
@section varchar(256)=NULL
AS
BEGIN
SET NOCOUNT ON;

SELECT @output=S_DATA FROM HL7_SETTINGS WHERE S_SECTION=ISNULL(@section,'SETTINGS') AND S_NAME=@name AND [ENABLED]=1;
END







GO
