USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Patient_Check]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------

CREATE procedure [dbo].[Patient_Check]
	@hl7messageID int,
	@patientID    int          output,
	@error        varchar(max) output
AS
BEGIN
	SET NOCOUNT ON;
	SET @error='';

	DECLARE @patientCODE  varchar(256),
		   @patientNAME  varchar(256),
		   @patientADDR  varchar(256),
		   @patientTEL   varchar(256),
		   @patientOIB   varchar(256),
		   @patientMBO   varchar(256),
		   @patientDOB   datetime,
		   @patientSEXID int,
		   @ctemp        varchar(max);

	DECLARE @val     varchar(max);
	DECLARE @msgtype varchar(max);

	DECLARE @tmp1 table(id int,no1 int,no2 int,col_data varchar(max));
	DECLARE @tmp2 table(id int,no1 int,no2 int,col_data varchar(max));
	DECLARE @tmp3 table(id int,no1 int,no2 int,col_data varchar(max));

	-- First we check the message type
	SELECT
			@msgtype = MSGTYPE
		FROM
			HL7_MESSAGE
		WHERE
			ID = @hl7messageID;

	-- For some reason, mesasge type is empty, no soup for you!
	IF(isnull(@msgtype,'')='')
	BEGIN
   		SET @error     = 'Patient_Check - Invalid message, no message type';
   		SET @patientID = NULL;
   		RETURN;
   	END;
/*--------------------------------------------------------------------------------*/
/*
180993^^^^PI~""^^^^PN~132222214^^^^HC~06242590867^^^^OIB INTO @tmp1
id	no1	no2	col_data
1	1	1	351925
2	1	2	
3	1	3	
4	1	4	
5	1	5	PI
6	2	1	
7	2	2	
8	2	3	
9	2	4	
10	2	5	PN
11	3	1	132463144
12	3	2	
13	3	3	
14	3	4	
15	3	5	HC
...
*/
	-- So, In2 HIS will send us three sets of patient ID's in its PID-3 field
	-- First the PI set set which contains HIS data
	INSERT INTO
		@tmp1(
			id,
			no1,
			no2,
			col_data)
		SELECT
				row_number()over(ORDER by a.col_no,b.col_no) id, 
				a.col_no   no1,
				b.col_no   no2,
				b.col_data 
			FROM (
				SELECT x.* 
				FROM 
					HL7_PID cross apply dbo.Split(FIELD_DATA,'~') x 
				WHERE
					    SEGMENT_CNT    = 1
					AND FIELD_POS      = 3 
					AND HL7_MESSAGE_ID = @hl7messageID
					AND x.col_data like '%^PI'
				) a
				cross apply dbo.Split(col_data,'^') b;

	-- Then the PN set
	INSERT INTO
		@tmp2(
			id,
			no1,
			no2,
			col_data)
		SELECT
				row_number()over(ORDER by a.col_no,b.col_no) id,
				a.col_no   no1,
				b.col_no   no2,
				b.col_data 
			FROM (
				SELECT x.*
				FROM
					HL7_PID cross apply dbo.Split(FIELD_DATA,'~') x 
				WHERE
					    SEGMENT_CNT    = 1
					AND FIELD_POS      = 3
					AND HL7_MESSAGE_ID = @hl7messageID
					AND x.col_data like '%^HC'
					) a
					cross apply dbo.Split(col_data,'^') b;
	-- And finally the OIB set
	INSERT INTO 
		@tmp3(
			id,
			no1,
			no2,
			col_data)
		SELECT
				row_number()over(ORDER by a.col_no,b.col_no) id, 
				a.col_no no1,
				b.col_no no2,
				b.col_data 
			FROM (
				SELECT x.*
				FROM
					HL7_PID cross apply dbo.Split(FIELD_DATA,'~') x 
				WHERE
					    SEGMENT_CNT    = 1
					AND FIELD_POS      = 3
					AND HL7_MESSAGE_ID = @hl7messageID
					AND x.col_data like '%^OIB'
					) a
					cross apply dbo.Split(col_data,'^') b;

	SELECT
			@patientCODE = col_data
		FROM
			@tmp1
		WHERE
			    no1 = 1
			AND no2 = 1;

	SELECT
			@patientMBO = col_data
		FROM
			@tmp2
		WHERE
			    no1=3
			AND no2=1;

	SELECT
			@patientOIB = col_data
		FROM
			@tmp3
		WHERE
				no1 = 4
			AND 	no2 = 1;

	IF(isnull(@patientCODE,'')='""')
	BEGIN
		SET @patientCODE='';
	END;
	IF(isnull(@patientMBO,'')='""')
	BEGIN
		SET @patientMBO='';
	END;
	IF(isnull(@patientOIB,'')='""')
	BEGIN
		SET @patientOIB='';
	END;

	IF(isnull(@patientCODE,'')='' AND isnull(@patientMBO,'')<>'')
	BEGIN
		SET @patientCODE=@patientMBO;
	END;

	IF(isnull(@patientCODE,'')='')
	BEGIN
		SET @error='Invalid patient code';
		SET @patientID=NULL;
		RETURN;
	END;

	DELETE FROM @tmp1;
	DELETE FROM @tmp2;
	DELETE FROM @tmp3;


	-- OK, now we read Patient Name from PID-5
	SELECT
			@patientNAME = field_data
		FROM
			HL7_PID
		WHERE
			    SEGMENT_CNT    = 1
			AND FIELD_POS      = 5
			AND HL7_MESSAGE_ID = @hl7messageID;

	-- Sanity check, if name or code are empty, we bugger off.
	IF(isnull(@patientNAME,'')='' OR isnull(@patientCODE,'')='')
	BEGIN
		SET @error     = 'Invalid patient name or code';
		SET @patientID = NULL;
		RETURN;
	END;

/*--------------------------------------------------------------------------------*/
/*
Radićeva^^Kutina^^44320^^P~""^^^^^^H~""^^^^^^M  INTO @tmp1
id	no1	no2	col_data
1	1	1	Radićeva
2	1	2	
3	1	3	Kutina
4	1	4	
5	1	5	44320
6	1	6	
7	1	7	P
...
*/
	-- Now we do the same thing with address, yada yada yada
	INSERT INTO @tmp1(id,no1,no2,col_data)
	SELECT row_number()over(ORDER by a.col_no,b.col_no) id, a.col_no no1,b.col_no no2,b.col_data 
	FROM (SELECT x.* FROM HL7_PID cross apply dbo.Split(FIELD_DATA,'~') x 
	where SEGMENT_CNT=1 AND FIELD_POS=11 AND HL7_MESSAGE_ID=@hl7messageID AND x.col_data like '%^P') a
	cross apply dbo.Split(col_data,'^') b;

	SELECT @patientADDR=REPLACE(isnull(col_data,''),'""','') FROM @tmp1 where no1=1 AND no2=1; -- ulica
	IF(@patientADDR<>'')
  		SET @patientADDR=@patientADDR+' ';

	SELECT @patientADDR=@patientADDR+REPLACE(isnull(col_data,''),'""','') FROM @tmp1 where no1=1 AND no2=2; -- ulicni broj
	SET @patientADDR=LTRIM(RTRIM(@patientADDR));

	if @patientADDR!=''
  		set @patientADDR=@patientADDR+', ';

	SELECT @patientADDR=@patientADDR+REPLACE(isnull(col_data,''),'""','') FROM @tmp1 where no1=1 AND no2=5; -- postanski broj

	SET @patientADDR=@patientADDR+' ';

	SELECT @patientADDR=@patientADDR+REPLACE(isnull(col_data,''),'""','') FROM @tmp1 where no1=1 AND no2=3; -- grad

	SET @patientADDR=LTRIM(RTRIM(@patientADDR));

	if RIGHT(@patientADDR,1)=','
		SET @patientADDR=LEFT(@patientADDR,LEN(@patientADDR)-1);

	DELETE FROM @tmp1;

	-- Now the patient birthdate
	SELECT @ctemp=field_data FROM HL7_PID where SEGMENT_CNT=1 AND FIELD_POS=7 AND HL7_MESSAGE_ID=@hl7messageID;
	if(isnull(@ctemp,'')='')
	BEGIN
		SET @error='Invalid patient BirthDate';
		SET @patientID=NULL;
		return;
	end;

	-- Yada yada yada, boring
	BEGIN TRY
		SET @patientDOB=CONVERT(datetime,SUBSTRING(@ctemp,1,8),112); -- ISO | 112 = yyyymmdd
	END TRY
	BEGIN CATCH
		SELECT @error='Failed to convert patient BirthDate to date['+@ctemp+']: '+ERROR_MESSAGE();
		SET @patientID=NULL;
		RETURN;
	END CATCH;

/*--------------------------------------------------------------------------------*/
/*
SexID	Sex
0	NULL
1	M
2	F
3	O
*/
	-- And patient sex, yaaawn
	SELECT @ctemp=field_data FROM HL7_PID where SEGMENT_CNT=1 AND FIELD_POS=8 AND HL7_MESSAGE_ID=@hl7messageID;
	SELECT @patientSEXID=SEXID FROM SEX WHERE SEX=@ctemp;
	IF @patientSEXID is NULL
		SET @patientSEXID=0;
	
	-- Patient telephone number, we read that also...
	INSERT INTO @tmp1(id,no1,no2,col_data)
	SELECT TOP 1 1 id, a.col_no no1,b.col_no no2,b.col_data 
	FROM (SELECT x.* FROM HL7_PID cross apply dbo.Split(FIELD_DATA,'~') x 
	where SEGMENT_CNT=1 AND FIELD_POS=13 AND HL7_MESSAGE_ID=@hl7messageID) a
	cross apply dbo.Split(col_data,'^') b;

	SELECT @patientTEL=RTRIM(LTRIM(col_data)) FROM @tmp1;

	delete FROM @tmp1;
	/*------------------------------------------------------------------------------
	  FINALLY WE HAVE PATIENT DATA TO WORK WITH
	--------------------------------------------------------------------------------*/

	SET @error='';

	/* CHECK DATA LENGTH - ISSA DATABASE COLUMN SIZE */
	IF     (LEN(@patientCODE) > 50 
		OR LEN(@patientNAME) > 50 
		OR LEN(@patientMBO) > 20)
	BEGIN
		SET @error='Patient Code/Name/MBO too long';
		RETURN;
	END;

	-- OK, so first we try to find our patient
	EXEC Patient_Find 
			@patientCODE,
			@patientNAME,
			@patientDOB,
			@patientSEXID,
			@patientMBO,
			@patientOIB,
			@patientADDR,
			@patientTEL,
			@patientID 	output,
			@error 		output;

	-- If we found him, we found his PatientID
	-- If this PatientID is null, we didn't find the patient, let's add him
	IF @patientID is NULL
	BEGIN
		IF(@error<>'')
		BEGIN
			RETURN;
		END;
		IF(LEFT(@msgtype,7)<>'ADT^A01' AND LEFT(@msgtype,7)<>'ORM^O01' AND LEFT(@msgtype,7)<>'OMG^O19')
		BEGIN
			RETURN;
		END;
		DECLARE @i int;
		SET @i=0;
		-- We add patient using the Patient_Add1 procedure
		WHILE(@i<10)
		BEGIN
			EXEC Patient_Add1
				@patientCODE,
				@patientNAME,
				@patientDOB,
				@patientSEXID,
				@patientMBO,
				@patientOIB,
				@patientADDR,
				@patientTEL,
				@patientID   output,
				@error       output;
			IF(@error LIKE '%Violation of PRIMARY KEY%')
				Exec HL7_Wait;
			ELSE
				BREAK;
			SET @i=@i+1;
		END;
		IF @patientID > 1
		BEGIN
			EXEC PatientNameTableUpdate @patientID, @error output;
		END;
	END;
	ELSE
	BEGIN
		--So, in here it appears we have found our patient, and we are trying to update him?
		/*
			Update patient.ID if empty. BIS ID = patient.ID, BIS MBO = patient.Code - Varazdin only
		*/
		UPDATE
				PATIENT
			SET
				ID = REPLACE(@patientMBO,'""','')
			WHERE
				    patientid     = @patientID
				AND ISNULL(ID,'') = '';
		/* patient exists but not HL7 patient or different data - change old code AND add new patient - playing it safe 
		OR we could trust other system blindly AND update patient */
		/* IF @error='CodeIssuer' OR @error='Discrepancies' */
		IF @error='Discrepancies'
		BEGIN
			DECLARE @patientIDnew int;
			SET @error='';
			EXEC Patient_Add2
				@patientID,
				@patientCODE,
				@patientNAME,
				@patientDOB,
				@patientSEXID,
				@patientMBO,
				@patientOIB,
				@patientADDR,
				@patientTEL,
				@patientIDnew output,
				@error        output;
			SET @patientID=@patientIDnew;
			IF @patientID > 1
			BEGIN
				EXEC PatientNameTableUpdate @patientID, @error output;
			END;
		END;
	END;
END

GO
