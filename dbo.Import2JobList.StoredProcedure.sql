USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Import2JobList]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Import2JobList]
@tab HL7_TABLE1 READONLY,
@error varchar(max) output
AS
BEGIN
	SET NOCOUNT ON;
	SET @error='';

	DECLARE @newexamUID varchar(256),@rnd varchar(256),@cnt int,@x int;

	SELECT @cnt=COUNT(1) FROM @tab;
	SET @x=0;

	DECLARE @tmp1 TABLE(id int);
	DECLARE @theid int;

	WHILE @x < @cnt
	BEGIN
		SET @x=@x+1;
		SET @rnd=SUBSTRING(CONVERT(VARCHAR,RAND(),3),3,10);
   /*
    UID=Vams UID + Wibu Workstation ID + type + random int + ISO date time
    type: 1 = exam, 99 = users, the rest = 999
    RAND() = 0,314503727205451
    UID=varchar(64) ...
   
   1.2.826.0.1.3680043.2.39.12699.1.113477.20190318072949
   */
	SET @newexamUID='1.2.826.0.1.3680043.2.39'+'.11741'+'.1.1'+@rnd+'.'+convert(varchar,getdate(),112)+replace(convert(varchar,getdate(),114),':','');
	BEGIN TRY
		INSERT INTO JOBLIST(
			examdeviceid,
			patientid,
			jobdate,
			jobstart,
			jobend,
			referingid,
			newexamUID,
			newexamnumber,
			examtypeid,
			examtype,
			editdate,
			editby,
			SENTFROM,
			CURRENTDISEASE,
			CLINICALDIAGNOSIS,
			PreceedingExams,
			EXAMSTATUS,
			PAYTYPE,
			STATUSMASK,
			Hospital)
		OUTPUT Inserted.JoblistID into @tmp1
		SELECT
			DEVICEID,
			PATIENTID,
			ORDER_DATE,
			ORDER_TIME,
			DATEADD(MINUTE,5,ORDER_TIME),
			REFERRINGID,
			@newexamUID,
			ISNULL(ACCESSIONNUMBER2,ACCESSIONNUMBER),
			EXAMTYPEID,
			EXAMTYPENAME,
			GETDATE(),1,
			SENTFROM,
			CURRENTDISEASE,
			CLINICALDIAGNOSIS,
			PreceedingExams,
			EXAMSTATUS,
			PAYTYPE,
			CASE WHEN ISNULL(ADMISSIONID,'')='S' then 2 else 0 END,
			CASE WHEN patienttype='I' then 1 else 0 END
		FROM
			@tab
		WHERE
			ID=@x;
		SELECT @theid=id from @tmp1;

	  INSERT INTO
			HL7_ORDERS(
				HL7_MESSAGE_ID,
				JOBLISTID,
				ACCNO,
				TFIELD1,
				TFIELD2,
				AccessionNumberHIS) 
			SELECT 
				HL7_MESSAGE_ID,
				@theid,
				ACCESSIONNUMBER,
				CURRENTDISEASE,
				BILLING_REQUEST_NO,
				ACCESSIONNUMBER
			FROM 
				@tab
			WHERE
				ID=@x;

   END TRY
   BEGIN CATCH
      SET @error='Failed to insert order ['+CAST(@x as VARCHAR)+'] into JobList: '+ERROR_MESSAGE();
	  
	  DELETE FROM HL7_ORDERS WHERE HL7_MESSAGE_ID IN (SELECT HL7_MESSAGE_ID FROM @tab);

	  RETURN;

   END CATCH;

END;

/* all good */

END


GO
