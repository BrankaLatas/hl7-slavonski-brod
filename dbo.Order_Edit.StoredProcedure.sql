USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Order_Edit]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[Order_Edit]
@hl7messageID INT,
@error VARCHAR(MAX) output
AS
BEGIN
SET NOCOUNT ON;
SET @error='';

DECLARE @joblistid int;

SET @joblistid=NULL;

SELECT TOP 1 @joblistid=joblistid FROM HL7_ORDERS WHERE ACCNO IN
(select FIELD_DATA from HL7_ORC where FIELD_POS=2 AND HL7_MESSAGE_ID=@hl7messageID);

IF @joblistid IS NULL
BEGIN
  SET @error='Can not update order, order does not exist';
  EXEC Shared_LogError @hl7messageID,@error,2,'Edit order';
  RETURN;
END;
/**************************************************************************************
   BACKUP JOBLIST
**************************************************************************************/
/*
EXEC Joblist_Backup @joblistid,@error output;
IF @error<>''
BEGIN
   EXEC Shared_LogError @hl7messageID,@error,2,'Edit order';
   RETURN;
END;
*/
/**************************************************************************************
   UPDATE ORDER - nothing to update at the moment, we do not receive order updates, only new order or cancel order
**************************************************************************************/
/*
BEGIN TRY
UPDATE JOBLIST SET EDITDATE=GETDATE(),EDITBY=1 WHERE JOBLISTID=@joblistid;
END TRY
BEGIN CATCH
  SET @error='Can not mark order as canceled ['+ERROR_MESSAGE()+']';
  EXEC Shared_LogError @hl7messageID,@error,2,'Edit order';
  RETURN;
END CATCH;
*/
/* All good */
/*----------------------------------------------------------------------------------------*/
SET @error=''; /* No error = OK for c# server */
EXEC Shared_LogError @hl7messageID,@error,998,'Edit order';
/*----------------------------------------------------------------------------------------*/

END







GO
