USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[HL7_Wait]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[HL7_Wait]
AS
BEGIN
      /*
         Add random delay, when lots of threads try to get patientid or examid, operation can fail
      */
      declare @rnd varchar(32),@delay varchar(32);
      set @rnd=cast(cast(rand()*100 as int) as varchar);
      if(@rnd='0')
        set @rnd='10';
      
      set @delay=stuff('00:00:00.000',13-len(@rnd),len(@rnd),@rnd);
      WAITFOR DELAY @delay;

END







GO
