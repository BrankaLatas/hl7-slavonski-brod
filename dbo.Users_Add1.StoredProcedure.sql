USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Users_Add1]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[Users_Add1]
@usersCODE varchar(256),
@usersNAME varchar(256),
@usersID int output,
@error varchar(max) output,
@levelID int=6
AS
BEGIN
SET NOCOUNT ON;
SET @error='';
SET @usersID=null;
/*
 UID=Vams UID + Wibu Workstation ID + type + random int + ISO date time
 type: 1 = exam, 99 = users, the rest = 999
 RAND() = 0,314503727205451
 UID=varchar(64) ...
*/
DECLARE @uid varchar(256),@rnd varchar(256),@departmentID int;

/*
 select b.levelid from stringresource a join userlevel b on a.stringresourceid=b.resourcenumber 
 where a.English='Referring Physician' or it could be 'Physician' etc., Description field in StringResource='UsersLevel'

SET @levelID=6; /* 'Referring Physician' */
*/

/* SELECT DEPARTMENTID FROM DEPARTMENT WHERE NAME='...' or maybe DEscription like '...%' etc. */
SET @departmentID=1; -- no department, but still needs value

SET @rnd=SUBSTRING(CONVERT(VARCHAR,RAND(),3),3,10);

SET @uid='1.2.826.0.1.3680043.2.39'+'.11741'+'.99.1'+@rnd+'.'+convert(varchar,getdate(),112)+replace(convert(varchar,getdate(),114),':','');

DECLARE @tmp1 TABLE(id int);
DECLARE @guest int=1;

IF(@levelID=13)
  SET @guest=0;

BEGIN TRY
INSERT INTO Users([LOGIN],[NAME],[UID],LEVELID,USERCODE,EDITBY,EDITDATE,DEPARTMENT,GUEST)
OUTPUT Inserted.UsersID into @tmp1
VALUES(@usersCODE,@usersNAME,@uid,@levelID,@usersCODE,1,GETDATE(),@departmentID,@guest);
END TRY
BEGIN CATCH
  SELECT @error='Failed to create new user: '+ERROR_MESSAGE();
  SET @usersID=NULL;
  RETURN;
END CATCH;

SET @error='';
SELECT @usersID=id from @tmp1;

END;



GO
