USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Users_Backup]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[Users_Backup]
@usersID int,
@error varchar(max) output
AS
BEGIN
SET NOCOUNT ON;
SET @error='';

if(@usersID is null)
  BEGIN
  SET @error='Users_Backup - PatientID is null';
  RETURN;
  END;

DECLARE @query nvarchar(max), @columns nvarchar(max);

BEGIN TRY

SELECT @query=N'SELECT @columns=STUFF((select '',''+COLUMN_NAME FROM '+SUBSTRING(DB_NAME(),1,LEN(DB_NAME())-4)+'.INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_NAME=''Users'' AND TABLE_SCHEMA=''dbo'' FOR XML PATH ('''')),1,1,'''')';

exec sp_executesql @query,N'@columns nvarchar(max) output',@columns output;

IF(ISNULL(@columns,'')='')
BEGIN
   SELECT @error='Failed to get users columns [usersid='+cast(@usersID as varchar)+']: '+ERROR_MESSAGE();
   RETURN;
END;

SET @query='INSERT INTO UsersBackup('+@columns+',deleted) SELECT '+@columns+',0 FROM Users WHERE UsersID='+CAST(@usersID AS VARCHAR);

EXEC (@query);

END TRY
BEGIN CATCH
   SELECT @error='Failed to backup user [usersid='+cast(@usersID as varchar)+']: '+ERROR_MESSAGE();
   RETURN;
END CATCH;

END


GO
