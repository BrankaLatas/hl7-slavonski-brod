USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Patient_Find]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








------------------------------------------------------------------------------

CREATE procedure [dbo].[Patient_Find]
@patientCODE varchar(256),@patientNAME varchar(256),@patientDOB datetime,@patientSEXID int,
@patientMBO varchar(256),@patientOIB varchar(256),@patientADDR varchar(256),@patientTEL varchar(256),
@patientID int output, @error varchar(max) output
AS
begin
SET NOCOUNT ON;
SET @error='';
SET @patientID=null;

declare @issaCODE varchar(256),@issaNAME varchar(256),@issaDOB datetime,@issaSEXID int, @issaCODEISSUER int,
@issaADDR varchar(256),@issaTEL varchar(256),@issaMBO varchar(256),@issaOIB varchar(256);

BEGIN TRY
SELECT @patientID=patientid,@issaCODE=code,@issaNAME=[name],@issaDOB=BirthDate,@issaSEXID=SexID,@issaCODEISSUER=CodeIssuer,
@issaMBO=ID,@issaADDR=[address],@issaTEL=telephone,@issaOIB=SecurityNumber from Patient WHERE code=@patientCODE;
END TRY
BEGIN CATCH
   SET @error='Failed to search patient ['+ERROR_MESSAGE()+']';
   RETURN;
END CATCH;

if(@patientID is NULL)
  begin
  set @error='';
  return;
  end;
  /*
if(isnull(@issaCODEISSUER,-1)<>3)
  begin
  set @error='CodeIssuer';
  return;
  end;
  */
if((@issaDOB is NULL OR @patientDOB is NULL) OR
(isnull(@issaSEXID,-1)<>isnull(@patientSEXID,-1) OR @issaNAME<>@patientNAME OR @issaDOB<>@patientDOB))
  begin
  set @error='Discrepancies'; /* HL7 patient (code issuer=3), but different data - update, or rename old code and add new patient? */
  return;
  end;

/* additional check for differences if we need to update patient */
if(isnull(@issaMBO,'')<>isnull(@patientMBO,'') OR isnull(@issaADDR,'')<>isnull(@patientADDR,'') OR isnull(@issaTEL,'')<>isnull(@patientTEL,''))
  begin
  set @error='Misc. Discrepancies';
  return;
  end;

  /* all good, we found the exact HL7 patient */

END




GO
