USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Users_Check]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[Users_Check]
	@hl7messageID int,
	@usersID      int          output,
	@error        varchar(max) output,
	@segment_cnt  int = null, /* 1=default, ORC segments can repeat in message (ORC, OBR pairs) */
	@dont_add     int = 0
AS
BEGIN
	SET NOCOUNT ON;
	SET @error='';
	SET @usersID=null;

	DECLARE @usersCODE		varchar(256),
			@usersNAME      varchar(256),
			@usersFirstName varchar(256),
			@usersLastName  varchar(256),
			@usersLOGIN 	varchar(256),
			@ctemp      	varchar(max);
	DECLARE @issaNAME   	varchar(256);
	/* DECLARE @usersIDnew int; */
	/*--------------------------------------------------------------------------------*/
	/*
	          0082147^SMOLIĆ^TATJANA
	*/
	SELECT 
			@ctemp = FIELD_DATA
		FROM
			HL7_ORC
		WHERE
				SEGMENT_CNT    = ISNULL(@segment_cnt,1)
			AND FIELD_POS      = 12
			AND HL7_MESSAGE_ID = @hl7messageID;

	DECLARE @tbl1 TABLE(col_no int, col_data varchar(max));

	IF isnull(@ctemp,'')<>''
	BEGIN
		INSERT INTO
				@tbl1(col_no,col_data)
			SELECT
				col_no,
				col_data
			FROM
				Split(@ctemp,'^');
		SELECT @usersCODE      = col_data FROM @tbl1 WHERE col_no=1;
		SELECT @usersFirstName = col_data FROM @tbl1 WHERE col_no=3;
		SELECT @usersLastName  = col_data FROM @tbl1 WHERE col_no=2;
		SELECT @usersNAME      = @usersFirstName + ' ' + @usersLastName
		/*
		SET @ctemp=null;
		SELECT @ctemp=col_data FROM @tbl1 WHERE col_no=3;
		IF(isnull(@ctemp,'')<>'')
		BEGIN
			SET @usersNAME=@usersNAME+' '+@ctemp;
		END;
		SET @ctemp=null;
		SELECT @ctemp=col_data FROM @tbl1 WHERE col_no=4;
		IF(isnull(@ctemp,'')<>'')
		BEGIN
			SET @usersNAME=@usersNAME+' '+@ctemp;
		END;
		*/
	END;

	IF(isnull(@usersCODE,'')='""' OR isnull(@usersNAME,'')='""')
	BEGIN
		SET @error='No referring data';
		RETURN;
	END;

	IF(isnull(@usersCODE,'')='' OR isnull(@usersNAME,'')='')
	BEGIN
		SET @error='No referring data';
		RETURN;
	END;
	/*--------------------------------------------------------------------------------*/
	/* CHECK DATA LENGTH - ISSA DATABASE COLUMN SIZE */
	IF(LEN(@usersCODE)>64 OR LEN(@usersNAME)>250)
	BEGIN
		SET @error='Users Code/Name too long';
		RETURN;
	END;

	EXEC Users_Find
		@usersCODE,
		@usersNAME,
		@usersID    output,
		@error      output;
	IF @usersID is null
	BEGIN
		IF(@error='' AND @dont_add=0)
		BEGIN
			set @error='';
			EXEC Users_Add1
				@usersCODE,
				@usersNAME,
				@usersID    output,
				@error      output;
		END;
	END;
	ELSE
	BEGIN
		IF @error='Discrepancies'
		BEGIN
			set @error='';
			  /*
			  set @usersIDnew=NULL;
			  EXEC Users_Add2 @usersID,@usersCODE,@usersNAME,@usersIDnew output, @error output;
			  IF(@error<>'')
			    SET @usersID=@usersIDnew;
		      */
		END;
	END;
/*--------------------------------------------------------------------------------*/
END;


GO
