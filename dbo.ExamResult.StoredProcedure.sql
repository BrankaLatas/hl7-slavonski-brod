USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[ExamResult]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--This procedure should update the result in Issa based on the message from third party HIS
CREATE PROCEDURE [dbo].[ExamResult]
    @hl7_message_id int,
    @error varchar(max) output
AS
BEGIN
	SET NOCOUNT ON;
	SET @error='';

	DECLARE
		@result  varchar(max) = '',
		@usersid          INT = 0, 
		@resultdate  DATETIME = NULL;
	DECLARE
		@examid INT,
		@tmp1   varchar(max),
		@tmp2   varchar(max),
		@tmp3   varchar(max);
	DECLARE 
		@patientcode    varchar(256),
		@ourpatientcode varchar(256),
		@len            INT,
		@statusmask     INT;
	/*
	MSH|^~\&|MEDIT|NEURON|ISSA|OBVZ|20190419130939||ORM^O01|MSGID|P^T|2.3||||||8859/2
	PID|||AAAAAAAAA||PREZIME^IME||YYYYMMDD000000.0000+0200|M|
	ORC|SC|BBBBB|NNNNN||F||||NN^PREZIME IME|
	OBR||BBBBB|NNNNN||||20190416160244||||||||||||||||||F|||
	OBX|1|TX|BBBBB||ssss|
	*/

	SELECT
			@tmp1 = ISNULL(FIELD_DATA,'')
		FROM
			HL7_ORC
		WHERE 
			SEGMENT_CNT    = 1 
		AND  FIELD_POS      = 2 
		AND  HL7_MESSAGE_ID = @hl7_message_id;

	SELECT
			@tmp2 = ISNULL(FIELD_DATA,'')
		FROM
			HL7_OBR
		WHERE
			    SEGMENT_CNT    = 1
			AND FIELD_POS      = 2
			AND HL7_MESSAGE_ID = @hl7_message_id;

	SELECT
			@tmp3 = ISNULL(FIELD_DATA,'')
		FROM
			HL7_OBX
		WHERE
				SEGMENT_CNT    = 1
			AND  FIELD_POS      = 3
			AND  HL7_MESSAGE_ID = @hl7_message_id;

	IF( (@tmp1='' OR @tmp2='' OR @tmp3='') OR (@tmp1<>@tmp2 OR @tmp1<>@tmp3) )
	BEGIN
		SET @error='Invalid order number in segments';
		RETURN;
	END;

	-- OK, now we try to get the actual ExamID where we have to put our result
	-- We assume that Accession number which is coming from the incoming message is
	-- equal to the ExamID of the exam which needs to be updated
	BEGIN TRY
		SELECT TOP 1
				@examid = examid 
			FROM
				Exam
			WHERE
				Exam.ExamID = CAST(@tmp1 AS INT);
	END TRY
	BEGIN CATCH
		SET @error='Invalid order number';
		RETURN;
	END CATCH;

	-- OK, so we were not able to find matching order
	IF(@examid IS NULL)
	BEGIN
		SET @error='Order not found for acc. no. ['+@tmp1+']';
		RETURN;
	END;

	-- Right, everything seems fine, but it appears your exam was not marked for Neuron
	-- Bugger off.
	IF(SELECT COUNT(1) FROM EXAM WHERE UsersIDResult=14 AND EXAMID=@examid)<1
	BEGIN
		SET @error='Acc. no. ['+@tmp1+'] not marked for Neuron in Issa';
		RETURN;
	END;

	-- Now we take out the PatientID from the incoming message
	SELECT
			@patientcode = FIELD_DATA
		FROM
				 HL7_PID
			WHERE 
				    SEGMENT_CNT    = 1
				AND FIELD_POS      = 3 
				AND HL7_MESSAGE_ID = @hl7_message_id;

	/*
	SELECT @ourpatientcode=ISNULL(patient.id,patient.code),@len=LEN(exam.result),@statusmask=exam.statusmask 
	from patient join exam on patient.patientid=exam.patientid where exam.examid=@examid;
	*/

	-- Then we get the PatientID from our Patient table, based on the ExamID which we got from the incoming message
	-- We also load the length of the Result field and the status of our exam in Issa
	SELECT
			@ourpatientcode = patient.code,
			@len            = LEN(exam.result),
			@statusmask     = exam.statusmask 
		FROM
				Patient
			JOIN Exam 
			ON 
				patient.patientid = exam.patientid 
		WHERE
			Exam.Examid = @examid;

	-- OK, so if PatientID is different, bugger off.
	IF(@patientcode<>@ourpatientcode)
	BEGIN
		SET @error='Received patient code is not the same as ordered';
		RETURN;
	END;

	-- Obviously, if there is something in the Exam.Result field, no update for you also!
	-- Bugger off.
	IF(ISNULL(@len,0)<>0)
	BEGIN
		SET @error='Exam result not empty';
		RETURN;
	END;

	-- Again, if exam is marked as finished, bugger off also.
	IF(@statusmask&1=1)
	BEGIN
		SET @error='Exam is marked as finished';
		RETURN;
	END;

	-- OK, so we finally decided to take our result from the incoming HL7 message
	-- No update yet, we just took the data from the message.
	SELECT
			@result = FIELD_DATA
		FROM 
			HL7_OBX 
		WHERE
			    SEGMENT_CNT    = 1 
			AND FIELD_POS      = 5 
			AND HL7_MESSAGE_ID = @hl7_message_id;

	-- Oh, drama, result we have received is empty
	-- Bugger off, no update for you!
	IF(ISNULL(@result,'')='')
	BEGIN
		SET @error='Received result is empty';
  		RETURN;
	END;

	-- Aha, but you didn't fill in the result date & time properly, no update for you!
	-- Bugger off!
	SELECT @tmp1=SUBSTRING(FIELD_DATA,1,14) FROM HL7_OBR WHERE SEGMENT_CNT=1 and FIELD_POS=7 AND HL7_MESSAGE_ID=@hl7_message_id;
	IF(LEN(ISNULL(@tmp1,''))<>14)
	BEGIN
		SET @error='Invalid result date & time';
		RETURN;
	END;

	-- OK, now we go and read the result date from incoming message
	DECLARE @s varchar(32);
	BEGIN TRY
		SET @s=SUBSTRING(@tmp1,1,8)+' '+SUBSTRING(@tmp1,9,2)+':'+SUBSTRING(@tmp1,11,2)+':'+SUBSTRING(@tmp1,13,2);
		SELECT @resultdate=CONVERT(datetime,@s,120);
	END TRY
	BEGIN CATCH
		SET @error='Failed to convert ['+@s+'] to Result date';
		RETURN;
	END CATCH;

	-- Right, now we have to determine who is the user who wrote the incoming report
	DECLARE @usersCODE varchar(256),
		   @usersNAME varchar(256);

	-- We read the ORC-11 field
	SELECT 
			@tmp1=FIELD_DATA 
		FROM
			HL7_ORC
		WHERE 
			    SEGMENT_CNT    = 1 
			AND FIELD_POS      = 11
			AND HL7_MESSAGE_ID = @hl7_message_id;

	DECLARE @tbl1 TABLE(col_no int, col_data varchar(max));
	INSERT INTO @tbl1(col_no,col_data) SELECT col_no,col_data FROM Split(@tmp1,'^');

	-- We store the data from the field to our temporary table
	SELECT @usersCODE=col_data FROM @tbl1 WHERE col_no=1;
	SELECT @usersNAME=col_data FROM @tbl1 WHERE col_no=2;

	-- You assholes! Where is the reading radiologist data?!
	-- Bugger off, no update for you!
	IF(isnull(@usersCODE,'')='' OR isnull(@usersNAME,'')='')
	BEGIN
		SET @error='No physician data';
		RETURN;
	END;

	-- Now we try to find that user in our Users table, we have a procedure for it, ha!
	EXEC Users_Find
			@usersCODE,
			@usersNAME,
			@usersid output,
			@error output;

	-- OK, so you did send me the physician data, but I have no idea who he is, you assholes!
	-- Bugger off, no update for you!
	IF(@usersid is null)
	BEGIN
		SET @error='Unknown physician';
		RETURN;
	END;
	
	-- OK, so we're finally past all possible screw-ups, update begins...
	BEGIN TRY
		-- First we backup the current exam record
		EXEC Exam_Backup @examid,@error output;
		IF(@error<>'')
		BEGIN
			SET @error='Exam backup failed';
			RETURN;
  		END;
		/*
		UPDATE EXAM SET Result=dbo.HL7_UnEscape(@result), StatusMask=StatusMask|1, usersID=@usersid,ResultDate=@resultdate,editdate=getdate(),
		editby=1 WHERE examid=@examid AND ISNULL(result,'')='';
		*/

		--Now we finally do our grand update, Yeee!!!
  		UPDATE EXAM 
  			SET 
				Result          = dbo.HL7_UnEscape(@result), 
				usersID         = @usersid,
				StatusMask      = StatusMask|1,
				--UsersOverViewID = @usersid,
				ResultDate      = @resultdate,
				editdate        = getdate(),
				editby          = 1 
			WHERE
				    examid            = @examid
				AND ISNULL(result,'') = '';
	END TRY
	BEGIN CATCH
		SET @error='Failed to update exam result';
	END CATCH;
END

GO
