USE [IssaSB1_HL7]
GO
/****** Object:  StoredProcedure [dbo].[Patient_Add1]    Script Date: 25.5.2020. 11:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------
-- This procedure takes patient data and tries to enter it into the Patient table

CREATE procedure [dbo].[Patient_Add1]
	@PatientCode  varchar(256),
	@PatientName  varchar(256),
	@PatientDOB   datetime,
	@PatientSexID int,
	@PatientMBO   varchar(256),
	@PatientOIB   varchar(256),
	@PatientAddr  varchar(256),
	@PatientTel   varchar(256),
	@PatientID    int           output,
	@error        varchar(max)  output
AS
BEGIN
	SET NOCOUNT ON;
	SET @error='';

	DECLARE @CodeIssuer INT=3;

	DECLARE @UID    varchar(256), 
		    @LastID int;

	SET @PatientID = NULL;
	/*
	 UID=Vams UID + Wibu Workstation ID + type + random int + ISO date time
	 type: 1 = exam, 99 = users, the rest = 999
	 RAND() = 0,314503727205451
	 UID=varchar(64) ...
	*/
	SET @UID = '1.2.826.0.1.3680043.2.39'+'.11718'+'.999.1'+SUBSTRING(CONVERT(VARCHAR,RAND(),3),3,10)+'.'+convert(varchar,getdate(),112)+replace(convert(varchar,getdate(),114),':','');
	
	DECLARE @tmp1 TABLE(id int);

	SELECT
			@LastID = case when a.res>b.res then a.res+1 else b.res+1 end
		FROM 
			(Select isnull(max(patientid),0) res from patient)       a,
			(Select isnull(max(patientid),0) res from patientbackup) b;

	BEGIN TRY
		INSERT INTO Patient(
			PatientID,
			Code,
			[name],
			BirthDate,
			SexID,
			ID,
			[address],
			Telephone,
			EditBy,
			EditDate,
			UID,
			CodeIssuer,
			SecurityNumber)
		OUTPUT
			Inserted.PatientID into @tmp1
		VALUES(
			@LastID,
			@PatientCode,
			@PatientName,
			@PatientDOB,
			@patientSEXID,
			REPLACE(@PatientMBO,'""',''),
			REPLACE(@PatientAddr,'""',''),
			SUBSTRING(REPLACE(@PatientTel,'""',''),1,50),
			1,
			GETDATE(),
			SUBSTRING(@UID,1,64),
			@CodeIssuer,
			@PatientOIB);
	END TRY
	BEGIN CATCH
		SELECT @error='Failed to insert new patient: '+ERROR_MESSAGE();
		SET @PatientID=NULL;
		RETURN;
	END CATCH;

	SET @error='';
	SELECT @PatientID = id from @tmp1;
END

GO
